SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
CREATE FUNCTION [dbo].[fn_ConvertDateintoUTC]
(
    -- Add the parameters for the function here
  @date Datetime, @State varchar(50)
)
RETURNS datetimeoffset(7)
AS
BEGIN
    Declare @OutputDate as datetimeoffset(7)
	Select @OutputDate = Case @State
	                                 WHEN 'WA' THEN cast(@date as datetime) AT TIME ZONE 'W. Australia Standard Time'
	                                 WHEN 'NSW'THEN  cast(@date as datetime) AT TIME ZONE 'AUS Eastern Standard Time'
									 WHEN 'QLD'THEN  cast(@date as datetime) AT TIME ZONE 'E. Australia Standard Time'
									 WHEN 'VIC'THEN  cast(@date as datetime) AT TIME ZONE 'AUS Eastern Standard Time'
									 WHEN 'ACT'THEN  cast(@date as datetime) AT TIME ZONE 'AUS Eastern Standard Time'
									 WHEN 'TAS'THEN  cast(@date as datetime) AT TIME ZONE 'AUS Eastern Standard Time'
									 WHEN 'SA'THEN  cast(@date as datetime) AT TIME ZONE 'Cen. Australia Standard Time'
									 WHEN 'NT'THEN  cast(@date as datetime) AT TIME ZONE 'AUS Central Standard Time'
									 ELSE @date END

Return @OutputDate

END
GO
