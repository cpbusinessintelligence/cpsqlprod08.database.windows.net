SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION dReturnDate( @dFecha as datetime)

returns DATETIME

as

begin

     DECLARE @D AS datetimeoffset

     SET @D = CONVERT(datetimeoffset, @Dfecha) AT TIME ZONE 'AUS Eastern Standard Time'

     RETURN CONVERT(datetime, @D);

end

GO
