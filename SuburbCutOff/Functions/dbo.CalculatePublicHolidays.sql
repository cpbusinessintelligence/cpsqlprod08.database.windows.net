SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CalculatePublicHolidays] 
(	
	-- Add the parameters for the function here
	@Date Date,
	@State Varchar(20),
	@Zone Varchar(20),
	@BusinessDays int
)
RETURNS  @OutputTable table (NextBusinessDate Date , NextBusinessDay Varchar(10))
AS
 
	BEGIN
	-- Load the next 10 public holidays into a table variable
	Declare @TempHolidays table ( TempDate Date, [HolidayType] Varchar(20), [State/Zone Name] Varchar(20) )

    INSERT INTO @TempHolidays SELECT top 10000 [Date], [HolidayType] ,[State/Zone Name]    
							  FROM [SuburbCutOff].[dbo].[PublicHolidays]
							  Where Date > = @Date and Date <= Getdate() +30  order by Date asc 

	
	-- Check if any state/zone  based holidays which are not relevant
	Delete from @TempHolidays Where HolidayType =  'State' and [State/Zone Name] <> @State
	Delete from @TempHolidays Where HolidayType =  'Zone'  and [State/Zone Name] <> @Zone

	DECLARE @businessDate As date = @date;
	Declare @Counter  as int = @BusinessDays
	--Find nth public holiday
    WHILE  @counter >0 
	BEGIN
		Select  @businessDate = DATEADD(dd, 1, @businessDate);
		 while EXISTS (SELECT TempDate FROM @TempHolidays WHERE TempDate=@businessDate )
		    Select  @businessDate = DATEADD(dd, 1, @businessDate);
		Select @Counter =  @Counter - 1
	 END
   -- to check if today is a public holiday 
   if @BusinessDays = 0
     while EXISTS (SELECT TempDate FROM @TempHolidays WHERE TempDate=@businessDate )
		    Select  @businessDate = DATEADD(dd, 1, @businessDate);
     
   insert INTO @OutputTable Select  @businessDate, Datename(weekday,@businessDate) 

	Return 
	--SELECT Datename(weekday,@Date) as td , @Date as date

END





GO
