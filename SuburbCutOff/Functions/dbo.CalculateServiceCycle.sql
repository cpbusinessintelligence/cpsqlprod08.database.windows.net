SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jobin philip
-- Create date: 26 March 2019
-- Description:	This function will look for Public holidays nationally, state wise and Zone wise
-- =============================================
CREATE FUNCTION [dbo].[CalculateServiceCycle] 
(	
	-- Add the parameters for the function here
	@ProfileCode Varchar(20),
	@WeekDay Varchar(20),
	@Date Date,
	@ReadyAtDate Datetime,
	@PostCode Varchar(20),
	@Suburb Varchar(50)
)
RETURNS  @OutputTable table (CycleStart	DateTime , CycleEnd Datetime, CutOffOrder int,CutOff1 time(7), Comments Varchar(100))
AS
 
	BEGIN
		Declare @CutOff1 as time(7)
		Declare @CutOff2 as time(7)
		Declare @CutOff3 as time(7)
		Declare @CutOff4 as time(7)
		Declare @CutOff5 as time(7)
		Declare @Zone As varchar(20)

		Select @Zone =  Zone ,@CutOff1 = CutOff1, @CutOff2 = cutOff2, @cutoff3 =Cutoff3, @Cutoff4 = cutoff4, @cutoff5= cutoff5
		from [dbo].[Suburbs] where PostCode = @PostCode and @Suburb = @Suburb
   
		   if @Zone is not null
		   BEGIN
					 -- Load available ServiceSchedule for that day
					insert into @OutputTable Select CAST(@Date as Datetime) + CAST(SPD.StartTime as Datetime) ,
												   CAST(@Date as Datetime) + CAST(SPD.EndTIme as Datetime), 
												   SPD.[Order] ,@cutoff1 ,Case When SPD.StartTime is null then 'No Schedules fot the day' else 'All Good' end
												 from [dbo].[ProfileDefinition] PD Join [dbo].[SuburbProfile] SP on PD.ProfileCode =SP.ProfileCode 
																				   left Join [dbo].[SuburbProfileDetails] SPD on SP.[SuburbProfileID]= SPD.ProfileID

												  Where PD.ProfileCode = @ProfileCode
													and PD.IsActive = 1 
													and SP. [DayofWeek] = @WeekDay
													and SP.Isworking = 'Y'

					Delete @OutputTable where @ReadyAtDate > CycleEnd   -- Remove all cycles which are passed
					if (select count(*) from @OutputTable) >1
					
					  DELETE FROM @OutputTable Where CutOffOrder  not in (SELECT TOP 1 CutOffOrder FROM @OutputTable order by CutOffOrder asc)
					----   insert into @OutputTable Select Null,Null,Null,Null,'Suburb Dont Exist'
			 
			   
			     Update @OutputTable SET CycleStart = (CASE CutOffOrder WHEN 1 Then  CAST(@Date as Datetime) + CAST(@CutOff1 as Datetime)
				                                                       WHEN 2 THEN  CAST(@Date as Datetime) + CAST(@CutOff2 as Datetime)
																	   WHEN 3 THEN  CAST(@Date as Datetime) + CAST(@CutOff3 as Datetime)
																	   WHEN 4 THEN  CAST(@Date as Datetime) + CAST(@CutOff4 as Datetime)
																	   WHEN 5 THEN  CAST(@Date as Datetime) + CAST(@CutOff5 as Datetime)
																	   ELSE CycleStart END)

END
			Else 
			     insert into @OutputTable Select Null,Null,Null,Null,'Suburb Dont Exist'

			Return 
			--SELECT Datename(weekday,@Date) as td , @Date as date


		
END




GO
