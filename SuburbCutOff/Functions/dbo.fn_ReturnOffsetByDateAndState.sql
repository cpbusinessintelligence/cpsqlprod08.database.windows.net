SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
Create FUNCTION [dbo].[fn_ReturnOffsetByDateAndState]
(
    -- Add the parameters for the function here
  @date Datetime, @State varchar(50)
)
RETURNS varchar(30)
AS
BEGIN
    Declare @OutputDate as varchar(30)
	Select @OutputDate = Case @State  
	                                 WHEN 'WA' THEN FORMAT(cast(@date as datetime) AT TIME ZONE 'W. Australia Standard Time', 'zzz') 
	                                 WHEN 'NSW'THEN FORMAT(cast(@date as datetime) AT TIME ZONE 'AUS Eastern Standard Time', 'zzz')  
									 WHEN 'QLD'THEN FORMAT(cast(@date as datetime) AT TIME ZONE 'E. Australia Standard Time', 'zzz') 
									 WHEN 'VIC'THEN FORMAT(cast(@date as datetime) AT TIME ZONE 'AUS Eastern Standard Time', 'zzz') 
									 WHEN 'ACT'THEN FORMAT(cast(@date as datetime) AT TIME ZONE 'AUS Eastern Standard Time', 'zzz') 
									 WHEN 'TAS'THEN FORMAT(cast(@date as datetime) AT TIME ZONE 'AUS Eastern Standard Time', 'zzz') 
									 WHEN 'SA'THEN FORMAT(cast(@date as datetime) AT TIME ZONE 'Cen. Australia Standard Time', 'zzz') 
									 WHEN 'NT'THEN FORMAT(cast(@date as datetime) AT TIME ZONE 'AUS Central Standard Time', 'zzz') 
									 ELSE FORMAT(cast(@date as datetime),'zzz') END

Return @OutputDate

END
GO
