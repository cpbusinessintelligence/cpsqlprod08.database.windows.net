SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PublicHolidays_12062019_KP] (
		[PublicHolidaysID]     [int] NOT NULL,
		[HolidayType]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[State/Zone Name]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Date]                 [date] NOT NULL,
		[Description]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddWho]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]          [smalldatetime] NOT NULL,
		[EditWho]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]         [smalldatetime] NULL
)
GO
ALTER TABLE [dbo].[PublicHolidays_12062019_KP] SET (LOCK_ESCALATION = TABLE)
GO
