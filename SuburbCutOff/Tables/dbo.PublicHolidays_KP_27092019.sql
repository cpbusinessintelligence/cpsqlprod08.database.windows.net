SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PublicHolidays_KP_27092019] (
		[PublicHolidaysID]     [int] IDENTITY(1, 1) NOT NULL,
		[HolidayType]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[State/Zone Name]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Date]                 [date] NOT NULL,
		[Description]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddWho]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]          [smalldatetime] NOT NULL,
		[EditWho]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]         [smalldatetime] NULL
)
GO
ALTER TABLE [dbo].[PublicHolidays_KP_27092019] SET (LOCK_ESCALATION = TABLE)
GO
