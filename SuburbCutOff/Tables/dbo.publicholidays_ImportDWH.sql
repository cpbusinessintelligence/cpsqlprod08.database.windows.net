SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[publicholidays_ImportDWH] (
		[Date]            [date] NOT NULL,
		[Description]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Zone]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[publicholidays_ImportDWH] SET (LOCK_ESCALATION = TABLE)
GO
