SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NSWCutOff] (
		[Suburb_Name]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PostCode]                    [smallint] NOT NULL,
		[Working_Days]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[__of_Cycles_a_day]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Cut_off_time_for_suburb]     [time](7) NOT NULL,
		[Start_Time]                  [time](7) NOT NULL,
		[End_Time]                    [time](7) NOT NULL,
		[Profile__For_IT_Use_]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[NSWCutOff] SET (LOCK_ESCALATION = TABLE)
GO
