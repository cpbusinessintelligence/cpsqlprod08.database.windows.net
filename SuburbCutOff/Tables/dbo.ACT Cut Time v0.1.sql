SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACT Cut Time v0.1] (
		[Branch]                      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone]                        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb_Name]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]                    [int] NULL,
		[Working_Days]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[__of_Cycles_a_day]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Cut_off_time_for_suburb]     [datetime2](7) NULL,
		[Start_Time]                  [datetime2](7) NULL,
		[End_Time]                    [datetime2](7) NULL,
		[Profile__For_IT_Use_]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Primary_Delivery_Agent]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ACT Cut Time v0.1] SET (LOCK_ESCALATION = TABLE)
GO
