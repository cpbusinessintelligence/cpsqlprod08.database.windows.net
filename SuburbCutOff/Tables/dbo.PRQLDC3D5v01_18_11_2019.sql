SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PRQLDC3D5v01_18_11_2019] (
		[Postco_H2_A1_N116]      [int] NULL,
		[Suburb]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SuburbProfile]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CutOff1]                [datetime2](7) NULL,
		[CutOff2]                [datetime2](7) NULL,
		[CutOff3]                [datetime2](7) NULL,
		[Cycle_1_start_time]     [datetime2](7) NULL,
		[Cycle_1_end_time]       [datetime2](7) NULL,
		[Cycle_2_start_time]     [datetime2](7) NULL,
		[Cycle_2_end_time]       [datetime2](7) NULL,
		[Cycle_3_start_time]     [datetime2](7) NULL,
		[Cycle_3_end_time]       [datetime2](7) NULL
)
GO
ALTER TABLE [dbo].[PRQLDC3D5v01_18_11_2019] SET (LOCK_ESCALATION = TABLE)
GO
