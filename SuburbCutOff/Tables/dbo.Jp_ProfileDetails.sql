SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Jp_ProfileDetails] (
		[ProfileDetailID]     [int] NOT NULL,
		[ProfileID]           [int] NOT NULL,
		[StartTime]           [sql_variant] NOT NULL,
		[EndTime]             [sql_variant] NOT NULL,
		[IsWorking]           [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddWho]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]         [smalldatetime] NOT NULL,
		[EditWho]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]        [smalldatetime] NULL,
		[Order]               [int] NULL
)
GO
ALTER TABLE [dbo].[Jp_ProfileDetails] SET (LOCK_ESCALATION = TABLE)
GO
