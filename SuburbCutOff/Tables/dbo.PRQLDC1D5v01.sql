SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PRQLDC1D5v01] (
		[Postcode]               [int] NOT NULL,
		[Suburb]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Zone]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[State]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SuburbProfile]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CutOff1]                [datetime2](7) NOT NULL,
		[Cycle_1_start_time]     [datetime2](7) NOT NULL,
		[Cycle_1_end_time]       [datetime2](7) NOT NULL
)
GO
ALTER TABLE [dbo].[PRQLDC1D5v01] SET (LOCK_ESCALATION = TABLE)
GO
