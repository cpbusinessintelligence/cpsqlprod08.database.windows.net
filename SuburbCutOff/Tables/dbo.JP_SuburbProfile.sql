SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JP_SuburbProfile] (
		[JP_SuburbProfileID]     [int] NOT NULL,
		[ProfileCode]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ProfileDescription]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DayOfWeek]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsWorking]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NumberCycles]           [int] NOT NULL,
		[AddWho]                 [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]            [smalldatetime] NOT NULL,
		[EditWho]                [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]           [smalldatetime] NULL
)
GO
ALTER TABLE [dbo].[JP_SuburbProfile] SET (LOCK_ESCALATION = TABLE)
GO
