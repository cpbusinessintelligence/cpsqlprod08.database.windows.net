SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSuburbProfileDetails_02072019_KP] (
		[SuburbProfileDetailID]     [int] IDENTITY(1, 1) NOT NULL,
		[ProfileID]                 [int] NOT NULL,
		[IsWorking]                 [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[StartTime]                 [time](7) NOT NULL,
		[EndTime]                   [time](7) NOT NULL,
		[Order]                     [int] NULL,
		[AddWho]                    [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]               [smalldatetime] NOT NULL,
		[EditWho]                   [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]              [smalldatetime] NULL
)
GO
ALTER TABLE [dbo].[tblSuburbProfileDetails_02072019_KP] SET (LOCK_ESCALATION = TABLE)
GO
