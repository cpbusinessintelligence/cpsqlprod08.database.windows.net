SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProfileDefinition] (
		[ProfileDefinitionID]     [int] IDENTITY(1, 1) NOT NULL,
		[ProfileType]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ProfileCode]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ProfileDescription]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]                [bit] NOT NULL,
		[AddWho]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]             [smalldatetime] NOT NULL,
		[EditWho]                 [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]            [smalldatetime] NULL,
		CONSTRAINT [PK_ProfileDefinition]
		PRIMARY KEY
		CLUSTERED
		([ProfileDefinitionID])
)
GO
ALTER TABLE [dbo].[ProfileDefinition] SET (LOCK_ESCALATION = TABLE)
GO
