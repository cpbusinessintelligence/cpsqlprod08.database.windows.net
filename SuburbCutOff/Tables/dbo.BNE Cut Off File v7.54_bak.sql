SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BNE Cut Off File v7.54_bak] (
		[ID]                             [int] IDENTITY(1, 1) NOT NULL,
		[Branch]                         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone]                           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Run__]                          [int] NULL,
		[External_Reference__]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Driver_Name]                    [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email_Address]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Mobile_Number]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb_Name]                    [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]                       [int] NULL,
		[Working_Days]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[__of_Cycles_a_day]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_Cut_off_time_for_suburb]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Cut_off_time_for_suburb]        [datetime2](7) NULL,
		[Start_Time]                     [datetime2](7) NULL,
		[End_Time]                       [datetime2](7) NULL,
		[Profile__For_IT_Use_]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[In_Production__For_IT_Use_]     [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_BNE Cut Off File v7.54_bak]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[BNE Cut Off File v7.54_bak] SET (LOCK_ESCALATION = TABLE)
GO
