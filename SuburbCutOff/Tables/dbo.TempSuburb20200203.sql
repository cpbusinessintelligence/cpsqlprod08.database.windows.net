SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TempSuburb20200203] (
		[Branch]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Driver]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[External Reference #]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Driver Name]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email Address]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Mobile Number]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb Name]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Working Days]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[# of Cycles a day]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Cut off time for suburb]     [time](7) NULL,
		[Start Time]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[End Time]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Profile (For IT Use)]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Flag]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[TempSuburb20200203] SET (LOCK_ESCALATION = TABLE)
GO
