SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SuburbProfile_Backup_Delete] (
		[SuburbProfileID]     [int] IDENTITY(1, 1) NOT NULL,
		[ProfileCode]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DayOfWeek]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DayNumber]           [int] NOT NULL,
		[IsWorking]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NumberCycles]        [int] NOT NULL,
		[AddWho]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]         [smalldatetime] NOT NULL,
		[EditWho]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]        [smalldatetime] NULL,
		CONSTRAINT [PK_SuburbProfile_1]
		PRIMARY KEY
		CLUSTERED
		([SuburbProfileID])
)
GO
ALTER TABLE [dbo].[SuburbProfile_Backup_Delete] SET (LOCK_ESCALATION = TABLE)
GO
