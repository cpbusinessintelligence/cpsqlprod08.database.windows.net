SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Suburbs_202008281400_PV] (
		[SuburbsID]         [int] IDENTITY(1, 1) NOT NULL,
		[Postcode]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Suburb]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Zone]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WeekDayNumber]     [int] NULL,
		[SuburbProfile]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CutOff1]           [time](7) NULL,
		[CutOff2]           [time](7) NULL,
		[CutOff3]           [time](7) NULL,
		[CutOff4]           [time](7) NULL,
		[CutOff5]           [time](7) NULL,
		[AddWho]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]       [smalldatetime] NOT NULL,
		[EditWho]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]      [smalldatetime] NULL,
		CONSTRAINT [PK_Suburbs]
		PRIMARY KEY
		CLUSTERED
		([SuburbsID])
)
GO
CREATE NONCLUSTERED INDEX [nci_wi_Suburbs_4F2F9EC2599A733A177ABC59E9F1C41E]
	ON [dbo].[Suburbs_202008281400_PV] ([Postcode], [Suburb])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[Suburbs_202008281400_PV] SET (LOCK_ESCALATION = TABLE)
GO
