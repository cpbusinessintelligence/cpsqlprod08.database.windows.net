SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SuburbProfileDetails_20201016_PV_AfterQLD] (
		[SuburbProfileDetailID]     [int] IDENTITY(1, 1) NOT NULL,
		[ProfileID]                 [int] NOT NULL,
		[IsWorking]                 [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[StartTime]                 [time](7) NOT NULL,
		[EndTime]                   [time](7) NOT NULL,
		[Order]                     [int] NULL,
		[AddWho]                    [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]               [smalldatetime] NOT NULL,
		[EditWho]                   [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]              [smalldatetime] NULL
)
GO
ALTER TABLE [dbo].[SuburbProfileDetails_20201016_PV_AfterQLD] SET (LOCK_ESCALATION = TABLE)
GO
