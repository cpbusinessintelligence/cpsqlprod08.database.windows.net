SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VIC Cut Time Update August 2019 v0.3] (
		[Branch]                         [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone]                           [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone IT]                        [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb_Name]                    [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]                       [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Working Days]                   [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[__of_Cycles_a_day]              [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Cut_off_time_for_suburb]        [datetime] NULL,
		[Start Time]                     [datetime] NULL,
		[End Time]                       [datetime] NULL,
		[Profile__For_IT_Use_]           [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[In Production (For IT Use)]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[VIC Cut Time Update August 2019 v0.3] SET (LOCK_ESCALATION = TABLE)
GO
