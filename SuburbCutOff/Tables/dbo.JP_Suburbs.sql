SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[JP_Suburbs] (
		[JP_SuburbsID]      [int] NOT NULL,
		[Postcode]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Suburb]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ServiceCycle]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CutOff1]           [sql_variant] NULL,
		[CutOff2]           [sql_variant] NULL,
		[CutOff3]           [sql_variant] NULL,
		[CutOff4]           [sql_variant] NULL,
		[CutOff5]           [sql_variant] NULL,
		[AddWho]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]       [smalldatetime] NOT NULL,
		[EditWho]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]      [smalldatetime] NULL,
		[State]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WeekDayNumber]     [int] NULL
)
GO
ALTER TABLE [dbo].[JP_Suburbs] SET (LOCK_ESCALATION = TABLE)
GO
