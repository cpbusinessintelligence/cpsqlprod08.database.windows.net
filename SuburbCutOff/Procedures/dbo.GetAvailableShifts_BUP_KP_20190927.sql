SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAvailableShifts_BUP_KP_20190927] 

	@Date DATEtime,
	@PostCode Varchar(20),
	@Suburb Varchar(20)
as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[CPPL_RPTQueryFreightByCompany]
    --' ---------------------------
    --' Purpose: Updates Freight Exception table everyday for Freight Exception Report-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 27 -03 -2019
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                                                              Bookmark
    --' ----          ---     ---     -----                                                                                                               -------
         

    --'=====================================================================
		Declare @NextBusinessDay As varchar(20)
		Declare @NextBusinessDate As date
		Declare @Zone as Varchar(20)
	    Declare @State as Varchar(20)

		Create table #TempHolidays ( TempDate Date, [HolidayType] Varchar(20), [State/Zone Name] Varchar(20) )

		Select S.PostCode,S.Suburb,S.ZOne,S.State,SD.ProfileCode,
		    Convert(Date,Null) as DatesAvailable,
			@Date as TimetoBook,
		    SD.[DayOfWeek],
			 SD.[DayNumber],
			 SD.[SuburbProfileID],
			 SD.[NumberCycles],
			 SD.ISworking,
			 CutOff1,
			 CutOff2,
			 CutOff3,
			 CutOff4,
			 CutOff5,
			 Convert(Datetime,Null) as MaxCutOff

           into #Temp1
		   from [dbo].[Suburbs] S join [dbo].[ProfileDefinition] Pd on S.[SuburbProfile] = PD.ProfileCode
		                                Join [dbo].[SuburbProfile]  SD on PD.ProfileCode = SD.[ProfileCode]

										Where isActive =1 and SD.ISworking = 'Y' and S.Postcode = @PostCode and S.Suburb =  @Suburb

	   Select TOP 1 @State = State ,@Zone = ZOne FROM #Temp1





		INSERT INTO #TempHolidays SELECT top 20 [Date], [HolidayType] ,[State/Zone Name]    
							  FROM [SuburbCutOff].[dbo].[PublicHolidays]
							  Where Date > = @Date and Date <= Getdate() +30  order by Date asc 

	
		-- Check if any state/zone  based holidays which are not relevant
		Delete from #TempHolidays Where HolidayType =  'State' and [State/Zone Name] <> @State
		Delete from #TempHolidays Where HolidayType =  'Zone'  and [State/Zone Name] <> @Zone

	

	

	   Select @NextBusinessDay = NextBusinessDay ,@NextBusinessDate = NextBusinessDate  from [dbo].[CalculatePublicHolidays] (@Date ,@State,@Zone ,0)

		Create Table #TTT (CalendarDate Date, WeekDayName Varchar(20))

		Declare @count as integer = 0
		While @Count <  7
		  BEGIN
			  insert into #TTT Select  DateAdd(day,@count,@NextBusinessDate),DateName(dw,DateAdd(day,@count,@NextBusinessDate)) 
			  Select @Count = @Count + 1
		  END

		  Update #temp1 Set DatesAvailable = T.CalendarDate    From #Temp1 T1 Join #TTT T on T1.DayOfWeek=T.WeekDayName


		 Delete #Temp1 From #Temp1 T Join #TempHolidays H On T.DatesAvailable = H.TempDate 



	      Update #Temp1 SET MaxCutoff  =  (Case NumberCycles When 1 THEN CAST(DatesAvailable as Datetime) + CAST(CutOff1 as Datetime)
		                                                    When 2 THEN CAST(DatesAvailable as Datetime) + CAST(CutOff2 as Datetime)
															When 3 THEN CAST(DatesAvailable as Datetime) + CAST(CutOff3 as Datetime)
															When 4 THEN CAST(DatesAvailable as Datetime) + CAST(CutOff4 as Datetime)
															When 5 THEN CAST(DatesAvailable as Datetime) + CAST(CutOff5 as Datetime)
															Else CAST(DatesAvailable as Datetime) + CAST('00:00:00.000' as Datetime)
															END)
       -- Delete Records if wont fix
	   Delete #Temp1 Where MaxCutoff <= TimetoBook

	

      -- Update #Temp1 SET DateforCalculation =  [dbo].[CalculatePublicHolidays] (@Date ,State,Zone ,0) -- where DateName(dw,@Date) = DayOfWeek

	  Select t.*,SPD.StartTime ,SPD.EndTime ,SPD.[Order]
	   into #TempFinal
	   from #Temp1 T  left Join [dbo].[SuburbProfileDetails] SPD on T.SuburbProfileID =SPD.ProfileID
	   
	   --Select CAST(DatesAvailable as Datetime) +  (Case [order] WHEN 1 THEN CAST(CutOFf1 as Datetime) WHEN 2 THEN CAST(CutOFf2 as Datetime)  WHEN 3 THEN CAST(CutOFf3 as Datetime) WHEN 4 THEN CAST(CutOFf4 as Datetime) WHEN 5 THEN CAST(CutOFf5 as Datetime) ELSE CAST(EndTime as Datetime) END),* from #Tempfinal
	   Delete  #TempFinal Where CAST(DatesAvailable as Datetime) +  (Case [order] WHEN 1 THEN CAST(CutOFf1 as Datetime) WHEN 2 THEN CAST(CutOFf2 as Datetime)  WHEN 3 THEN CAST(CutOFf3 as Datetime) WHEN 4 THEN CAST(CutOFf4 as Datetime) WHEN 5 THEN CAST(CutOFf5 as Datetime) ELSE CAST(EndTime as Datetime) END) <    TimetoBook
	   

	   Select  Top 1   PostCode ,
	                    Suburb ,
						State,
						Zone , 
						CutOff1,
						CutOff2,
						CutOff3,
						CutOff4,
						CutOff5,
						TimetoBook as BookingTime,
						DayofWeek,
						--Convert(datetimeoffset(7), [dbo].[fn_ConvertDateintoUTC]((CAST(DatesAvailable as Datetime) + CAST(StartTime as Datetime)),State)) As CycleStartTime,
						--Convert(datetimeoffset(7), [dbo].[fn_ConvertDateintoUTC]((CAST(DatesAvailable as Datetime) + CAST(EndTime as Datetime)),State)) As CycleEndTime,
						CAST(DatesAvailable as Datetime) + CAST(StartTime as Datetime) As CycleStartTime,
						CAST(DatesAvailable as Datetime) + CAST(EndTime as Datetime) As CycleEndTime
	   from #TempFinal order by DatesAvailable asc , [Order] Asc
	    
 --Select  Null as   PostCode ,
	--              Null as       Suburb ,
	--				 Null as	State,
	--				 Null as	Zone , 
	--				 Null as	CutOff1,
	--				 Null as	CutOff2,
	--				 Null as	CutOff3,
	--				 Null as	CutOff4,
	--					 Null as CutOff5,
	--					 Null asBookingTime,
	--					 Null as DayofWeek,
	--					 Null as CycleStartTime,
	--					 Null as CycleEndTime


   end

--   exec GetAvailableShifts '2019-04-05 12:19:00', '2150', 'Parramatta'

--exec GetAvailableShifts '2019-04-03 11:45:00', '2000', 'Sydney'


-- exec GetAvailableShifts '2019-04-12 16:19:00', '3094', 'MONTMORENCY' 


GO
