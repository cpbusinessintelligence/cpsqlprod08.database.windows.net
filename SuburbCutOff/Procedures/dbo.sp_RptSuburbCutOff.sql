SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Praveen Valappil
-- Create date: 14/05/2020
-- Description:	To get suburb cutoff list
-- =============================================
CREATE PROCEDURE sp_RptSuburbCutOff
AS
BEGIN
	SELECT DISTINCT 
		S.[State] As [Branch]
		, S.[Zone]
		, S.[Suburb]
		, S.[Postcode]  
		, SP.[DayOfWeek]
		, CASE SPD.[Order] WHEN 1 THEN 'Cycle 1' WHEN 2 THEN 'Cycle 2' WHEN 3 THEN 'Cycle 3' WHEN 4 THEN 'Cycle 4' Else 'Cycle 5' End As [# of Cycles a day]
		, CASE SPD.[Order] WHEN 1 THEN Isnull(S.CutOff1,ISNULL(S.CutOff2,ISNULL(S.CutOff3,ISNULL(S.CutOff4,NULL)))) WHEN 2 THEN S.CutOff2 WHEN 3 THEN S.CutOff3 WHEN 4 THEN S.CutOff4 Else S.CutOff5 End As [Cut off time for suburb]
		, SPD.StartTime
		, SPD.EndTime
		, S.SuburbProfile As [Profile]
		, CASE SP.[DayOfWeek]  WHEN 'MONDAY' THEN 1 WHEN 'TUESDAY' THEN 2 WHEN 'WEDNESDAY' THEN 3 WHEN 'THURSDAY' THEN 4 WHEN 'FRIDAY' THEN 5 WHEN 'SATURDAY' THEN 6	WHEN 'SUNDAY' THEN 7 END [DayOfWeekNo]
	 FROM [dbo].[Suburbs] S
	 JOIN [SuburbProfile] SP On S.SuburbProfile = SP.ProfileCode --and SP.IsWorking = 'Y'
	 JOIN [SuburbProfileDetails] SPD On SP.SuburbProfileID = SPD.ProfileID 
	 --WHERE 
	 --S.Suburb ='Bligh Park'
	 --and SPD.[Order] = 1
	 ORDER BY [Branch],[Zone],[Suburb],[DayOfWeekNo]
END
GO
