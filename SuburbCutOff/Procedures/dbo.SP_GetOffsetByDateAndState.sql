SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SP_GetOffsetByDateAndState]
@date Datetime, @State varchar(50)
As
Begin

SELECT dbo.fn_ReturnOffsetByDateAndState(@date,@State)

End


GO
