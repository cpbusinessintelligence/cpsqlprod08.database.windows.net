SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAvailableShifts_BUP_JP_20190402] 

	@Date DATEtime,
	@PostCode Varchar(20),
	@Suburb Varchar(20)
as
begin

     --'=====================================================================
    --' CP -Stored Procedure -[CPPL_RPTQueryFreightByCompany]
    --' ---------------------------
    --' Purpose: Updates Freight Exception table everyday for Freight Exception Report-----
    --' Developer: Jobin Philip (Couriers Please Pty Ltd)
    --' Date: 27 -03 -2019
    --' Copyright: 2014 Couriers Please Pty Ltd
    --' Change Log: 
    --' Date          Who     Ver     Reason                                                                                                              Bookmark
    --' ----          ---     ---     -----                                                                                                               -------
         

    --'=====================================================================
		Declare @Profile as Varchar(20)
		Declare @CutOff1 as time(7)
		Declare @CutOff2 as time(7)
		Declare @CutOff3 as time(7)
		Declare @CutOff4 as time(7)
		Declare @CutOff5 as time(7)
		Declare @State As varchar(20)
		Declare @Zone As varchar(20)
		Declare @NextBusinessDay As varchar(20)
		Declare @NextBusinessDate As date

		---------------------------------------------

		Select @Zone =  Zone ,@CutOff1 = CutOff1, @CutOff2 = cutOff2, @cutoff3 =Cutoff3, @Cutoff4 = cutoff4, @cutoff5= cutoff5 , @profile = SuburbProfile
		from [dbo].[Suburbs] where PostCode = @PostCode and @Suburb = @Suburb

		Select @NextBusinessDay = NextBusinessDay ,@NextBusinessDate = NextBusinessDate  from [dbo].[CalculatePublicHolidays] (@Date ,@State,@Zone ,0)
			
		--Select *   from dbo.[CalculateServiceCycle] (@Profile,@NextBusinessDay,Convert(Date,@NextBusinessDate), @Date , @PostCode,@Suburb)
-----------------------------------------
		
		Select SD.ProfileCode,
		    SD.[DayOfWeek],
			 SD.[DayNumber],
			 SD.[NumberCycles],
			 SD.ISworking

    into #Temp1
		   from [dbo].[Suburbs] S join [dbo].[ProfileDefinition] Pd on S.[SuburbProfile] = PD.ProfileCode
		                                Join [dbo].[SuburbProfile]  SD on PD.ProfileCode = SD.[ProfileCode]

										Where isActive =1 and SD.ISworking = 'Y'




	if ((Select count(*) from #Temp1 where [DayofWeek] = @NextBusinessDay and NumberCycles > 0)) = 0
	    	Select @NextBusinessDay = NextBusinessDay ,@NextBusinessDate = NextBusinessDate  from [dbo].[CalculatePublicHolidays] (@NextBusinessDate ,@State,@Zone ,1)

    	if ((Select count(*) from #Temp1 where [DayofWeek] = @NextBusinessDay and NumberCycles > 0)) = 0
	    	Select @NextBusinessDay = NextBusinessDay ,@NextBusinessDate = NextBusinessDate  from [dbo].[CalculatePublicHolidays] (@NextBusinessDate ,@State,@Zone ,1)

 	if ((Select count(*) from #Temp1 where [DayofWeek] = @NextBusinessDay and NumberCycles > 0)) = 0
	    	Select @NextBusinessDay = NextBusinessDay ,@NextBusinessDate = NextBusinessDate  from [dbo].[CalculatePublicHolidays] (@NextBusinessDate ,@State,@Zone ,1)

 	if ((Select count(*) from #Temp1 where [DayofWeek] = @NextBusinessDay and NumberCycles > 0)) = 0
	    	Select @NextBusinessDay = NextBusinessDay ,@NextBusinessDate = NextBusinessDate  from [dbo].[CalculatePublicHolidays] (@NextBusinessDate ,@State,@Zone ,1)


	Select *   from dbo.[CalculateServiceCycle] (@Profile,@NextBusinessDay,Convert(Date,@NextBusinessDate), @Date , @PostCode,@Suburb)
   end

GO
