CREATE ROLE [aspnet_Membership_BasicAccess] AUTHORIZATION [dbo]
GO

EXEC sp_addrolemember @rolename=N'aspnet_Membership_BasicAccess', @membername =N'aspnet_Membership_FullAccess'

GO
