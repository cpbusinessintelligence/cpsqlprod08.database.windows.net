SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SpCppl_GetCPNotificationMaster]
@CPNotificationId int=0,
@Title Nvarchar(200)='',
@Description Nvarchar(200)='',
@FileName Nvarchar(200)='',
@Url Nvarchar(200)='',
@FromDate Nvarchar(10)='',
@ToDate Nvarchar(10)=''
As
BEGIN 
	SELECT CPNotificationId,Title,Description,FileName,Url,ThumbnailName,ThumbnailUrl,IsActive FROM tblCPNotificationMaster WHERE IsActive=1 
	AND
	CASE WHEN isnull(@CPNotificationId,0) = 0 THEN 0 ELSE isnull(CPNotificationId,0)  END = isnull(@CPNotificationId,0)
	AND
	CASE WHEN isnull(@Title,'') = '' THEN '' ELSE isnull(Title,'')  END like '%'+ isnull(@Title,'')+'%'
	AND
	CASE WHEN isnull(@Description,'') = '' THEN '' ELSE isnull(Description,'')  END like '%'+ isnull(@Description,'')+'%'
	AND
	CASE WHEN isnull(@FileName,'') = '' THEN '' ELSE isnull(FileName,'')  END like '%'+ isnull(@FileName,'')+'%'
	AND
	CASE WHEN isnull(@Url,'') = '' THEN '' ELSE isnull(Url,'')  END like '%'+  isnull(@Url,'')+'%'
	AND
	CASE WHEN isnull(@FromDate,'') = '' THEN '' ELSE CreatedDateTime  END >=   isnull(@FromDate,'')
	AND
	CASE WHEN isnull(@ToDate,'') = '' THEN '' ELSE CreatedDateTime  END <=   isnull(@ToDate,'')
	ORDER BY CPNotificationId DESC
END  
GO
