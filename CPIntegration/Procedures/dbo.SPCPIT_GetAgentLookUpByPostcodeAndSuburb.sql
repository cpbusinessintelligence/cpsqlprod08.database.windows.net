SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPIT_GetAgentLookUpByPostcodeAndSuburb]  --'PINE'
@PostCode nvarchar(50),@Suburb nvarchar(50) 
As
Begin     
	
	 SELECT * FROM tblAgentLookUp where PostCode = @Postcode and Suburb = @Suburb			 

End
GO
