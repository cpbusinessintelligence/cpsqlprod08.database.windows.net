SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

 
CREATE Procedure [dbo].[CPPL_MessageFromDriver]
 (
 @ThreadID nvarchar(200) =null,
 @Branch nvarchar(200),
  @MessageDatetime datetimeoffset(7)= null ,
  @Message nvarchar(max) =null,
 @SendBy  nvarchar(max)=null,
 @SendTo varchar(50) =null 
 )
 As
 Begin 
	Declare @ThreadIdExist tinyInt =0 , @TempThreadID int =0
	SET @ThreadIdExist =(select COUNT(*) from [dbo].[tblThread] where NowGoThreadID=@ThreadID)
	if @ThreadIdExist =0
	Begin 
		 Insert into [dbo].[tblThread] (NowGoThreadID,Branch, CreatedDateTime,SendBy,CreatedBy,ThreadName,DriverID)
		 values (@ThreadID,@Branch,@MessageDatetime,@SendBy,@SendTo,@ThreadID,@ThreadID )
		 Set @TempThreadID =(Select SCOPE_IDENTITY());
	End 
	Else 
	Begin
		select @TempThreadID=ThreadID from [dbo].[tblThread] where NowGoThreadID=@ThreadID
	end 
		Insert into tblMessage 
		(ThreadID,BookingNumber,
		SourceSystem,CPMessageID,
		Branch,MessageDatetime,
		Message,Comment,SendBy,
		SendTo,EmmId,IsProcessed,
		NowGoMessageID,CPRequest,
		CPResponse,NowGoRequest,
		NowGoResponse,UTCDateTime,
		LocalDateTime,CreatedDateTime,
		CreatedBy,UpdatedDateTime,UpdatedBy)		 
		Values(@TempThreadID,'0',
		'0',@ThreadID,
		@Branch,@MessageDatetime,
		@Message,'',@SendBy,
		@SendTo,'',1,
		@ThreadID,'',
		'','','',GETDATE(),GETDATE(),getdatE(),'CPIT',Getdate(),-1)
		
		Select @TempThreadID As ThreadID, SCOPE_IDENTITY() as MessageId





 End
GO
