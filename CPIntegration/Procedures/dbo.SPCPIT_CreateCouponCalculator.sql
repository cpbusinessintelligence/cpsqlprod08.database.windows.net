SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPIT_CreateCouponCalculator]
(
@FromPrizeZoneID int =null,
@ToPrizeZoneID int =null,
@WB1Count int =null,
@WB2Count int =null,
@WB3Count int =null,
@SatchelCount int =null,
@RedirectionFee decimal(18,4) =null,
@LocalDateTime datetime,
@CreatedBy int
)
AS
Begin

Insert into  [dbo].[tblCouponCalculator] (FromPrizeZoneID,ToPrizeZoneID,WB1Count,WB2Count,WB3Count,SatchelCount,RedirectionFee,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy)
values (@FromPrizeZoneID,@ToPrizeZoneID,@WB1Count,@WB2Count,@WB3Count,@SatchelCount,@RedirectionFee,SYSDATETIMEOFFSET(),@LocalDateTime,GETDATE(),@CreatedBy)
Select SCOPE_IDENTITY() as ID

End
GO
