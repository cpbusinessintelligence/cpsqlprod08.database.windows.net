SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_CreateDlb]
(
@PrepaidAccountID int,
@NationalLocationNumber varchar(50) =null,
@Branch varchar(50),
@Code varchar(50),
@CustomerName varchar(100),
@Location  varchar(50) ,
@LocalDateTime datetime,
@CreatedBy int
)
AS
Begin

Insert into  [dbo].[tblDLB] (NationalLocationNumber,Branch,Code,CustomerName,Location,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy)
values (@NationalLocationNumber,@Branch,@Code,@CustomerName,@Location,SYSDATETIMEOFFSET(),@LocalDateTime,GETDATE(),@CreatedBy)
Select SCOPE_IDENTITY() as DLBID

End
GO
