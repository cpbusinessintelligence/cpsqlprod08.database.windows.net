SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[SPCPIT_GetAddressByPrepaidAccIDAndAddressType] --352906
 @PrepaidAccountID int,@AddressType varchar(50)
 As
Begin
--select tbladddes.AddressID,tblpreacc.PrepaidAccountID,tbladddes.FirstName as desfirstname,tbladddes.AddressTypeID,
--tbladddes.LastName as deslastname,isnull(tbladddes.CompanyName,'') as descompname,tbladddes.Address1 as desaddress1 ,tbladddes.Suburb + ' '+tbladdstate.StateCode+' '+tbladddes.Postcode as desstatedetails,
--tbladddes.Suburb as  dessuburb ,tbladdstate.StateCode as desstatecode,tbladddes.Postcode as despostcode,
--tbladdmail.FirstName as mailfirstname,
--tbladdmail.LastName as maillastname,isnull(tbladdmail.CompanyName,'') as mailcompname,tbladdmail.Address1 as mailaddress1 ,tbladdmail.Suburb + ' '+tbladdstate.StateCode+ ' '+tbladdmail.Postcode as mailstatedetails,
--tbladdmail.Suburb as mailsuburb ,tbladdstate.StateCode as mailstate,tbladdmail.Postcode as mailpostcode
-- from tblPrepaidAccount as tblpreacc 
 
--LEFT join [dbo].[tblAddress] as tbladddes on tblpreacc.DespatchAddressID =tbladddes.AddressID
--LEFT join [dbo].[tblAddress] as tbladdmail on tblpreacc.MailingAddressID =tbladdmail.AddressID
--LEFT join [dbo].[tblState] as tbladdstate  on tbladdstate.StateID=tbladddes.State
--where    tblpreacc.PrepaidAccountID =@PrepaidAccountID

select t.AddressID,t.AddressTypeID,t.FirstName,t.LastName,t.CompanyName ,t.Email,t.Address1,t.Address2,t.Address3 ,t.Suburb,t.Postcode,StateCode AS State,
tblState.StateName, AddressType,t.Suburb + ' '+StateCode+' '+t.Postcode as AddressDetails,t.Phone,t.Mobile 
from tblprepaidaccount  as tblpreacc 
inner join tblAddress t on t.AddressID = tblpreacc.DespatchAddressID or t.AddressID = tblpreacc.MailingAddressID
inner join tblAddressType as addtype on addtype.AddressTypeID= t.AddressTypeID
inner join tblState  on tblState.StateID= t.State
where prepaidaccountid=@PrepaidAccountID and addtype.AddressType = @AddressType

End
GO
