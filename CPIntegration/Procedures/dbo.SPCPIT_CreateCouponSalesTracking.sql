SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[SPCPIT_CreateCouponSalesTracking]

       @RP nvarchar (50)= null 
      ,@SaleDateTime nvarchar (100)= null 
      ,@accountCode nvarchar(100)=null
	  ,@barcode nvarchar(100)=null
	  ,@branch varchar(100) = null
	  ,@consignmentType varchar(50) = null
	  ,@coupon nvarchar(50) = null
	  ,@customerPhoneNumber nvarchar(50)=null
	  ,@driverWareHouseNumber nvarchar(100) = null
	  ,@insuranceCode nvarchar(50) = null
	  ,@insurancevalue nvarchar(100)=null
	  ,@invoiceNumber nvarchar(100)=null
	  ,@sale nvarchar(50)= null
	  ,@transactionType nvarchar(50)= null,
	  @SalesPriceExGST nvarchar(50) =null ,
	  @GST nvarchar(50) =null,
	  @InsuranceAmount nvarchar(50) =null,
	  @SalesPriceIncGST nvarchar(50) =null,
	  @NoOfCoupon nvarchar(50) =null 
	  ,@CreatedBy varchar(50)=null
	  ,@UpdatedBy varchar(50)=null
	  
As 
Begin

INSERT INTO [dbo].[tblCouponSales]
           ([RP],[SaleDateTime],[accountCode],[barcode],[branch],[consignmentType],[coupon],[customerPhoneNumber],[driverWareHouseNumber],
		   [insuranceCode],[insurancevalue],[invoiceNumber],[sale],[transactionType],SalesPriceExGST,GST,InsuranceAmount,SalesPriceIncGST,NoOfCoupon,[CreatedBy],[CreatedDateTime],[UpdatedBy],[UpdatedDateTime])
     VALUES
           (
			@RP,@SaleDateTime,@accountCode,@barcode,@branch,@consignmentType,@coupon,@customerPhoneNumber,@driverWareHouseNumber,
			@insuranceCode,@insurancevalue,@invoiceNumber,@sale,@transactionType,@SalesPriceExGST,@GST,@InsuranceAmount,@SalesPriceIncGST,@NoOfCoupon,@CreatedBy,GetDate(),null,GetDate()
           )
Select SCOPE_IDENTITY() as ID

End







GO
