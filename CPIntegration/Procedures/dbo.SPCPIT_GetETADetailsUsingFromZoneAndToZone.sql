SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[SPCPIT_GetETADetailsUsingFromZoneAndToZone]  --'PINE'
@FromZone nvarchar(50), @ToZone nvarchar(50), @ServiceCode nvarchar(50) 
As
Begin     
	
	
                IF(@ServiceCode!=null and @ServiceCode!='')
				Begin
				SELECT * FROM tblETACalculator Where FromZone=@FromZone and ToZone=@ToZone and ServiceCode=@ServiceCode
				End
				Else
				Begin 
				SELECT * FROM tblETACalculator Where FromZone=@FromZone and ToZone=@ToZone
				End
	End


GO
