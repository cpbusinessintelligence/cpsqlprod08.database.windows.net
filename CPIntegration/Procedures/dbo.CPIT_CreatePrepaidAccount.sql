SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PRocedure [dbo].[CPIT_CreatePrepaidAccount]
@AccountCode varchar(50) =null,
@ParentAccountID int =0,
@ProntoAccountCode varchar(50)=null,
@Branch varchar(50) =null,
@OpenDays varchar(50) =null,
@OpenTime time =null,
@CloseTime time=null,
@Contractor int =0,
@Depot int=0,
@State int=0,
@Latitude varchar(50) =null,
@Longitude varchar(50) =null,
@CreatedBy int =0,
@AddFirstName varchar(50)=null,
@AddLastName varchar(50)=null,
@Address1 varchar(100)=null,
@Suburb varchar(50)=null,
@AddState varchar(20)=null,
@Postcode varchar(4)=null,
@AddCompName varchar(100) =null,
@DesFirstName varchar(50)=null ,
@DesLastName varchar(50)=null, 
@DesAddress1 varchar(100)=null,
@DesSuburb varchar(50)=null,
@DesAddState varchar(20) =null,
@DesPostcode varchar(4)=null,
@DesCompName varchar(100) =null


As
Begin
Declare @MailAddressID  int
Declare @DesAddressID  int
Declare @AddressTypeID  int
Declare @StateID  int
 
        Select @StateID =StateID from [dbo].[tblState] where StateCode =@AddState
		Select @AddressTypeID =AddressTypeID from [dbo].[tblAddressType] where AddressType ='Mailing'
		Insert into tblAddress(AddressTypeID,FirstName,LastName,Address1,Suburb,State,Postcode,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy,CompanyName)
		values (@AddressTypeID,@AddFirstName,@AddLastName,@Address1,@Suburb,@StateID,@Postcode,GETDATE(),GETDATE(),GETDATE(),@CreatedBy,@AddCompName)

		set @MailAddressID = SCOPE_IDENTITY() 

		 Select @StateID =StateID from [dbo].[tblState] where StateCode =@DesAddState
		 Select @AddressTypeID =AddressTypeID from [dbo].[tblAddressType] where AddressType ='Despatch'
		Insert into tblAddress(AddressTypeID,FirstName,LastName,Address1,Suburb,State,Postcode,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy,CompanyName)
		values (@AddressTypeID,@DesFirstName,@DesLastName,@DesAddress1,@DesSuburb,@StateID,@DesPostcode,GETDATE(),GETDATE(),GETDATE(),@CreatedBy,@DesCompName)

		set @DesAddressID = SCOPE_IDENTITY() 

		Insert into tblPrepaidAccount (AccountCode,ParentAccountID,ProntoAccountCode,Branch,DespatchAddressID,MailingAddressID,OpenDays,OpenTime,CloseTime,Contractor,Depot,State,Latitude,Longitude,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy) 
		values (@AccountCode,@ParentAccountID,@ProntoAccountCode,@Branch,@DesAddressID,@MailAddressID,@OpenDays,@OpenTime,@CloseTime,@Contractor,@Depot,@State,@Latitude,@Longitude,GETDATE(),GETDATE(),GETDATE(),@CreatedBy)


Select  SCOPE_IDENTITY() as ID , @MailAddressID as MailAddressID, @DesAddressID as DesAddressID

End
GO
