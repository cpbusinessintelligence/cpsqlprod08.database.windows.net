SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPIT_GetRedemptionZoneLookUpByPostcodeAndSuburb]  --2000 ,'Sydney'
@PostCode nvarchar(50),@Suburb nvarchar(50) 
As
Begin     
	
	 SELECT * FROM tblRedemptionZoneLookUp where UPPER(PostCode) = UPPER(@Postcode) and UPPER(Suburb) = UPPER(@Suburb)			 

End
GO
