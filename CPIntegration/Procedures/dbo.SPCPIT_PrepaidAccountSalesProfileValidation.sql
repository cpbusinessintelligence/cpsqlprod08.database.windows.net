SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPIT_PrepaidAccountSalesProfileValidation] 
	-- Add the parameters for the stored procedure here
	@inventoryCode as varchar(50),@accountCode as varchar(50),@branch as varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here	
	declare @InventoryID int,@PrepaidAccountID int
	Select @inventoryID = InventoryID from [dbo].[tblInventory] where InventoryCode = @inventoryCode and (IsActive is null or IsActive = 1)
	Select @prepaidAccountID = PrepaidAccountID from [dbo].[tblPrepaidAccount] where AccountCode = @accountCode and Branch = @branch and (IsActive is null or IsActive = 1)

	Select inv.InventoryCode as CouponPrefix,inv.[Description],pasp.SalesPrice from tblPrepaidAccountSalesProfile as pasp inner join tblInventory as inv
	on pasp.InventoryID = inv.InventoryID
	where pasp.PrepaidAccountID = @prepaidAccountID and pasp.InventoryID = @inventoryID

END
GO
