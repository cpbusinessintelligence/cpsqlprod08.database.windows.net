SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_UpdateDLB]
(
@NationalLocationNumber varchar(50),
@Branch varchar(50), 
@Code  varchar(50), 
@CustomerName  varchar(100),
@Location  varchar(50) ,
@UpdatedBy int ,
@DlbID int ,
@LocalDateTime  datetime

)
AS
Begin
BEGIN TRY
BEGIN tran 

update  tblDLB set NationalLocationNumber=@NationalLocationNumber,Branch=@Branch,Code=@Code,CustomerName=@CustomerName,Location=@Location,UpdatedDateTime =GETDATE(),UpdatedBy=@UpdatedBy,LocalDateTime=@LocalDateTime
where DlbID =@DlbID
Select 'Success' as [OutPutMsg]

COMMIT tran 
END TRY
BEGIN CATCH
begin
    rollback tran
	
	INSERT INTO [dbo].[tblErrorLog] ([Error],[ErrorCode],[ErrorType],[System], [Function],
				[UTCDateTime],[CreatedDateTime],[LocalTime],CreatedBy )
				VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,ERROR_SEVERITY(),ERROR_PROCEDURE(),
				'Local',
				'[SPCPIT_CreateSalesRep]',
				SYSDATETIMEOFFSET(),getdate(), convert(time,GETDATE(),103),-1
				)

				Select 'Error' as [OutPutMsg], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
end
END CATCH
End
GO
