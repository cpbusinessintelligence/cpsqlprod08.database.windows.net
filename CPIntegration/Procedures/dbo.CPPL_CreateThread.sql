SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



 
 CREATE Procedure [dbo].[CPPL_CreateThread]
 (
 @BookingNumber nvarchar(200) =null,
 @ThreadName varchar(200),
 @IdempotencyKey nvarchar(200) =null ,
 @DriverID nvarchar(100) ,
 @StopExtRef uniqueidentifier =null ,
 @Branch varchar(50) =null,
 @SendBy varchar(50) =null,
 @EmmID varchar(50)=null,
 @IsProcessed bit  =0,
 @driverRef nvarchar(100)=null,
 @UTCDateTime datetimeoffset(7)= null ,
 @LocalDateTime datetime= null ,
 @CPRequest nvarchar(max) =null,
 @NowGoRequest  nvarchar(max)=null,
 @CreatedBy varchar(50) =null
 )
 As
 Begin
 
 Insert into [dbo].[tblThread] (BookingNumber,ThreadName,IdempotencyKey,DriverID,StopExtRef,Branch,SendBy,EmmID,IsProcessed,driverRef,UTCDateTime,LocalDateTime,CPRequest,NowGoRequest,CreatedDateTime,CreatedBy)
 values (@BookingNumber,@ThreadName,@IdempotencyKey,@DriverID,@StopExtRef,@Branch,@SendBy,@EmmID,@IsProcessed,@driverRef,@UTCDateTime,@LocalDateTime,@CPRequest,@NowGoRequest,
 getdate(),@CreatedBy)
 Select  SCOPE_IDENTITY() as ThreadID 

 End
GO
