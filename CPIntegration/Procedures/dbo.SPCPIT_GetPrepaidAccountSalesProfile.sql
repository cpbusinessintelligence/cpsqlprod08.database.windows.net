SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_GetPrepaidAccountSalesProfile]
(
@PrepaidAccountID int
)
As
Begin

Select tblPrepaidAccountSalesProfileID,PrepaidAccountID,tblPrepaidAccountSalesProfile.InventoryID,
WarehouseCode,IsDiscountApplicable,DiscountPercentage, [InventoryCode],[Description],NoOfCoupon,CostPerCoupon,SalesPriceExGST,
convert(varchar(10), DiscountStartDate,111) as DiscountStartDate,convert(varchar(10), DiscountEndDate,111)  as DiscountEndDate,SalesPrice,tblPrepaidAccountSalesProfile.[CreatedBy] ,
case when isnull(tblPrepaidAccountSalesProfile.IsActive,1) = 1 then 'Yes' else 'No' end as Active
From   [dbo].[tblPrepaidAccountSalesProfile] inner join tblinventory on [tblPrepaidAccountSalesProfile].InventoryID =tblinventory.InventoryID  where PrepaidAccountID=@PrepaidAccountID --and IsActive<>0


End
GO
