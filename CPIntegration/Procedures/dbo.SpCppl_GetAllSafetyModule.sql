SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SpCppl_GetAllSafetyModule]
@ModuleId int=null

AS
BEGIN 
SELECT distinct md.* FROM tblDriverSafetyModule md inner join tblDriverSafetyModuleContent mdc on md.ID=mdc.DriverySafetyModuleID WHERE md.IsActive=1 and	
	CASE WHEN isnull(@ModuleId,0) = 0 THEN 0 ELSE isnull(mdc.DriverySafetyModuleID,0)  END = isnull(@ModuleId,0)	
	ORDER BY md.ID ASC
END 
GO
