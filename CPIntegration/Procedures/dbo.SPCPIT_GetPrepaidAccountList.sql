SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--exec [SPCPIT_GetPrepaidAccountList]@WhereStr =''
CREATE Procedure [dbo].[SPCPIT_GetPrepaidAccountList]
@AccountCode varchar(50)= null,
@ProntoAccountCode varchar(50)= null,
@Branch int= null,
@Depot int= null,
@State int= null,
@CompanyName varchar(100)= null
 As
Begin
select tblpreacc.PrepaidAccountID,AccountCode,ParentAccountID,ProntoAccountCode,OpenDays,convert(varchar(17),  cast(OpenTime as datetime),113) OpenTime,convert(varchar(17),  cast(CloseTime as datetime),113)CloseTime,Latitude,Longitude, tblpreacc.*,tblpreaccd.AccountDetailID,
CONVERT(varchar(10), tblpreaccd.StartDate,111) as StartDate ,CONVERT(varchar(10), tblpreaccd.LastUsed,111) as  LastUsed,Added,
tblpreaccd.AccountType as AccountTypeID,tblpreaccd.IndustryCode as IndustryCodeID,
acctype.Description as AccountType,induscode.Description as IndustryCode,case when Eft =1 then 'Yes' else 'No' end as Eft,Eft as Eftid,Eft AS EftID,
case when RegularRun =1 then 'Yes' else 'No' end as RegularRun,RegularRun AS RegularRunid,SalesCode,
case when Status =1 then 'Active' else 'In Active' end as Status,Status AS StatusID,PickupDays,CONVERT(varchar(10), RepStart,111) as RepStart  ,Comments as detailscomments,
tblprecon.*,tbltd.DepotName,[tblState].*,tbladddes.FirstName as desfirstname,
tbladddes.LastName as deslastname,isnull(tbladddes.CompanyName,'') as descompname,tbladddes.Address1 as desaddress1 ,tbladddes.Suburb + ' '+tbladdstate.StateCode+' '+tbladddes.Postcode as desstatedetails,
tbladddes.Suburb as  dessuburb ,tbladdstate.StateCode as desstatecode,tbladddes.Postcode as despostcode,
tbladdmail.FirstName as mailfirstname,
tbladdmail.LastName as maillastname,isnull(tbladdmail.CompanyName,'') as mailcompname,tbladdmail.Address1 as mailaddress1 ,tbladdmail.Suburb + ' '+tbladdstate.StateCode+ ' '+tbladdmail.Postcode as mailstatedetails,
tbladdmail.Suburb as mailsuburb ,tbladdstate.StateCode as mailstate,tbladdmail.Postcode as mailpostcode
,tblcon.FranchiseeCode
 from tblPrepaidAccount as tblpreacc 
inner join tblPrepaidAccountDetail   as tblpreaccd  on tblpreacc.PrepaidAccountID = tblpreaccd.PrepaidAccountID
inner join tblPrepaidAccountContact as tblprecon on  tblpreacc.PrepaidAccountID = tblprecon.PrepaidAccountID
inner join [dbo].[tblDepot] as tbltd  on tblpreacc.Depot=tbltd.DepotID
--inner join [dbo].[tblBranch] on tblpreacc.Branch=[tblBranch].BranchID
inner join [dbo].[tblState]  on tblpreacc.State=[tblState].StateID
inner join [dbo].[tblState] as tbladdstate  on tbladdstate.StateID=[tblState].StateID
inner join [dbo].[tblAddress] as tbladddes on tblpreacc.DespatchAddressID =tbladddes.AddressID
inner join [dbo].[tblAddress] as tbladdmail on tblpreacc.MailingAddressID =tbladdmail.AddressID
inner join [dbo].[tblFranchisee] as tblcon on   tblpreacc.Contractor =tblcon.FranchiseeID
inner join [dbo].[tblAccountType] as acctype on tblpreaccd.AccountType =acctype.AccountTypeID
inner join [dbo].tblIndustryCode as induscode on tblpreaccd.IndustryCode =induscode.IndustryCodeID
inner join [dbo].tblSalesRep  on tblpreaccd.SalesRepID =tblSalesRep.SalesRepID
where case when isnull(@AccountCode,'') = '' then '' else isnull(tblpreacc.AccountCode,'')  end = isnull(@AccountCode,'')
and case when isnull(@ProntoAccountCode,'') = '' then '' else isnull(tblpreacc.ProntoAccountCode,'')  end = isnull(@ProntoAccountCode,'')
and case when isnull(@Branch,'') = '' then '' else isnull(tblpreacc.Branch,'')  end = isnull(@Branch,'')
and case when isnull(@Depot,'') = '' then '' else isnull(tblpreacc.Depot,'')  end = isnull(@Depot,'')
and case when isnull(@State,'') = '' then '' else isnull(tblpreacc.State,'')  end = isnull(@State,'')
and case when isnull(@CompanyName,'') = '' then '' else isnull(tbladddes.CompanyName,'')  end = isnull(@CompanyName,'')
and case when isnull(@CompanyName,'') = '' then '' else isnull(tbladdmail.CompanyName,'')  end = isnull(@CompanyName,'')
and isnull(tblpreacc.IsActive,1) =1

 
End
GO
