SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SPCPIT_GetTariffAccountCode]  
@accountCode nvarchar(100)
As
Begin     
	 SELECT TariffAccountCode FROM tblTariffAccount where Accountcode = @accountCode
End

GO
