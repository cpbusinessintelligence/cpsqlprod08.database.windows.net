SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SpCppl_CreateImageMaster]
@Title Nvarchar(100)=null,
@Description Nvarchar(300)=null,
@FileName Nvarchar(100)=null,
@Url Nvarchar(500)=null,
@CreatedBy Nvarchar(50)=null,
@ThumbnailName Nvarchar(100)=null,
@ThumbnailUrl  Nvarchar(500)=null

AS

BEGIN
	INSERT INTO tblImageMaster(Title,Description,FileName,Url,CreatedDateTime,CreatedBy,ThumbnailName,ThumbnailUrl)
	VALUES(@Title,@Description,@FileName,@Url,Getdate(),IIF(ISNULL(@CreatedBy,'')='','System',@CreatedBy),@ThumbnailName,@ThumbnailUrl)
	Select @@identity
END
GO
