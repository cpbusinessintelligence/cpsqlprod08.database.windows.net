SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPIT_AddErrorLog]
@ErrorCode varchar(50)=null,
@ErrorType varchar(50)=null,
@Error varchar(max)=null,
@Function varchar(200)=null,
@System varchar(50)=null,
@Request varchar(max)=null,
@Response varchar(max)=null,
@URL varchar(max)=null,
@AffectedUser varchar(50)=null,
@UTCDateTime datetime2(7)=null,
@LocalTime datetime=null,
@CreatedBy int=null,
@ClientCode varchar(10) = null

As
Begin

INSERT INTO [dbo].[tblErrorLog]
           ([ErrorCode],[ErrorType],[Error],[Function],[System],[Request],[Response],[URL],[ClientCode],[AffectedUser],[UTCDateTime],[LocalTime],[CreatedDateTime],[CreatedBy])
     VALUES
           (@ErrorCode,@ErrorType,@Error,@Function,@System,@Request,@Response,@URL,@ClientCode,@AffectedUser,@UTCDateTime,@LocalTime,GetDate(),@CreatedBy)


End
GO
