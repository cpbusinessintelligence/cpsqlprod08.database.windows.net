SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPIT_PrepaidAccountContactsByPrepaidAccountID] 
@PrepaidAccountID int
 As
Begin
select PrepaidAccountID,ContactID,ContactType,FirstName,LastName,
 Mobile,Phone,Email,Comment ,case when  isnull(IsActive,1) =1 then 'Yes' else 'No' end as Active
  from tblPrepaidAccountContact 
where PrepaidAccountID=@PrepaidAccountID
 

 
End
GO
