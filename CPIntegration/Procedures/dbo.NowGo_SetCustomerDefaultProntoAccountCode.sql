SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NowGo_SetCustomerDefaultProntoAccountCode]
(
	@DefaultProntoAccountCode VARCHAR(20),
	@Branch VARCHAR(30)
)

AS
BEGIN

    SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRY
		UPDATE	STG
		SET 
				STG.[ProntoAccountCode] = @DefaultProntoAccountCode
		FROM	[tblCouponSalesSLSExportStaging] STG
		WHERE	[branch] = @Branch AND [ProntoAccountCode] IS NULL
	END TRY

	BEGIN CATCH

		THROW;

	END CATCH

END
GO
