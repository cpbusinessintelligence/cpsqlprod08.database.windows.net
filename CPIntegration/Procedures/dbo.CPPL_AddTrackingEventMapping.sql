SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CPPL_AddTrackingEventMapping]
@WorkFlow Nvarchar(50)='',
@OutCome Nvarchar(50)='',
@Message Nvarchar(max)='',
@PublishTo Nvarchar(50)='',
@Archive bit=0,
@CreatedBy Nvarchar(50)='-1'
AS
BEGIN	
	INSERT INTO dbo.tblTrackingEventMapping (WorkFlow,OutCome,Message,PublishTo,IsArchive,CreatedDateTime,CreatedBy)
	VALUES (@WorkFlow,@OutCome,@Message,@PublishTo,@Archive,GETDATE(),@CreatedBy)
	
	EXEC CPPL_GetTrackingEventMapping	 
END
GO
