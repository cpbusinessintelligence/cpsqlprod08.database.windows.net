SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*
*/
-- =============================================
-- Author		:	Arpan Sharma
-- Create date	:	31-July-2020
-- Description	:	To get all detail of Safetr Driver for view in CPGo project
-- =============================================
Create Procedure [dbo].[SpCppl_GetAllSafetyModuleContentByModuleID]
@ModuleId int=0

As
BEGIN 
	SELECT mdc.* FROM tblDriverSafetyModuleContent mdc inner join tblDriverSafetyModule md on md.ID=mdc.DriverySafetyModuleID WHERE mdc.IsActive=1 and	mdc.DriverySafetyModuleID=@ModuleId
	--CASE WHEN isnull(@ModuleId,0) = 0 THEN 0 ELSE isnull(mdc.DriverySafetyModuleID,0)  END = isnull(@ModuleId,0)	
	ORDER BY mdc.ID DESC
END 


GO
