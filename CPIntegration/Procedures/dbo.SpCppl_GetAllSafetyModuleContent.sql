SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*
*/
-- =============================================
-- Author		:	Arpan Sharma
-- Create date	:	31-July-2020
-- Description	:	To get all detail of Safetr Driver for view in CPGo project
-- =============================================
CREATE Procedure [dbo].[SpCppl_GetAllSafetyModuleContent]
@ModuleContentId int=null

As
BEGIN 
	SELECT mdc.* FROM tblDriverSafetyModuleContent mdc inner join tblDriverSafetyModule md on md.ID=mdc.DriverySafetyModuleID WHERE mdc.IsActive=1 and	
	CASE WHEN isnull(@ModuleContentId,0) = 0 THEN 0 ELSE isnull(mdc.ID,0)  END = isnull(@ModuleContentId,0)	
	ORDER BY mdc.ID DESC
END 


GO
