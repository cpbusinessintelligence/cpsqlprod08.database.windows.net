SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SPCPIT_GetCouponCalculatorByFromZoneToZone]  --'PINE'
@FromZone nvarchar(50),@ToZone nvarchar(50),@Condition nvarchar(50) 
As
Begin 
	 if(@Condition = 'WB1Count != null')
	 Begin
	     SELECT WB1Count,WB2Count,WB3Count,SatchelCount FROM tblCouponCalculator where FromZone = @FromZone and ToZone = @ToZone and WB1Count is not null 
	 End
	 Else if(@condition = 'WB2Count != null')
	 Begin
	     SELECT WB1Count,WB2Count,WB3Count,SatchelCount FROM tblCouponCalculator where FromZone = @FromZone and ToZone = @ToZone and WB2Count is not null 
	 End
	 Else if(@condition = 'WB3Count != null')
	 Begin
	     SELECT WB1Count,WB2Count,WB3Count,SatchelCount FROM tblCouponCalculator where FromZone = @FromZone and ToZone = @ToZone and WB3Count is not null 
	 End
	 Else if(@condition = 'SatchelCount != null')
	 Begin
	     SELECT WB1Count,WB2Count,WB3Count,SatchelCount FROM tblCouponCalculator where FromZone = @FromZone and ToZone = @ToZone and SatchelCount is not null 
	 End			
	 

End

GO
