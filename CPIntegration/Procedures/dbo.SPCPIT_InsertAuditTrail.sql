SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_InsertAuditTrail]
--@ActionPerform varchar(100),
--@EventType varchar(100),
--@TableName varchar(100),
--@ColumnName varchar(100),
--@OldValue varchar(100)=null,
--@NewValue varchar(100)=null,
--@UTCDateTime DateTimeoffset(7)=null,
--@LocalDateTime DateTime,
--@CreatedBy int,
--@CreatedDateTime DateTime=null,
--@UpdatedBy int=null,
--@UpdatedDateTime DateTime=null
@auditTrailTable [dbo].[AuditTrailTable] readonly
As
Begin
Begin Try
BEGIN TRAN 

		INSERT [dbo].[tblEventAudtTrail] 
			   ([EventType], [TableName], [ColumnName], [OldValue], [NewValue], [UTCDateTime], [LocalDateTime],[CreatedBy],[CreatedDateTime]) 
		Select [EventType], [TableName], [ColumnName], [OldValue], [NewValue], SYSDATETIMEOFFSET(), [LocalDateTime],[CreatedBy],GETDATE() from @auditTrailTable


		--select * from @auditTrailTable
		
		Select 'Success' as [Status]

COMMIT TRAN 

END TRY
		BEGIN CATCH
			begin
				rollback tran
				--INSERT INTO [dbo].[tblErrorLog] ([Error],[Function])
				--VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,'[SPCPIT_InsertAuditTrail]')

				Select 'Error' as [Status], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
			end
		END CATCH

END 

--Declare 
--@EventType varchar(100)='Create',
--@TableName varchar(100)='tblPrepaidAccount',
--@ColumnName varchar(100)='AccountCode',
--@OldValue varchar(100)=null,
--@NewValue varchar(100)='321',
--@UTCDateTime DateTimeoffset(7)='26-Apr-18 12:00:00 AM',
--@LocalDateTime DateTime='26-Apr-18 12:00:00 AM',
--@CreatedBy int=-1,
--@CreatedDateTime DateTime=getdate(),
--@UpdatedBy int=null,
--@UpdatedDateTime DateTime=null
--Exec [SPCPIT_InsertAuditTrail] @EventType, @TableName, @ColumnName,@OldValue,@NewValue,@UTCDateTime,
--@LocalDateTime,@CreatedBy,@CreatedDateTime, @UpdatedBy, @UpdatedDateTime



GO
