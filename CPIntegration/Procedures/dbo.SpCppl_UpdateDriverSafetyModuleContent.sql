SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SpCppl_UpdateDriverSafetyModuleContent]
@ID int=0,
@DriverySafetyModuleID int=0,
@Title varchar(200)=null,
@ImagePath Nvarchar(500)=null,
@YouTubeLink Nvarchar(500)=null,
@UpdatedBy Nvarchar(50)=null

AS

BEGIN	

	Update tblDriverSafetyModuleContent Set DriverySafetyModuleID=@DriverySafetyModuleID, Title=@Title, ImagePath=@ImagePath, YouTubeLink=@YouTubeLink,  UpdatedBy=@UpdatedBy, UpdatedDateTime=GETDATE() where ID=@ID

	exec SpCppl_GetAllSafetyModuleContent
END
GO
