SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[SPCPIT_GetPrepaidAccountDeatilsByAccountID]
@PrepaidAccountID int
 As
Begin
select tblpreaccd.PrepaidAccountID,AccountDetailID, 
CONVERT(varchar(10), tblpreaccd.StartDate,111) as StartDate ,CONVERT(varchar(10), tblpreaccd.LastUsed,111) as  LastUsed,Added,
tblpreaccd.AccountType as AccountType,tblpreaccd.IndustryCode as IndustryCode,
acctype.Description as AccountTypedesc,induscode.Description as IndustryCodedesc,
case when Eft =1 then 'Yes' else 'No' end as Eft,Eft as Eftid,
case when RegularRun =1 then 'Yes' else 'No' end as RegularRun,RegularRun AS RegularRunid,tblSalesRep.SalesRepName  ,SalesCode,tblpreaccd.SalesRepID,
case when Status =1 then 'Active' else 'In Active' end as Status,Status AS StatusID,PickupDays,CONVERT(varchar(10), RepStart,111) as RepStart  ,Comments 
  from tblPrepaidAccountDetail as tblpreaccd
  LEFT join [dbo].[tblAccountType] as acctype on tblpreaccd.AccountType =acctype.AccountTypeID
LEFT join [dbo].tblIndustryCode as induscode on tblpreaccd.IndustryCode =induscode.IndustryCodeID
LEFT join [dbo].tblSalesRep  on tblpreaccd.SalesRepID =tblSalesRep.SalesRepID
where tblpreaccd.PrepaidAccountID=@PrepaidAccountID
 

 
End
GO
