SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[SPCPIT_GetETAZoneLookUpByPostcodeAndSuburb]  --'PINE'
@PostCode nvarchar(50),@Suburb nvarchar(50) 
As
Begin     
	
	 SELECT * FROM tblETAZoneLookUp where PostCode = @Postcode and Suburb = @Suburb			 

End
GO
