SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SPCPIT_GetPrepaidCustomerbyAccCodeAndBranch] --'5858','Goldcoast'
@AccountCode varchar(50),
@Branch nvarchar(50) 
--and IsActive=0
As
Begin
IF EXISTS (SELECT * FROM tblPrepaidAccount WHERE AccountCode = @AccountCode) 
BEGIN
select tpa.PrepaidAccountID, tpa.AccountCode,tpa.ParentAccountID,tpa.ProntoAccountCode,tpa.Branch,tpa.DespatchAddressID,tpa.MailingAddressID,
tpa.OpenDays,tpa.OpenTime,tpa.CloseTime,tpa.Contractor,td.DepotName as Depot,ts.StateCode as State,tpa.Latitude,tpa.Longitude,
'Valid Customer' as IsValidCustomerAccountCode,tpa.IsActive 
from tblPrepaidAccount tpa 
Left join tblDepot td on tpa.Depot = td.DepotID   
Left join tblState ts on tpa.State = ts.StateID
where tpa.AccountCode=@AccountCode and tpa.Branch=@Branch 
and (tpa.IsActive = 1 or tpa.IsActive is null)
END
ELSE
BEGIN
select top (1) 'Invalid Customer' as IsValidCustomerAccountCode from tblPrepaidAccount
END



End
GO
