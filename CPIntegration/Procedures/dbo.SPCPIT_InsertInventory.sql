SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPIT_InsertInventory]
(
@InvetoryCode varchar(50),
@Description varchar(100),
@SalesPriceExGST decimal(18,4),
@GST decimal(18,4),
@SalesPriceIncGST decimal(18,4),
@LocalDateTime DateTime,
@CreatedBy int
)
As
Begin
Begin Transaction
Begin Try
Insert into [dbo].[tblInventory] (InventoryCode,[Description],SalesPriceExGST,
[UTCDateTime],[LocalDateTime],
[CreatedDateTime],[CreatedBy])
values (@InvetoryCode,@Description,@SalesPriceExGST,SYSDATETIMEOFFSET(),GETDATE(),GETDATE(),@CreatedBy)
commit Transaction
Select SCOPE_IDENTITY () as ID
End Try
Begin Catch
rollback Transaction
End Catch

End
GO
