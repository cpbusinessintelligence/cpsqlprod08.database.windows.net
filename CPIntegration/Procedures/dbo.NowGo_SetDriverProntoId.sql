SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NowGo_SetDriverProntoId]

AS
BEGIN

    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRY
		-- GET Driver's Pronto ID
		UPDATE	STG
		SET 
				STG.[ProntoId] = CNT.[ProntoId]
		FROM	[tblCouponSalesSLSExportStaging] STG
		INNER JOIN [dbo].[tblContractor] CNT
		ON		STG.[DriverRunNumber] = CNT.[DriverNumber]
		AND		TRIM(STG.[branch]) =  TRIM(CNT.[branch]);

	END TRY

	BEGIN CATCH
	
		THROW;

	END CATCH	
END
GO
