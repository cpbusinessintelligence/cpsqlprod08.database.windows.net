SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[NowGo_SetDriverNumberAndBranchFromDriverRef]

AS
BEGIN

    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRY
		-- Extract Driver Number And Branch from Driver Ref
		UPDATE	STG
		SET 
				[DriverRunNumber] = [dbo].[fn_NowGo_Get_Driver_Run_Number]([driverWareHouseNumber]),
				[branch] = [dbo].[fn_NowGo_Get_Branch]([driverWareHouseNumber])
		FROM	[tblCouponSalesSLSExportStaging] STG;		

	END TRY

	BEGIN CATCH
	
		THROW;

	END CATCH	
END
GO
