SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_InsertServiceAccess]
@ServiceName varchar(100),
@ClientCode varchar(100),
@ServiceCode varchar(100) = null,
@ServiceURL varchar(100),
@APIUserAccess bit = 0

As
Begin
Begin Try
BEGIN TRAN 

	Declare @ClientID int 
	select @ClientID = ClientID from tblClient where ClientCode = @ClientCode 
	If @ServiceCode is null 
	begin
		select @ServiceCode = @ClientCode + (select  cast(isnull( max(serviceid),0)+1 as varchar) from tblServices) + format(getdate(),'ddMMyyyyHHmmss')
	end

		Declare @ServiceID int

		INSERT [dbo].[tblServices] ([ServiceName], [ServiceCode], [ServiceURL], [CreatedDateTime], [CreatedBy], [UpdatedDateTime], [UpdatedBy]) 
		VALUES (@ServiceName, @ServiceCode, @ServiceURL, getdate(), -1, NULL, NULL)

		set @ServiceID = scope_identity()

		INSERT [dbo].[tblClientServiceAccess] ([ClientID], [ServiceID],[IsActive], [CreatedDateTime], [CreatedBy], [UpdatedDateTime], [UpdatedBy]) 
		VALUES (@ClientID, @ServiceID, 1, getdate(), -1, NULL, NULL)

	if @APIUserAccess = 1
	begin
		INSERT [dbo].[tblClientServiceAccess] ([ClientID], [ServiceID], [IsActive], [CreatedDateTime], [CreatedBy], [UpdatedDateTime], [UpdatedBy]) 
		VALUES (6, @ServiceID, 1, getdate(), -1, NULL, NULL)
	end
		
		Select 'Success' as [Status], @ServiceCode as NewServiceCode

COMMIT TRAN 

END TRY
		BEGIN CATCH
			begin
				rollback tran
				INSERT INTO [dbo].[tblErrorLog] ([Error],[Function],[ClientCode])
				VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,'[SPCPPL_InsertServiceAccess]', @ClientCode)

				Select 'Error' as [Status], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
			end
		END CATCH

END 

--Declare 
--@ServiceName varchar(100)='ETACalculatorZoneLevelService',
--@ClientCode varchar(100)='CPIT',
--@ServiceCode varchar(100) = null, -- If it is null then system will generate automatically
--@ServiceURL varchar(100)='api/v1/ETACalculatorZoneLevelServices',
--@APIUserAccess bit = 0
--Exec [SPCPIT_InsertServiceAccess] @ServiceName, @ClientCode, @ServiceCode, @ServiceURL, @APIUserAccess


GO
