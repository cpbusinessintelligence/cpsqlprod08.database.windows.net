SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_CreatePrepaidAccountContact]
(
@PrepaidAccountID int,
@ContactType varchar(50),
@FirstName varchar(50),
@LastName varchar(50),
@Mobile varchar(10),
@Phone varchar(10),
@Email varchar(150),
@Comment varchar(100),
@CreatedBy int 
)
As
Begin

Insert into [dbo].[tblPrepaidAccountContact] (PrepaidAccountID,ContactType,FirstName,LastName,Mobile,Phone,Email,Comment,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy)
values (@PrepaidAccountID,@ContactType,@FirstName,@LastName,@Mobile,@Phone,@Email,@Comment,GETDATE(),GETDATE(),GETDATE(),@CreatedBy)

Select SCOPE_IDENTITY () as ID

End
GO
