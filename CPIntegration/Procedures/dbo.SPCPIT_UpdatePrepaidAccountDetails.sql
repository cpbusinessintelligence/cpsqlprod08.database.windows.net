SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[SPCPIT_UpdatePrepaidAccountDetails]
(
@SalesCode varchar(50),
@AccountType int,
@IndustryCode int,
@LocalDateTime datetime,
@UpdatedBy int,
@PrepaidAccountID int,
@StartDate datetime,
@LastUsed datetime,
@Added varchar(50),
@Eft bit,
@Status bit,
@RegularRun bit, 
@PickupDays varchar(50),
@RepStart datetime,
@Comments varchar(100)

)
AS
Begin
Begin Try
BEGIN TRAN 

Declare @SalesRepID Int

Select @SalesRepID = SalesRepID from [dbo].[tblPrepaidAccountDetail] where PrepaidAccountID = @PrepaidAccountID

UPDATE [dbo].[tblSalesRep]
   SET [SalesCode] = @SalesCode,
       [AccountType] = @AccountType,
       [IndustryCode] = @IndustryCode,
       [UTCDateTime] = SYSDATETIMEOFFSET(),
       [LocalDateTime] = @LocalDateTime,
       [UpdatedDateTime] = GETDATE(),
       [UpdatedBy] = @UpdatedBy
 WHERE SalesRepID = @SalesRepID


UPDATE [dbo].[tblPrepaidAccountDetail]
   SET [StartDate] = @StartDate,
       [LastUsed] = @LastUsed,
       [Added] = @Added,
       [AccountType] = @AccountType,
       [IndustryCode] = @IndustryCode,
       [Eft] = @Eft,
       [Status] = @Status,
       [RegularRun] = @RegularRun,
       [PickupDays] = @PickupDays,
       [SalesRepID] = @SalesRepID,
       [RepStart] = @RepStart,
       [Comments] = @Comments,
       [UTCDateTime] = SYSDATETIMEOFFSET(),
       [LocalDateTime] = @LocalDateTime,     
       [UpdatedDateTime] = GETDATE(),
       [UpdatedBy] = @UpdatedBy
 WHERE [PrepaidAccountID] = @PrepaidAccountID

 Select 'Success' as [Status]

COMMIT TRAN 

END TRY
		BEGIN CATCH
			begin
				rollback tran
				INSERT INTO [dbo].[tblErrorLog] ([Error],[Function])
				VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,'[SPCPIT_UpdatePrepaidAccountDetails]')

				Select 'Error' as [Status], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
			end
		END CATCH
End
GO
