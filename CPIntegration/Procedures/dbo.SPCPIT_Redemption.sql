SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPIT_Redemption]
	@ContractorCode varchar(50),
	@Activity varchar(50) =null,
	@Fromdate varchar(100)=null,
	@Todate varchar(100)=null
AS
BEGIN

	Select [ContractorCode],[Activity],[ActivityDate],[Category],[CouponType],[Count],[Amount],CreatedDateTime from [tblRedemption]
	where
	[ContractorCode]= @ContractorCode
	 and
	 case when isnull(@activity,'') = '' then '' else isnull([Activity],'')  end = isnull(@activity,'')
	 and 
	  ((
            @fromdate is not null 
            and 
            @todate is not null
            and 
            [ActivityDate] between CONVERT(date, @fromdate,103)   AND   CONVERT(date, @todate,103) 
        )
        or
        (
           (
                @fromdate is null 
                or 
                @todate  is null
            )
            and 
            CONVERT(date, [ActivityDate],103) = CONVERT(date, getdate(), 103)

			))
			order by CreatedDateTime desc

END
GO
