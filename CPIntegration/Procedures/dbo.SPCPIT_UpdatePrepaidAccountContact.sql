SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_UpdatePrepaidAccountContact]
(
@PrepaidAccountID int,
@ContactType varchar(50),
@FirstName varchar(50),
@LastName varchar(50),
@Mobile varchar(10),
@Phone varchar(10),
@Email varchar(150),
@Comment varchar(100),
@LocalDateTime datetime,
@UpdatedBy int 
)
As
Begin
Begin Try
BEGIN TRAN 

UPDATE [dbo].[tblPrepaidAccountContact]
   SET [ContactType] = @ContactType,
       [FirstName] = @FirstName,
       [LastName] = @LastName,
       [Mobile] = @Mobile,
       [Phone] = @Phone,
       [Email] = @Email,
       [Comment] = @Comment,
       [UTCDateTime] = SYSDATETIMEOFFSET(),
       [LocalDateTime] = @LocalDateTime,      
       [UpdatedDateTime] = GETDATE(),
       [UpdatedBy] = @UpdatedBy
 WHERE [PrepaidAccountID] = @PrepaidAccountID

 Select 'Success' as [Status]

COMMIT TRAN 

END TRY
		BEGIN CATCH
			begin
				rollback tran
				INSERT INTO [dbo].[tblErrorLog] ([Error],[Function])
				VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,'[SPCPIT_UpdatePrepaidAccountContact]')

				Select 'Error' as [Status], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
			end
		END CATCH


End
GO
