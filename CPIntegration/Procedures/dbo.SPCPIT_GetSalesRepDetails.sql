SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
--exec [SPCPIT_GetSalesRepDetails]  @SalesCode =null,@SalesRepName =null,@AccountType =null,@IndustryCode=null
CREATE Procedure [dbo].[SPCPIT_GetSalesRepDetails]
@SalesCode varchar(50) =null,
@SalesRepName varchar(100)=null,
@AccountType int =0,
@IndustryCode int =0


As
Begin

Select SalesRepID ,SalesCode,SalesRepName,acctype.AccountType,inscode.IndustryCode,AccountTypeID,IndustryCodeID,acctype.Description as accounttypedesc,inscode.Description as inscodedescription
 from [dbo].[tblSalesRep] as saledrep
inner join [dbo].[tblAccountType] as acctype on saledrep.AccountType =acctype.AccountTypeID
inner join [dbo].[tblIndustryCode] as inscode on saledrep.IndustryCode =inscode.IndustryCodeID
where case when isnull(@SalesCode,'') = '' then '' else isnull(saledrep.SalesCode,'')  end = isnull(@SalesCode,'')
and case when isnull(@SalesRepName,'') = '' then '' else isnull(saledrep.SalesRepName,'')  end = isnull(@SalesRepName,'')
and case when isnull(@AccountType,'') = '' then '' else isnull(saledrep.AccountType,0)  end = isnull(@AccountType,'')
and case when isnull(@IndustryCode,'') = '' then '' else isnull(saledrep.IndustryCode,0)  end = isnull(@IndustryCode,'')
and isnull(saledrep.IsActive,1) =1

End

GO
