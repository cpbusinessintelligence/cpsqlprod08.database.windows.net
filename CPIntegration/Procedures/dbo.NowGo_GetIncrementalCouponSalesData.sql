SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NowGo_GetIncrementalCouponSalesData]
(
	@GetIncrementalDataInHours INT
)

AS
BEGIN

    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRY
		
	SELECT 
			 [ID]
			,[RP]
			,CONVERT(NVARCHAR(100), FORMAT(CONVERT(DATETIME, CONVERT(DATETIME, [CreatedDateTime], 103) AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'), 'dd/MM/yyyy HH:mm:ss')) [SaleDateTime]
			,[accountCode]
			,[barcode]
			,[branch]
			,[consignmentType]
			,[coupon]
			,[customerPhoneNumber]
			,[driverWareHouseNumber]
			,[insuranceCode]
			,[insurancevalue]
			,[invoiceNumber]
			,[sale]
			,[transactionType]
	FROM	[tblCouponSales] WITH (NOLOCK)
	WHERE	[CreatedDateTime] >= DATEADD( HOUR, - @GetIncrementalDataInHours, GETDATE())
	AND 	[IsExtractedToPronto] = 0

	END TRY

	BEGIN CATCH
	
		THROW;

	END CATCH	
END
GO
