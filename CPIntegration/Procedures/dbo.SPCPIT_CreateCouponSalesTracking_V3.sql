SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SPCPIT_CreateCouponSalesTracking_V3]
@json NVARCHAR(max)
As 
Begin
SET NOCOUNT ON;
       SET XACT_ABORT ON;
 BEGIN TRAN
    BEGIN TRY
  INSERT INTO [dbo].[tblCouponSales] (
  [RP],
  [SaleDateTime],
  [accountCode],
  [barcode],
  [branch],
  [consignmentType],
  [coupon],
  [customerPhoneNumber],
  [driverWareHouseNumber],
  [insuranceCode],
  [insurancevalue],
  [invoiceNumber],
  [sale],
  [transactionType],
  SalesPriceExGST,
  GST,
  InsuranceAmount,
  SalesPriceIncGST,
  NoOfCoupon,
  CouponSalesDetails,
  [CreatedBy],
  [CreatedDateTime],
  [UpdatedBy],
  [UpdatedDateTime]
		   )

    SELECT
        [RP]='',
		[SaleDateTime],
		[accountCode],
		[barcode],
		[branch],
		[consignmentType]='CP',
		[coupon]='',
		[customerPhoneNumber],
		[driverNumber],
		   [insuranceCode],
		   [insurancevalue],
		   [invoiceNumber],
		   [sale]='S',
		   [transactionType]='S',
		   SalesPriceExGST,
		   GST,
		   InsuranceAmount,
		   SalesPriceIncGST,
		   NoOfCoupon,
		   CouponSalesDetails,
		   [CreatedBy]='-1',
		   [CreatedDateTime] = GetDate(),
		   [UpdatedBy]='',
		   [UpdatedDateTime] = GetDate()
    FROM OPENJSON(@json)
    WITH (
	[RP] nvarchar(50),
		[SaleDateTime] nvarchar(100) '$.saleDateTime',
		[accountCode] nvarchar(100) '$.accountCode',
		driverNumber VARCHAR(33) '$.driverNumber',
		[branch] varchar(100) '$.branch',
		[consignmentType] varchar(50) ,
		[coupon] nvarchar(50) ,
		[customerPhoneNumber] nvarchar(50) '$.customerPhoneNumber',
		   [insurancevalue] nvarchar(100),
		   [invoiceNumber] nvarchar(100) '$.invoiceNumber',
		   [sale] nvarchar(50),
		   [transactionType] nvarchar(50),
		   CouponSalesDetails NVARCHAR(Max) '$.CouponSalesDetails' As JSON,
		   [CreatedBy] varchar(50),
		   [CreatedDateTime] datetime,
		   [UpdatedBy] varchar(50),
		   [UpdatedDateTime] datetime
   
    ) AS jsonValues
	CROSS APPLY OPENJSON (CouponSalesDetails) WITH (
	   barcode VARCHAR(33) '$.barcode',
	   insuranceCode VARCHAR(33) '$.insuranceCode',
	   salesPriceExGST VARCHAR(33) '$.salesPriceExGST',
	   GST VARCHAR(33) '$.GST',
	   insuranceAmount VARCHAR(33) '$.insuranceAmount',
	   salesPriceIncGST VARCHAR(33) '$.salesPriceIncGST',
	   noOfCoupon VARCHAR(33) '$.noOfCoupon'
	   )

         select 'Coupon sales transaction successful.' as TransactionStatus
    COMMIT TRAN;
  END TRY
  BEGIN CATCH
              ROLLBACK TRAN;
	      	  select 'Coupon sales transaction rolled back.' as TransactionStatus
  END CATCH
		       
End

GO
