SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:      Praveen Valappil
-- Create Date: 2020-09-04
-- Description: <Description, , >
-- =============================================
CREATE PROCEDURE [dbo].[CP_GET_ETACALCULATOR_JSON]
AS
BEGIN
	SELECT JSONString FROM
		(SELECT 
			[ETAID],
				(SELECT    
					[ETAID]
					,[FromZone]
					,[ToZone]
					,[PrimaryNetworkCategory]
					,[SecondaryNetworkCategory]
					,[ETA]
					,[FromETA]
					,[ToETA]
					,[1stPickupCutOffTime]
					,[1stDeliveryCutOffTime]
					,[1stAllowedDays]
					,[2ndPickupCutOffTime]
					,[2ndDeliveryCutOffTime]
					,[2ndAllowedDays]
					,[AIR_ETA]
					,[AIR_FromETA]
					,[AIR_ToETA]
					,[AIR_1stDeliveryCutOffTime]
					,[AIR_1stAllowedDays]
					,[AIR_2ndDeliveryCutOffTime]
					,[AIR_2ndAllowedDays]
					,[SameDay_ETA]
					,[SameDay_FromETA]
					,[SameDay_ToETA]
					,[SameDay_1stDeliveryCutOffTime]
					,[SameDay_1stAllowedDays]
					,[SameDay_2ndDeliveryCutOffTime]
					,[SameDay_2ndAllowedDays]
					,[AddWho]
					,[AddDateTime]
					,[EditWho]
					,[EditDateTime]
					,[Domestic_OffPeak_ETA]
					,[Domestic_OffPeak_FromETA]
					,[Domestic_OffPeak_ToETA]
					,[Domestic_OffPeak_1stDeliveryCutOff]
					,[Domestic_OffPeak_1stAllowedDays]
					,[Domestic_OffPeak_2ndDeliveryCutOff]
					,[Domestic_OffPeak_2ndAllowedDays]
				FROM [dbo].[ETACalculator] WITH (NOLOCK)
				WHERE [ETAID] = RelationalJSONData.ETAID
				FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER)JSONString
			FROM [ETACalculator]  AS RelationalJSONData  WITH (NOLOCK) 
			--WHERE  [Id] BETWEEN @Lower AND @Upper
			) AS
	JSONOnly
END
GO
