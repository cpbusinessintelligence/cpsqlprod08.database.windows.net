SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPIT_InsertPrepaidAccountSalesProfile]
(
@PrepaidAccountID int,
@InventoryID int,
@IsDiscountApplicable bit,
@DiscountPercentage decimal(18,4),
@DiscountStartDate datetime,
@DiscountEndDate datetime,
@SalesPrice decimal(18,4),
@LocalDateTime DateTime,
@CreatedBy int
)
As
Begin
Begin Transaction
Begin Try
Insert into [dbo].[tblPrepaidAccountSalesProfile] 
(PrepaidAccountID,InventoryID,IsDiscountApplicable,DiscountPercentage,DiscountStartDate,DiscountEndDate,SalesPrice,
[UTCDateTime],[LocalDateTime],[CreatedDateTime],[CreatedBy])
values (@PrepaidAccountID,@InventoryID,@IsDiscountApplicable,@DiscountPercentage,@DiscountStartDate,
@DiscountEndDate,@SalesPrice,
SYSDATETIMEOFFSET(),GETDATE(),GETDATE(),@CreatedBy)
commit Transaction
Select SCOPE_IDENTITY () as ID
End Try
BEGIN CATCH
			begin
				rollback Transaction
				INSERT INTO [dbo].[tblErrorLog] ([Error],[ErrorCode],[ErrorType],[System], [Function],
				[UTCDateTime],[CreatedDateTime],[LocalTime],CreatedBy )
				VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,ERROR_SEVERITY(),ERROR_PROCEDURE(),
				'Local',
				'[SPCPIT_InsertPrepaidAccountSalesProfile]',
				SYSDATETIMEOFFSET(),getdate(), convert(time,GETDATE(),103),-1
				)

				Select 'Error' as [Status], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
			end
		END CATCH


End

GO
