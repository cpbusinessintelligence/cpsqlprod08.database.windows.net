SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPIT_UpdateCancelBooking]

@CancelBookingID int,
@NowGoJobID varchar(50) = null,
@UpdatedBy varchar(50)= null,
@Response varchar(max) = null,
@NowGoRequest varchar(max) = null,
@NowGoResponse varchar(max) = null,
@DriverExtRef varchar(100)=null,
@UTCDateTime datetimeoffset(7)= null ,
@LocalDateTime datetime= null,
@IsClosed bit=0 
As 
Begin
SET NOCOUNT ON;
   SET XACT_ABORT ON;
     BEGIN TRANSACTION
	   	SAVE TRANSACTION SavePoint;
           BEGIN TRY	
                    Update [tblCancelBooking] set [NowGoJobID] = @NowGoJobID, [IsClosed]  = @IsClosed ,[UpdatedDateTime]=GetDate(), [UpdatedBy] = @UpdatedBy, [Response] = @Response, 
					[NowGoRequest] = @NowGoRequest, [NowGoResponse] = @NowGoResponse,  [DriverExtRef] = @DriverExtRef, [UTCDateTime] = @UTCDateTime, [LocalDateTime] = @LocalDateTime
                    where [CancelBookingID] = @CancelBookingID

                    Select SCOPE_IDENTITY() as CancelBookingID
     COMMIT TRANSACTION			
           END TRY  
       BEGIN CATCH
            ROLLBACK TRANSACTION;
	   END CATCH

End
GO
