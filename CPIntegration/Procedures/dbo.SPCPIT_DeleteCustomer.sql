SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_DeleteCustomer]
@PrepaidAccountID int,
@UserID int
AS
Begin
BEGIN TRY
BEGIN tran 

Declare @DespatchAddressID int 
Declare @MailingAddressID int 
update tblPrepaidAccountContact set IsActive=0,InActiveDateTime=GETDATE(),UpdatedDateTime=GETDATE(),UpdatedBy=@UserID  where PrepaidAccountID=@PrepaidAccountID
--delete from tblPrepaidAccountContact where  PrepaidAccountID=@PrepaidAccountID
--delete from tblPrepaidAccountDetail where  PrepaidAccountID=@PrepaidAccountID
Select @DespatchAddressID =DespatchAddressID,@MailingAddressID=MailingAddressID from  tblPrepaidAccount where PrepaidAccountID=@PrepaidAccountID
update tblPrepaidAccount set IsActive=0,InActiveDateTime=GETDATE(),UpdatedDateTime=GETDATE(),UpdatedBy=@UserID where PrepaidAccountID=@PrepaidAccountID
--delete from tblPrepaidAccount where  PrepaidAccountID=@PrepaidAccountID
update tblAddress set IsActive=0,InActiveDateTime=GETDATE(),UpdatedDateTime=GETDATE(),UpdatedBy=@UserID where AddressID = @DespatchAddressID
update tblAddress set IsActive=0,InActiveDateTime=GETDATE(),UpdatedDateTime=GETDATE(),UpdatedBy=@UserID where AddressID = @MailingAddressID
--delete  from tblAddress where AddressID = @DespatchAddressID
--delete  from tblAddress where AddressID = @MailingAddressID
COMMIT tran 

Select 'Success' as OutPutMsg 
END TRY
BEGIN CATCH
begin
    rollback tran
	
	INSERT INTO [dbo].[tblErrorLog]
           ([Error]
           ,[Function]
           )
     VALUES
           (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
           ,'SPCPIT_DeleteCustomer'
           )
		   Select 'Error' as OutPutMsg 
end
END CATCH

End

select * from tblPrepaidAccountContact
select * from tblPrepaidAccountDetail
select * from tblPrepaidAccount
select * from tblAddress

GO
