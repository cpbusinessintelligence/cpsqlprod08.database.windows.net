SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_GetPrepaidCustomerbyID]
@PrepaidAccountID int 
As
Begin
select tpa.*,ta.AddressID as DesaddID, ta.AddressTypeID as DesAddressTypeID,ta.FirstName as DesFirstName,ta.LastName as DesLastName,ta.Address1 as DesAddress1,ta.Suburb as DesSuburb,
ta.Postcode as DesPostcode,taddmail.AddressID as MailaddID, taddmail.AddressTypeID as MailAddressTypeID,taddmail.FirstName as MailFirstName,taddmail.LastName as MailLastName,
taddmail.Address1 as MailAddress1,taddmail.Suburb as MailSuburb,taddmail.Postcode as MailPostcode  ,tpad.StartDate,tpad.LastUsed,tpad.Added,tpad.AccountType,tpad.IndustryCode,
tpad.Eft,tpad.Status,tpad.RegularRun,tpad.PickupDays,tpad.SalesRepID,tpad.RepStart,tpad.Comments as Accountdetailscomments,tpac.ContactType,tpac.FirstName as confirstname ,
tpac.LastName as conlastname,tpac.Mobile as conmobile,tpac.Phone as conPhone,tpac.Email as conemail, tpac.Comment as conComment 
from tblPrepaidAccount  tpa 
inner join tbladdress ta on  tpa.DespatchAddressID =ta.AddressID
inner join tbladdress taddmail on  tpa.MailingAddressID =taddmail.AddressID
inner join tblPrepaidAccountDetail as tpad on tpa.PrepaidAccountID =tpad.PrepaidAccountID
inner join tblPrepaidAccountContact as tpac on tpa.PrepaidAccountID =tpac.PrepaidAccountID
where tpa.PrepaidAccountID =@PrepaidAccountID

End
GO
