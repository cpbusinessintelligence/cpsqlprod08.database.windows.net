SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_GetDLBDetails]
@NationalLocationNumber varchar(50),
@Branch varchar(50),
@Code varchar(50),
@CustomerName varchar(100),
@Location varchar(50)
 
As
Begin

select  DlbID,PrepaidAccountID,NationalLocationNumber,Branch,Code,CustomerName,Location   from  [dbo].[tblDLB]  
where case when isnull(@NationalLocationNumber,'') = '' then '' else isnull(NationalLocationNumber,'')  end = isnull(@NationalLocationNumber,'')
and case when isnull(@Branch,'') = '' then '' else isnull(Code,'')  end = isnull(@Code,'')
and case when isnull(@Code,'') = '' then '' else isnull(Code,'')  end = isnull(@Code,'')
and case when isnull(@CustomerName,'') = '' then '' else isnull(CustomerName,'')  end = isnull(@CustomerName,'')
and case when isnull(@Location,'') = '' then '' else isnull([Location],'')  end = isnull(@Location,'')
and isnull(IsActive,1) =1


End
GO
