SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CPPL_GetTrackingEventMapping]
@WorkFlow Nvarchar(50)='',
@OutCome Nvarchar(50)='',
@Message Nvarchar(max)='',
@PublishTo varchar(50)=''
AS
Select TrackingEventMappingId, WorkFlow,OutCome,Message,PublishTo from dbo.tblTrackingEventMapping WHERE IsDeleted=0
AND CASE WHEN ISNULL(@WorkFlow,'') = '' THEN '' ELSE ISNULL(WorkFlow,'')  END = ISNULL(@WorkFlow,'') 
AND CASE WHEN ISNULL(@OutCome,'') = '' THEN '' ELSE ISNULL(OutCome,'')  END = ISNULL(@OutCome,'') 
AND CASE WHEN ISNULL(@Message,'') = '' THEN '' ELSE ISNULL(Message,'')  END = ISNULL(@Message,'') 
AND CASE WHEN ISNULL(@PublishTo,'') = '' THEN '' ELSE ISNULL(PublishTo,'')  END = ISNULL(@PublishTo,'') 
order by CreatedDateTime Desc
GO
