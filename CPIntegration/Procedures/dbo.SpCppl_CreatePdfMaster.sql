SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SpCppl_CreatePdfMaster]
@Title Nvarchar(100)=null,
@Description Nvarchar(300)=null,
@FileName Nvarchar(100)=null,
@Url Nvarchar(500)=null,
@UTCDateTime datetime=null, 
@CreatedBy Nvarchar(50)=null,
@ThumbnailName Nvarchar(100)=null,
@ThumbnailUrl  Nvarchar(500)=null

AS

BEGIN
Begin Try
begin tran
	INSERT INTO tblPdfMaster(Title,Description,FileName,Url,UTCDateTime,CreatedDateTime,CreatedBy,ThumbnailName,ThumbnailUrl)
	VALUES(@Title,@Description,@FileName,@Url,Getdate(),@UTCDateTime,IIF(ISNULL(@CreatedBy,'')='','System',@CreatedBy),@ThumbnailName,@ThumbnailUrl)
	Select @@identity
	 COMMIT TRAN 

   END TRY
		BEGIN CATCH
		begin
			rollback tran
	
			INSERT INTO [dbo].[tblErrorLogNew1]
				   ([Error]
				   ,[FunctionInfo]
				   ,[ClientId])
			 VALUES
				   (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE()
				   ,'SpCppl_CreatePdfMaster', 2)
		end
		END CATCH
END
GO
