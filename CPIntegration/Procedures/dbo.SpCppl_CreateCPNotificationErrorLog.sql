SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SpCppl_CreateCPNotificationErrorLog]
	@Error varchar(max)=null,
    @FunctionInfo nvarchar (max) = NULL,
	@ClientId nvarchar (max)= NULL,
	@Request nvarchar (max)= NULL,
	@URL nvarchar (max)= NULL

As
Begin

INSERT INTO [dbo].[tblErrorLogNew1]
           ([Error],[FunctionInfo],[ClientId],[CreateDate],[Request],[URL])
     VALUES
           (@Error,@FunctionInfo,@ClientId,GETDATE(),@Request,@URL)
Select SCOPE_IDENTITY () as ID

End
GO
