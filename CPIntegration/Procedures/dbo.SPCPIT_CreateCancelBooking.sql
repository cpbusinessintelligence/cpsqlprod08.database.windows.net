SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[SPCPIT_CreateCancelBooking]

@BookingRefNo varchar(50)= null ,
@DriverExtRef varchar(100) = null,
@UTCDateTime datetimeoffset(7),
@LocalDateTime datetime= null ,
@CreatedBy varchar(50)= null,
@Request varchar(max) = null,
@NowGoRequest varchar(max) = null,
@IsClosed bit = 0
As 
Begin
SET NOCOUNT ON;
   SET XACT_ABORT ON;
     BEGIN TRANSACTION
	   	SAVE TRANSACTION SavePoint;
           BEGIN TRY	
                     INSERT INTO [dbo].[tblCancelBooking]
                     ([BookingRefNo],[DriverExtRef],[IsClosed],
		             [UTCDateTime],[LocalDateTime],[CreatedDateTime],[CreatedBy],[NowGoJobID],[Request],[Response],[NowGoRequest],[NowGoResponse])
                     VALUES
                     (
			           @BookingRefNo,@DriverExtRef, @IsClosed, @UTCDateTime, @LocalDateTime,GetDate(),@CreatedBy,null,@Request,null,@NowGoRequest,null
                     )
                Select SCOPE_IDENTITY() as CancelBookingID

     COMMIT TRANSACTION			
          END TRY  
          BEGIN CATCH
               ROLLBACK TRANSACTION;
	      END CATCH

End
GO
