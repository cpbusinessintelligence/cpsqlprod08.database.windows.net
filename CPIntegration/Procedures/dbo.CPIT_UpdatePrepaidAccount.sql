SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PRocedure [dbo].[CPIT_UpdatePrepaidAccount]
@PrepaidAccountID int,
@AccountCode varchar(50) =null,
@ParentAccountID int =0,
@ProntoAccountCode varchar(50)=null,
@Branch varchar(50) =null,
@OpenDays varchar(50) =null,
@OpenTime time =null,
@CloseTime time=null,
@Contractor int =0,
@Depot int=0,
@State int=0,
@Latitude varchar(50) =null,
@Longitude varchar(50) =null,
@LocalDateTime datetime = null,
@UpdatedBy int =0,
@AddFirstName varchar(50)=null,
@AddLastName varchar(50)=null,
@CompanyName varchar(100)=null,
@Email varchar(150)=null,
@Address1 varchar(100)=null,
@Suburb varchar(50)=null,
@AddState varchar(20)=null,
@Postcode varchar(4)=null,
@AddCompName varchar(100) =null,
@DesFirstName varchar(50)=null ,
@DesLastName varchar(50)=null, 
@DesAddress1 varchar(100)=null,
@DesSuburb varchar(50)=null,
@DesAddState varchar(20) =null,
@DesPostcode varchar(4)=null,
@DesCompName varchar(100) =null


As
Begin
Begin Try
BEGIN TRAN 

Declare @MailAddressID  int
Declare @DesAddressID  int
Declare @AddressTypeID  int
Declare @StateID  int

 Select @StateID =StateID from [dbo].[tblState] where StateCode =@AddState
 Select @AddressTypeID =AddressTypeID from [dbo].[tblAddressType] where AddressType ='Mailing'
 Select @MailAddressID =  MailingAddressID from tblPrepaidAccount where PrepaidAccountID = @PrepaidAccountID

 UPDATE [dbo].[tblAddress]
   SET [AddressTypeID] = @AddressTypeID,      
       [FirstName] = @AddFirstName,
       [LastName] = @AddLastName,
       [CompanyName] = @AddCompName,
       [Email] = @Email,
       [Address1] = @Address1,     
       [Suburb] = @Suburb,
       [State] = @StateID,
       [Postcode] = @Postcode,
	   [UTCDateTime] = SYSDATETIMEOFFSET(),
       [LocalDateTime] = @LocalDateTime,      
       [UpdatedDateTime] = GETDATE(),
       [UpdatedBy] = @UpdatedBy
 WHERE AddressID = @MailAddressID
  

 Select @StateID =StateID from [dbo].[tblState] where StateCode =@DesAddState
 Select @AddressTypeID =AddressTypeID from [dbo].[tblAddressType] where AddressType ='Despatch' 
 Select @DesAddressID =  DespatchAddressID from tblPrepaidAccount where PrepaidAccountID = @PrepaidAccountID

  UPDATE [dbo].[tblAddress]
   SET [AddressTypeID] = @AddressTypeID,      
       [FirstName] = @AddFirstName,
       [LastName] = @AddLastName,
       [CompanyName] = @DesCompName,
       [Email] = @Email,
       [Address1] = @Address1,     
       [Suburb] = @Suburb,
       [State] = @StateID,
       [Postcode] = @Postcode,
	   [UTCDateTime] = SYSDATETIMEOFFSET(),
       [LocalDateTime] = @LocalDateTime,      
       [UpdatedDateTime] = GETDATE(),
       [UpdatedBy] = @UpdatedBy
 WHERE AddressID = @DesAddressID


 UPDATE [dbo].[tblPrepaidAccount]
   SET [AccountCode] = @AccountCode,
       [ParentAccountID] = @ParentAccountID,
       [ProntoAccountCode] = @ProntoAccountCode,
       [Branch] = @Branch,
       [DespatchAddressID] = @DesAddressID,
       [MailingAddressID] = @MailAddressID,
       [OpenDays] = @OpenDays,
       [OpenTime] = @OpenTime,
       [CloseTime] = @CloseTime,
       [Contractor] = @Contractor,
       [Depot] = @Depot,
       [State] = @State,
       [Latitude] = @Latitude,
       [Longitude] = @Longitude,      
       [UTCDateTime] = SYSDATETIMEOFFSET(),
       [LocalDateTime] = @LocalDateTime,
       [UpdatedDateTime] = GETDATE(),
       [UpdatedBy] = @UpdatedBy
 WHERE PrepaidAccountID = @PrepaidAccountID 

 Select 'Success' as [Status]

COMMIT TRAN 

END TRY
		BEGIN CATCH
			begin
				rollback tran
				--INSERT INTO [dbo].[tblErrorLog] (ErrorCode,ErrorType,[Error],[Function],System)
				--VALUES ('','',cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,'[CPIT_UpdatePrepaidAccount]','')

				Select 'Error' as [Status], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
			end
		END CATCH


End
GO
