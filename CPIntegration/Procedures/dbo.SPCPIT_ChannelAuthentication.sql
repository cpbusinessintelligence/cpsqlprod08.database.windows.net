SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SPCPIT_ChannelAuthentication]
@ChannelName varchar(100) =null
AS
Begin
  Begin Try
     SELECT ChannelName as ChannelName FROM tblChannel 
     where ChannelName = @ChannelName and IsActive = 1 
     End Try
  Begin Catch
     select top(1) null as ChannelName from tblChannel
  End Catch
End
GO
