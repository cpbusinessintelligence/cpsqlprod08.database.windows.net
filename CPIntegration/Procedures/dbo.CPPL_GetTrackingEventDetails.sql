SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CPPL_GetTrackingEventDetails]
@LabelNumber NVARCHAR(50)='',
@NowGoWorkflowCompletionID NVARCHAR(50)=''
AS
BEGIN 
	
	SELECT TOP 100	 dbo.Fn_RtnPODImageIdByEventId(EVENTID)AS PodImageId, EVENTID,LabelNumber,Case When WorkflowType='deliver' And Outcome='delivered' then 1 else 0 end As isDeliver ,WorkflowType,Outcome,ConsignmentNumber,
					 Relation,ItemCount,CONVERT(VARCHAR(12), EventDateTime,109)+' '+CONVERT(VARCHAR(5),Cast(EventDateTime as time),108) EventDateTime,
					 Attribute, AttributeValue, DriverExtRef, DriverRunNumber, NowGoWorkflowCompletionID, IsProcessed,
					 DriverId,DeviceId,Branch,Latitude, Longitude,SigneeName,Case When IsSuccessful=1 Then 'Yes' Else 'No'End AS IsSuccessful,NowGoWorkflowCompletionID,NowGoConsignmentID,
					 NowGoSubjectArticleID,CONVERT(VARCHAR(12), CreatedDateTime,109)CreatedDateTime,
					 CreatedDateTime AS CreatedDateTime1 
	FROM tbl_TrackingEvent  
	WHERE	CASE WHEN ISNULL(@LabelNumber,'') = '' THEN '' ELSE ISNULL(LabelNumber,'')  END = ISNULL(@LabelNumber,'') AND
			CASE WHEN ISNULL(@NowGoWorkflowCompletionID,'') = '' THEN '' ELSE ISNULL(LabelNumber,'')  END = ISNULL(@NowGoWorkflowCompletionID,'') 
	ORDER BY CreatedDateTime1 DESC 
END
 


      
     
GO
