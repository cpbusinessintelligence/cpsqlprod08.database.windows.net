SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[SpCppl_DeleteDriverSafetyModuleContent]
@ModuleContentId int=0,
@IsActive bit=0,
@UpdatedBy Nvarchar(50)=null
AS
BEGIN	

	Update tblDriverSafetyModuleContent Set IsActive=@IsActive, UpdatedDateTime=GETDATE(), UpdatedBy=@UpdatedBy where ID=@ModuleContentId

	exec SpCppl_GetAllSafetyModuleContent
END


GO
