SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CPPL_UpdateTrackingEventMapping]
@TrackingEventMappingId Int =0,
@WorkFlow Nvarchar(50)='',
@OutCome Nvarchar(50)='',
@PublishTo Nvarchar(50)='',
@Message Nvarchar(max)='',
@Archive bit=0,
@UpdatedBy Nvarchar(50)='-1'

AS 
BEGIN 
	UPDATE dbo.tblTrackingEventMapping SET WorkFlow=@WorkFlow,OutCome=@OutCome,MESSAGE=@Message,PublishTo=@PublishTo,IsArchive=@Archive,UpdatedDateTime=GETDATE(),UpdatedBy=@UpdatedBy
	WHERE TrackingEventMappingId=@TrackingEventMappingId
	
	EXEC CPPL_GetTrackingEventMapping
END 
GO
