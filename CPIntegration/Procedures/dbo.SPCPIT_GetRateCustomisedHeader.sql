SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SPCPIT_GetRateCustomisedHeader]  --'PINE'
@AccountCode nvarchar(50),
@Service nvarchar(50),
@ValidFrom nvarchar(50)
As
Begin     
	 SELECT ValidFrom, ChargeMethod, VolumetricDivisor, FuelOverride, cast(Fuel as decimal(10,4)) as Fuel FROM tblRateCustomisedHeader
	 where Accountcode = @AccountCode and Service = @Service and ValidFrom <= @ValidFrom
End



GO
