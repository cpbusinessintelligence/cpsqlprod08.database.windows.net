SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================

CREATE PROCEDURE [dbo].[SP_RequestLog_Delete]

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

    -- Insert statements for procedure here
/****** Script for SelectTopNRows command from SSMS  ******/
BEGIN TRY
	BEGIN TRAN


		  PRINT  '-------- DELETE RECORDS FROM Request log TABLE  --------';

		  DELETE FROM  [tblRequestLog]	 where  LocalTime < GETDATE() - 30

		  PRINT  '-------- DELETE RECORDS FROM Request TABLE COMPLETED--------';

  COMMIT TRAN
  END TRY
  BEGIN CATCH
  begin
  	ROLLBACK TRAN
  end
  END CATCH
END
GO
