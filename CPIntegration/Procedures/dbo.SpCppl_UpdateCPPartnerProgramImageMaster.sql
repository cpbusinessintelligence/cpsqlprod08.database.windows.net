SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[SpCppl_UpdateCPPartnerProgramImageMaster]
@ImageId int=0,
@Title Nvarchar(100)=null,
@Description Nvarchar(300)=null,
@FileName Nvarchar(100)=null,
@Url Nvarchar(500)=null,
@UpdatedBy Nvarchar(50)=null

AS

BEGIN	

	Update tblCPPartnerProgramImageMaster Set Title=@Title, Description=@Description, FileName=@FileName, Url=@Url, UpdatedDateTime=GETDATE(), UpdatedBy= @UpdatedBy where ImageId=@ImageId

	exec SpCppl_GetCPPartnerProgramImageMasterForAdmin
END


GO
