SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_DeleteDLB]
(
@DlbID int ,
@UserID int 

)
As
Begin
BEGIN TRY
BEGIN tran 

update  tblDLB set IsActive =0 , InactiveDateTime =GETDATE() ,UpdatedDateTime=GETDATE(),UpdatedBy=@UserID   where DlbID=@DlbID
COMMIT tran 

Select 'Success' as OutPutMsg 
END TRY
BEGIN CATCH
begin
    rollback tran
	
	INSERT INTO [dbo].[tblErrorLog] ([Error],[ErrorCode],[ErrorType],[System], [Function],
				[UTCDateTime],[CreatedDateTime],[LocalTime],CreatedBy )
				VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,ERROR_SEVERITY(),ERROR_PROCEDURE(),
				'Local',
				'[SPCPIT_DeleteDLB]',
				SYSDATETIMEOFFSET(),getdate(), convert(time,GETDATE(),103),-1
				)

				Select 'Error' as [OutPutMsg], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
end
END CATCH

End






 
GO
