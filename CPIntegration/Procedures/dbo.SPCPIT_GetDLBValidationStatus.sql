SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_GetDLBValidationStatus]
@DLB varchar(50)
--and IsActive=0
As
Begin
IF EXISTS (SELECT * FROM tblDLB WHERE Code = @DLB) 
BEGIN
--select top (1) 'False' as DLBValidationStatus from tblDLB
select top (1) 'True' as DLBValidationStatus, [dbo].[tblPrepaidAccount].AccountCode, [dbo].[tblPrepaidAccount].Branch from tblDLB
inner join [dbo].[tblPrepaidAccount] on [dbo].[tblPrepaidAccount].PrepaidAccountID =  tblDLB.PrepaidAccountID
where tblDLB.code = @DLB 
END
ELSE
BEGIN
select top (1) 'False' as DLBValidationStatus, '' as AccountCode,'' as Branch from tblDLB
END
End

--ALTER Procedure [dbo].[SPCPIT_GetDLBValidationStatus]
--@DLB varchar(50)
----and IsActive=0
--As
--Begin
--IF EXISTS (SELECT * FROM tblDLB WHERE Code = @DLB) 
--BEGIN
--select top (1) 'True' as DLBValidationStatus from tblDLB
--END
--ELSE
--BEGIN
--select top (1) 'False' as DLBValidationStatus from tblDLB
--END
--End

GO
