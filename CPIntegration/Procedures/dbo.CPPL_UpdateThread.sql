SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--exec [CPPL_UpdateThread] @ThreadID="20",@CPResponse=null,@NowGoResponse='test',@UpdatedBy=null,@IsProcessed='1',@NowGoThreadID='123456'
 CREATE Procedure [dbo].[CPPL_UpdateThread]
 (
  @ThreadID  int,
  @CPResponse nvarchar(max) =null,
  @NowGoResponse nvarchar(max) =null,
  @UpdatedBy varchar(50) =null,
  @IsProcessed bit =0,
  @NowGoThreadID nvarchar(300) =null
 )
 As
 Begin
 
Update [dbo].[tblThread] set IsProcessed=@IsProcessed,NowGoThreadID=@NowGoThreadID, CPResponse=@CPResponse,NowGoResponse=@NowGoResponse,UpdatedDateTime=GETDATE(),UpdatedBy =@UpdatedBy where ThreadID=@ThreadID
 select @ThreadID as ThreadID

 End

 
GO
