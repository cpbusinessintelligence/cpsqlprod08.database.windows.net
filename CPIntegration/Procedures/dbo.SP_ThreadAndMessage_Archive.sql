SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================

CREATE PROCEDURE [dbo].[SP_ThreadAndMessage_Archive]

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

    -- Insert statements for procedure here
/****** Script for SelectTopNRows command from SSMS  ******/
BEGIN TRY
	BEGIN TRAN

	PRINT  '-------- COPY Message TABLE --------';

		INSERT INTO [dbo].[tblMessageArchive]	  
		SELECT *  FROM [dbo].[tblMessage] with (NOLOCK)   where CONVERT(date, CreatedDateTime) < CONVERT(date,GETDATE()-110 )

		  PRINT  '-------- COPY Message TABLE COMPLETED --------';

		  PRINT  '-------- DELETE RECORDS FROM Message TABLE  --------';

		  DELETE FROM  [tblMessage]	 where CONVERT(date, CreatedDateTime) < CONVERT(date,GETDATE()-110 )

		  PRINT  '-------- DELETE RECORDS FROM BOOKING TABLE COMPLETED--------';


		 

		 -- SELECT COUNT(*)  FROM [dbo].[tblMessageArchive]

		 PRINT  '-------- COPY Thread TABLE --------';
		
		--INSERT INTO [dbo].[tblThreadArchive]	  
		--SELECT *  FROM [dbo].[tblThread] with (NOLOCK)  where ThreadID in (SELECT ThreadID   FROM [dbo].[tblMessageArchive] with (NOLOCK))

		INSERT INTO [dbo].[tblThreadArchive]
		SELECT *  FROM [dbo].[tblThread] with (NOLOCK)  where CONVERT(date, CreatedDateTime) < CONVERT(date,GETDATE()-110 )

		 PRINT  '-------- COPY Thread TABLE COMPLETED --------';

		  PRINT  '-------- DELETE RECORDS FROM Thread TABLE  --------';

		  DELETE FROM  [tblThread]	 where CONVERT(date, CreatedDateTime) < CONVERT(date,GETDATE()-110 )

		  PRINT  '-------- DELETE RECORDS FROM BOOKING TABLE COMPLETED--------';

  COMMIT TRAN
  END TRY
  BEGIN CATCH
  begin
  	ROLLBACK TRAN
  end
  END CATCH
END
GO
