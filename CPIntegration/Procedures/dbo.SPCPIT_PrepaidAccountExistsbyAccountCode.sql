SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPIT_PrepaidAccountExistsbyAccountCode]
  @accountCode as varchar(50)
AS
BEGIN
 Select PrepaidAccountID,IsActive from [dbo].[tblPrepaidAccount] where AccountCode = @accountCode 
END

GO
