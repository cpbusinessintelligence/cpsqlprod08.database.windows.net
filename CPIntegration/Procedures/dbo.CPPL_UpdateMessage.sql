SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[CPPL_UpdateMessage]
(
 @MessageID int ,
 @CPResponse nvarchar(max) =null,
 @NowGoResponse nvarchar(max) =null,
 @UpdatedBy varchar(50) =null,
 @IsProcessed bit =0,
 @NowGoMessageID nvarchar(max) =0
  
)
As
Begin
 
update tblMessage  set IsProcessed=@IsProcessed,NowGoMessageID=@NowGoMessageID,CPResponse=@CPResponse,NowGoResponse=@NowGoResponse ,UpdatedDateTime =GETDATE(),
UpdatedBy=@UpdatedBy where ID=@MessageID

select @NowGoMessageID as NowGoMessageID

End
GO
