SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SpCppl_DeleteCPPartnerProgramImageMaster]
@ImageId int=0,
@IsActive bit=0,
@UpdatedBy Nvarchar(50)=null
AS
BEGIN	

	Update tblCPPartnerProgramImageMaster Set IsActive=@IsActive, UpdatedDateTime=GETDATE(), UpdatedBy=@UpdatedBy where ImageId=@ImageId

	exec SpCppl_GetCPPartnerProgramImageMasterForAdmin
END



GO
