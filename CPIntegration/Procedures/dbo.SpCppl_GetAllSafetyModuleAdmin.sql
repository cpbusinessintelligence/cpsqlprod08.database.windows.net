SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*
*/
-- =============================================
-- Author		:	Arpan Sharma
-- Create date	:	31-July-2020
-- Description	:	To get all detail of Safetr Driver for view in CPGo project
-- =============================================
Create Procedure [dbo].[SpCppl_GetAllSafetyModuleAdmin]
@ModuleId int=null

AS
BEGIN 
	SELECT * FROM tblDriverSafetyModule WHERE IsActive=1 and	
	CASE WHEN isnull(@ModuleId,0) = 0 THEN 0 ELSE isnull(ID,0)  END = isnull(@ModuleId,0)	
	ORDER BY ID DESC
END 



GO
