SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPIT_GetSuburbLookUpBySuburbOrPostcode]  --'PINE'
@suburbOrPostcode nvarchar(50) 
As
Begin		
		If(isnumeric(@suburbOrPostcode) = 1)
		Begin

		      If(len(cast(@suburbOrPostcode as int)) < 4)
			  Begin
			       set @suburbOrPostcode = '0'+@suburbOrPostcode;
			       SELECT * FROM tblSuburbLookUp where PostCodeSuburb like  @suburbOrPostcode+'%'
			  End
			  Else
			  Begin
			       SELECT * FROM tblSuburbLookUp where PostCodeSuburb like  @suburbOrPostcode+'%'
			  End
		    
		End
		Else
		Begin
			SELECT * FROM tblSuburbLookUp where Suburb like @suburbOrPostcode+'%'
		End

End
GO
