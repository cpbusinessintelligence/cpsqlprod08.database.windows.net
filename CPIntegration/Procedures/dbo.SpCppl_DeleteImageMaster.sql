SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SpCppl_DeleteImageMaster]
@ImageId int=0,
@IsActive bit=0,
@UpdatedBy Nvarchar(50)=null
AS
BEGIN	

	Update tblImageMaster Set IsActive=@IsActive, UpdatedDateTime=GETDATE(), UpdatedBy=@UpdatedBy where ImageId=@ImageId

	exec SpCppl_GetImageMasterForAdmin
END
GO
