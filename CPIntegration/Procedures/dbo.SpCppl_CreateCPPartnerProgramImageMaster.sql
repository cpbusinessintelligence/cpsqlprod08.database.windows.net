SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author		:	Rajkumar Sahu
-- Create date	:	23-Sep-2019
-- Description	:	Insert video file Information to show in cp-web-IT application
-- =============================================
CREATE Procedure [dbo].[SpCppl_CreateCPPartnerProgramImageMaster]
@Title Nvarchar(100)=null,
@Description Nvarchar(300)=null,
@FileName Nvarchar(100)=null,
@Url Nvarchar(500)=null,
@CreatedBy Nvarchar(50)=null

AS

BEGIN
	INSERT INTO tblCPPartnerProgramImageMaster(Title,Description,FileName,Url,CreatedDateTime,CreatedBy)
	VALUES(@Title,@Description,@FileName,@Url,Getdate(),IIF(ISNULL(@CreatedBy,'')='','System',@CreatedBy))
	Select @@identity
END





GO
