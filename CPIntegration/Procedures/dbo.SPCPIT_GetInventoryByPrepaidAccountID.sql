SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[SPCPIT_GetInventoryByPrepaidAccountID] 
@PrepaidAccountID int
 As
Begin
select InventoryID ,InventoryCode,[Description],NoOfCoupon,CostPerCoupon,SalesPriceExGST      
  from tblInventory 
where InventoryID in (Select InventoryID from tblPrepaidAccountSalesProfile where PrepaidAccountID = @PrepaidAccountID)
 
End
GO
