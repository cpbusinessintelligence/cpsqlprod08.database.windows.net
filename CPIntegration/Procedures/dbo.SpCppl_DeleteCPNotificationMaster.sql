SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SpCppl_DeleteCPNotificationMaster]
@CPNotificationId int=0,
@IsActive bit=0,
@UpdatedBy Nvarchar(50)=null
AS
BEGIN	

	Update tblCPNotificationMaster Set IsActive=@IsActive, UpdatedDateTime=GETDATE(), UpdatedBy=@UpdatedBy where CPNotificationId=@CPNotificationId

	exec SpCppl_GetCPNotificationMasterForAdmin
END
GO
