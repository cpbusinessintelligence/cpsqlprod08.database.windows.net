SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		:	Rajkumar Sahu
-- Create date	:	24-Sep-2019
-- Description	:	SELECT VIDEO Information to show in cp-web-IT application
-- =============================================
CREATE Procedure [dbo].[SpCppl_DeleteVideoMaster]
@VideoId int=0,
@InActiveDatetime datetime=null,
@InActiveBy Nvarchar(100)
As
BEGIN	 
	UPDATE tblVideoMaster SET IsActive=0,InActiveDatetime=@InActiveDatetime,InActiveBy=IIF(ISNULL(@InActiveBy,'')='','System',@InActiveBy)  WHERE VideoId=@VideoId  
	select @VideoId
END 
GO
