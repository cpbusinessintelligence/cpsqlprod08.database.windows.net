SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[SPCPIT_UpdateMessage]

@ID int,
@AzureMessageID varchar(50),
@UpdatedBy varchar(50)

As 
Begin

Update [tblMessage] set [AzureMessageID] = @AzureMessageID, [IsProcessed]  = 1 ,[UpdatedDateTime]=GetDate(), [UpdatedBy] = @UpdatedBy where [ID] = @ID

Select SCOPE_IDENTITY() as ID

End


GO
