SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[SPCPIT_UpdateBookingOld]

@BookingID int,
@MessageID varchar(50),
@UpdatedBy int

As 
Begin

Update [tblBooking] set [MessageId] = @MessageID, [IsProcessed]  = 1 ,[UpdatedDateTime]=GetDate(), [UpdatedBy] = @UpdatedBy where [BookingID] = @BookingID

Select SCOPE_IDENTITY() as BookingID

End
GO
