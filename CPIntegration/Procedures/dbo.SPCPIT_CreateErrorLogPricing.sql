SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SPCPIT_CreateErrorLogPricing]
@ErrorCode varchar(50)=null,
@ErrorType varchar(50)=null,
@Error varchar(max)=null,
@Function varchar(200)=null,
@Application varchar(200) = null,
@Request varchar(max)=null,
@Response varchar(max)=null,
@URL varchar(max)=null,
@AffectedUser varchar(50)=null,
@UTCDateTime datetime=null,
@LocalTime datetime=null,
@CreatedBy int=null,
@ClientCode varchar(10) = null,
@ServiceCode varchar(50) = null

As
Begin

INSERT INTO [dbo].[tblErrorLogPricing]
           ([ErrorCode],[ErrorType],[Error],[Function],[Application],[Request],[Response],[URL],[AffectedUser],[UTCDateTime],[LocalTime],[CreatedBy],[ClientCode],[ServiceCode])
     VALUES
           (@ErrorCode,@ErrorType,@Error,@Function,@Application,@Request,@Response,@URL,@AffectedUser,@UTCDateTime,@LocalTime,@CreatedBy,@ClientCode,@ServiceCode)
Select SCOPE_IDENTITY () as ID

End
GO
