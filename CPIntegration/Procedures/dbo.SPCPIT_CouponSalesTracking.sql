SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- exec [dbo].[SPCPIT_CouponSalesTracking] '','3217701'
CREATE PROCEDURE [dbo].[SPCPIT_CouponSalesTracking]
@invoiceNumber varchar(50)='',
@accountCode varchar(50)=''
AS
BEGIN 
		SELECT TOP 100 ID, invoiceNumber,accountCode,RP,barcode,branch,consignmentType,coupon,customerPhoneNumber,driverWareHouseNumber,insuranceCode,insurancevalue,
		sale,transactionType,convert(varchar(30), SaleDateTime,109) SaleDateTime
		FROM tblCouponSales  
		WHERE CASE WHEN isnull(@invoiceNumber,'') = '' THEN '' ELSE isnull(invoiceNumber,'')  END = isnull(@invoiceNumber,'') 
		AND CASE WHEN isnull(@accountCode,'') = '' THEN '' ELSE isnull(accountCode,'')  END = isnull(@accountCode,'') 
 		ORDER BY Id DESC  
END

GO
