SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[SPCPIT_GetCutoffTimeByPostcodeAndSuburb]  --'PINE'
@PostCode nvarchar(50),@Suburb nvarchar(50) 
As
Begin     
	
	 SELECT * FROM tblCutoffTime where PostCode = @PostCode and Suburb = @Suburb			 

End
GO
