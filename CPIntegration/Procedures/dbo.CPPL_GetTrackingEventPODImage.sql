SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CPPL_GetTrackingEventPODImage]
@EventID varchar(50)
AS

	Select PODImageID,ImageType from [dbo].[VIEW_TrackingEvent_PODImage] PODImage 
	inner join [dbo].[tbl_TrackingEvent] tracking on PODImage.EventID=tracking.EventID 
	Where tracking.WorkflowType='deliver' 
	and tracking.Outcome='delivered' 
	and PODImage.ImageType is not null 	
	and tracking.EventID=@EventID
GO
