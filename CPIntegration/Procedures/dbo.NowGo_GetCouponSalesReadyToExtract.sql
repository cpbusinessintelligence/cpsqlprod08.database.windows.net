SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NowGo_GetCouponSalesReadyToExtract]
(
	@Branch VARCHAR(30)
)

AS
BEGIN

    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRY
		
		-- GET Customer's Pronto Code
		SELECT 	
			'S' AS [literal],		
			IIF([ProntoId] IS NULL OR TRIM(REPLACE( REPLACE( [ProntoId], CHAR(10), ''), CHAR(13),'')) = '' OR REPLACE( REPLACE( [ProntoId], CHAR(10), ''), CHAR(13),'') = 'NULL', NULL, '"' + REPLACE( REPLACE( [ProntoId], CHAR(10), ''), CHAR(13),'') + '"') AS [driverWareHouseNumber],
			[SaleDateTime],
			IIF([barcode] IS NULL OR [barcode] = '', NULL, '"' + [barcode] + '"') AS [barcode],
			IIF([coupon] IS NULL OR [coupon] = '', NULL, '"' + [coupon] + '"') AS [coupon],
			IIF([RP] IS NULL OR [RP] = '', NULL, '"' + [RP] + '"') AS [RP],
			IIF([ProntoAccountCode] IS NULL OR TRIM(REPLACE( REPLACE( [ProntoAccountCode], CHAR(10), ''), CHAR(13),'')) = '', NULL, '"'+ TRIM(REPLACE( REPLACE( [ProntoAccountCode], CHAR(10), ''), CHAR(13),'')) +'"') AS [accountCode],
			IIF([customerPhoneNumber] IS NULL OR [customerPhoneNumber] = '', NULL, '"' + [customerPhoneNumber] + '"') AS [customerPhoneNumber],
			IIF([invoiceNumber] IS NULL OR [invoiceNumber] = '', NULL, '"' + [invoiceNumber] + '"') AS [invoiceNumber],
			[transactionType],
			[consignmentType],
			[ProntoInsuranceCode] AS [insuranceCode],
			[insurancevalue],
			'' AS unused
		FROM [tblCouponSalesSLSExportStaging]
		WHERE [branch] = @Branch;

	END TRY

	BEGIN CATCH
	
		THROW;

	END CATCH	
END
GO
