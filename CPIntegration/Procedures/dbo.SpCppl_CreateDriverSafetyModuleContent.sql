SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author		:	Arpan Sharma
-- Create date	:	16-April-2020
-- Description	:	Insert CP Notification record
-- =============================================
Create Procedure [dbo].[SpCppl_CreateDriverSafetyModuleContent]
@DriverySafetyModuleID int=0,
@Title varchar(200)=null,
@ImagePath Nvarchar(500)=null,
@YouTubeLink Nvarchar(500)=null,
@CreatedBy Nvarchar(50)=null

AS

BEGIN
	INSERT INTO tblDriverSafetyModuleContent(DriverySafetyModuleID,Title,ImagePath,YouTubeLink,CreatedBy,CreatedDateTime)
	VALUES(@DriverySafetyModuleID,@Title,@ImagePath,@YouTubeLink,@CreatedBy,Getdate())
	Select @@identity
END



GO
