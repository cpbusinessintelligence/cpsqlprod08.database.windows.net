SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_UpdateSalesRep]
(
 @SalesCode varchar(50),
 @SalesRepName varchar(100),
 @AccountType int,
 @IndustryCode int,
 @LocalDateTime datetime,
 @UpdatedBy int ,
 @SalesRepID int 
)
AS
Begin
BEGIN TRY
BEGIN tran 

update  [tblSalesRep] set SalesCode=@SalesCode,SalesRepName=@SalesRepName,AccountType=@AccountType,IndustryCode=@IndustryCode,UpdatedDateTime =GETDATE(),UpdatedBy=@UpdatedBy
where SalesRepID =@SalesRepID
Select 'Success' as [OutPutMsg]

COMMIT tran 
END TRY
BEGIN CATCH
begin
    rollback tran
	
	INSERT INTO [dbo].[tblErrorLog] ([Error],[ErrorCode],[ErrorType],[System], [Function],
				[UTCDateTime],[CreatedDateTime],[LocalTime],CreatedBy )
				VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,ERROR_SEVERITY(),ERROR_PROCEDURE(),
				'Local',
				'[SPCPIT_UpdateSalesRep]',
				SYSDATETIMEOFFSET(),getdate(), convert(time,GETDATE(),103),-1
				)

				Select 'Error' as [OutPutMsg], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
end
END CATCH
End
GO
