SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO





CREATE Procedure [dbo].[SPCPIT_CreateMessage]
    @BookingNumber varchar(50),
	@MessageID varchar(100),
	@Branch varchar(10),
	@MessageDatetime datetime = NULL,
	@Message nvarchar(max),
	@Comment varchar(100) = NULL,
	@SendBy varchar(100) NULL,
	@SendTo varchar(100),
	@EmmId varchar(50) = NULL,
	@IsProcessed bit = NULL,
	--@AzureMessageID varchar(100) = NULL,
	@UTCDateTime datetimeoffset(7),
	@LocalDateTime datetime = NULL,
	--@CreatedDateTime datetime,
	@CreatedBy varchar(50)
	--@UpdatedDateTime datetime = NULL,
	--@UpdatedBy int = NULL
As 
Begin

INSERT INTO [dbo].[tblMessage]
           (
		   [BookingNumber],
		   [MessageID],
		   [Branch],
		   [MessageDatetime],
		   [Message],
		   [Comment],
		   [SendBy],
		   [SendTo],
           [EmmId],
		   [IsProcessed],
		   [UTCDateTime],
		   [LocalDateTime],
		   [CreatedDateTime],
		   [CreatedBy]
		   )
     VALUES
           (
			@BookingNumber,
			@MessageID,
			@Branch,
			@MessageDatetime,
			@Message,
			@Comment,
			@SendBy,
			@SendTo,
			@EmmId,
		    0, 
			@UTCDateTime,
			@LocalDateTime,
			GetDate(),
			@CreatedBy
           )
Select SCOPE_IDENTITY() as ID

End


GO
