SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SPCPIT_GetPrepaidCustomerbyAccCodeAndBranch_V2] 
	--@AccountCode varchar(50),
	--@Branch nvarchar(50).
	@inventoryCode as varchar(50) 
--and IsActive=0
As
BEGIN
declare  @PrepaidAccountID  as int 

--select   @PrepaidAccountID = tpa.PrepaidAccountID
--from tblPrepaidAccount tpa 
--Left join tblDepot td on tpa.Depot = td.DepotID   
--Left join tblState ts on tpa.State = ts.StateID
--where tpa.AccountCode= @AccountCode and tpa.Branch=@Branch 
--and (tpa.IsActive = 1 or tpa.IsActive is null)

--select tpa.PrepaidAccountID, tpa.AccountCode,tpa.ParentAccountID,tpa.ProntoAccountCode,tpa.Branch,tpa.DespatchAddressID,tpa.MailingAddressID,
--tpa.OpenDays,tpa.OpenTime,tpa.CloseTime,tpa.Contractor,td.DepotName as Depot,ts.StateCode as State,tpa.Latitude,tpa.Longitude,
--'Valid Customer' as IsValidCustomerAccountCode,tpa.IsActive 
--from tblPrepaidAccount tpa 
--Left join tblDepot td on tpa.Depot = td.DepotID   
--Left join tblState ts on tpa.State = ts.StateID
--where tpa.AccountCode='38058543' and tpa.Branch='Brisbane' 
--and (tpa.IsActive = 1 or tpa.IsActive is null)

--select t.AddressID,t.AddressTypeID,t.FirstName,t.LastName,t.CompanyName ,t.Email,t.Address1,t.Address2,t.Address3 ,t.Suburb,t.Postcode,StateCode AS State,
--tblState.StateName, AddressType,t.Suburb + ' '+StateCode+' '+t.Postcode as AddressDetails,t.Phone,t.Mobile 
--from tblprepaidaccount  as tblpreacc 
--inner join tblAddress t on t.AddressID = tblpreacc.DespatchAddressID or t.AddressID = tblpreacc.MailingAddressID
--inner join tblAddressType as addtype on addtype.AddressTypeID= t.AddressTypeID
--inner join tblState  on tblState.StateID= t.State
--where prepaidaccountid=@PrepaidAccountID and addtype.AddressType = 'DespatchAddress'

--select t.AddressID,t.AddressTypeID,t.FirstName,t.LastName,t.CompanyName ,t.Email,t.Address1,t.Address2,t.Address3 ,t.Suburb,t.Postcode,StateCode AS State,
--tblState.StateName, AddressType,t.Suburb + ' '+StateCode+' '+t.Postcode as AddressDetails,t.Phone,t.Mobile 
--from tblprepaidaccount  as tblpreacc 
--inner join tblAddress t on t.AddressID = tblpreacc.DespatchAddressID or t.AddressID = tblpreacc.MailingAddressID
--inner join tblAddressType as addtype on addtype.AddressTypeID= t.AddressTypeID
--inner join tblState  on tblState.StateID= t.State
--where prepaidaccountid=@PrepaidAccountID and addtype.AddressType = 'MailingAddress'

--select tblpreaccd.PrepaidAccountID,AccountDetailID, 
--CONVERT(varchar(10), tblpreaccd.StartDate,111) as StartDate ,CONVERT(varchar(10), tblpreaccd.LastUsed,111) as  LastUsed,Added,
--tblpreaccd.AccountType as AccountType,tblpreaccd.IndustryCode as IndustryCode,
--acctype.Description as AccountTypedesc,induscode.Description as IndustryCodedesc,
--case when Eft =1 then 'Yes' else 'No' end as Eft,Eft as Eftid,
--case when RegularRun =1 then 'Yes' else 'No' end as RegularRun,RegularRun AS RegularRunid,tblSalesRep.SalesRepName  ,SalesCode,tblpreaccd.SalesRepID,
--case when Status =1 then 'Active' else 'In Active' end as Status,Status AS StatusID,PickupDays,CONVERT(varchar(10), RepStart,111) as RepStart  ,Comments 
--  from tblPrepaidAccountDetail as tblpreaccd
--  LEFT join [dbo].[tblAccountType] as acctype on tblpreaccd.AccountType =acctype.AccountTypeID
--LEFT join [dbo].tblIndustryCode as induscode on tblpreaccd.IndustryCode =induscode.IndustryCodeID
--LEFT join [dbo].tblSalesRep  on tblpreaccd.SalesRepID =tblSalesRep.SalesRepID
--where tblpreaccd.PrepaidAccountID=@PrepaidAccountID 

--select PrepaidAccountID,ContactID,ContactType,FirstName,LastName,
-- Mobile,Phone,Email,Comment ,case when  isnull(IsActive,1) =1 then 'Yes' else 'No' end as Active
--  from tblPrepaidAccountContact 
--where PrepaidAccountID=@PrepaidAccountID

--Select tblPrepaidAccountSalesProfileID,PrepaidAccountID,tblPrepaidAccountSalesProfile.InventoryID,
--WarehouseCode,IsDiscountApplicable,DiscountPercentage, [InventoryCode],[Description],NoOfCoupon,CostPerCoupon,SalesPriceExGST,
--convert(varchar(10), DiscountStartDate,111) as DiscountStartDate,convert(varchar(10), DiscountEndDate,111)  as DiscountEndDate,SalesPrice,tblPrepaidAccountSalesProfile.[CreatedBy] ,
--case when isnull(tblPrepaidAccountSalesProfile.IsActive,1) = 1 then 'Yes' else 'No' end as Active
--From   [dbo].[tblPrepaidAccountSalesProfile] inner join tblinventory on [tblPrepaidAccountSalesProfile].InventoryID =tblinventory.InventoryID  where 
--InventoryCode = @inventoryCode
--PrepaidAccountID=@PrepaidAccountID


Select InventoryID, [InventoryCode],[Description],NoOfCoupon,CostPerCoupon,SalesPriceExGST
--convert(varchar(10), DiscountStartDate,111) as DiscountStartDate,convert(varchar(10), DiscountEndDate,111)  as DiscountEndDate,
From  --  [dbo].[tblPrepaidAccountSalesProfile] inner join
 tblinventory  where 
InventoryCode = @inventoryCode



--Select tblPrepaidAccountSalesProfileID,PrepaidAccountID,tblPrepaidAccountSalesProfile.InventoryID,
--WarehouseCode,IsDiscountApplicable,DiscountPercentage, [InventoryCode],[Description],NoOfCoupon,CostPerCoupon,SalesPriceExGST,
--convert(varchar(10), DiscountStartDate,111) as DiscountStartDate,convert(varchar(10), DiscountEndDate,111)  as DiscountEndDate,SalesPrice,tblPrepaidAccountSalesProfile.[CreatedBy] ,
--case when isnull(tblPrepaidAccountSalesProfile.IsActive,1) = 1 then 'Yes' else 'No' end as Active
--From   [dbo].[tblPrepaidAccountSalesProfile] inner join tblinventory on [tblPrepaidAccountSalesProfile].InventoryID =tblinventory.InventoryID  where 
--InventoryCode = @inventoryCode
END

GO
