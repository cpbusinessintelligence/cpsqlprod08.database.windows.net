SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create Procedure [dbo].[SpCppl_UpdateImageMaster]
@ImageId int=0,
@Title Nvarchar(100)=null,
@Description Nvarchar(300)=null,
@FileName Nvarchar(100)=null,
@Url Nvarchar(500)=null,
@UpdatedBy Nvarchar(50)=null,
@ThumbnailName Nvarchar(100)=null,
@ThumbnailUrl  Nvarchar(500)=null

AS

BEGIN	

	Update tblImageMaster Set Title=@Title, Description=@Description, FileName=@FileName, Url=@Url, ThumbnailName=@ThumbnailName,  ThumbnailUrl=@ThumbnailUrl, UpdatedDateTime=GETDATE(), UpdatedBy= @UpdatedBy where ImageId=@ImageId

	exec SpCppl_GetImageMasterForAdmin
END
GO
