SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
CREATE PROCEDURE dbo.CP_Load_PricingZoneLookup_JSON

AS
BEGIN
    SELECT JSONString FROM
		(SELECT 
				[Id],
					(SELECT    
						 [id]
						,[PostCodeID]
						  ,[PostCode]
						  ,[Suburb]
						  ,[State]
						  ,[PrizeZone]
						  ,[PrizeZoneName]
						  ,[ETAZone]
						  ,[ETAZoneName]
						  ,[RedemptionZone]
						  ,[RedemptionZoneName]
						  ,[DepotCode]
						  ,[SortCode]
						  ,[IsPickup]
						  ,[IsDelivery]
						  ,[PostCodeSuburb]
						  ,[IsActive]
						  ,[PostCodeOld]
						  ,[CreatedDateTime]
						  ,[CreatedBy]
						  ,[UpdatedDateTime]
						  ,[UpdatedBy]
					FROM [dbo].[tblPricingZoneLookUp] WITH (NOLOCK)
						WHERE  [Id] = RelationalJSONData.Id
					FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER)JSONString
			FROM [tblPricingZoneLookUp]  AS RelationalJSONData  WITH (NOLOCK) 
			--WHERE  [Id] BETWEEN @Lower AND @Upper
			) AS
	JSONOnly
END
GO
