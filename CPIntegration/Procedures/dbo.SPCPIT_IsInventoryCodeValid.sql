SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_IsInventoryCodeValid]  @inventoryCode as varchar(50) 
as
begin
	Select InventoryID,IsActive from [dbo].[tblInventory] where InventoryCode = @inventoryCode --and IsActive <> 0
end
GO
