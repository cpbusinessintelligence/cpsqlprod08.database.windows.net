SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPIT_ServiceAccess]
 @clientCode varchar(40) = null,
 @serviceName varchar(100) = null,
 @ServiceCode varchar(50)=null
AS
BEGIN
		SELECT CSA.ClientID, CSA.ServiceID, C.ClientCode, S.ServiceCode
		FROM dbo.[tblClientServiceAccess] AS CSA 
		INNER JOIN dbo.[tblClient] AS C ON CSA.ClientID = C.ClientID 
		INNER JOIN dbo.[tblServices] AS S ON CSA.ServiceID = S.ServiceID
		WHERE (C.ClientCode = @clientCode) AND (S.ServiceCode = @serviceCode)AND (S.ServiceName = @serviceName)
		and IsActive = 1 and convert(date,isnull(InActiveDateTime,'01-01-1990'),103) >= case when isnull(InActiveDateTime,'') = '' then convert(date,'01-01-1990',103) else getdate() end

END

--[SPCPIT_ServiceAccess] 'CPIT','CouponCalculatorZoneLevelService',1




GO
