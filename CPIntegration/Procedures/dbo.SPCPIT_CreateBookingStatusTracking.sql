SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SPCPIT_CreateBookingStatusTracking]

       @uniqueID varchar (200)= null 
      ,@accountNumber varchar (150)= null 
      ,@branch varchar (100)= null 
      ,@bookingType varchar (50)= null 
      ,@itemNumber varchar (100)= null 
      ,@lastStatus varchar (50)= null 
      ,@lastUpdatedDateTime varchar (100)= null 
      ,@driverNumber varchar (100)= null 
      ,@responseCode varchar (50)= null 
      ,@statusCode varchar (50)= null 
      ,@msg varchar (200)= null 
      ,@bookingStatusID varchar (100)= null 
	  ,@CreatedBy varchar (50)= null 
	  ,@UpdatedBy varchar (50)= null
	   
	  
As 
Begin

INSERT INTO [dbo].[tblBookingStatus]
           ([uniqueID],[accountNumber],[branch],[bookingType],[itemNumber],[lastStatus],[lastUpdatedDateTime],[driverNumber],[responseCode],[statusCode],[msg],[bookingStatusID],
		   [CreatedBy],[CreatedDateTime],[UpdatedBy],[UpdatedDateTime])
     VALUES
           (
			@uniqueID,@accountNumber,@branch,@bookingType,@itemNumber,@lastStatus,@lastUpdatedDateTime,@driverNumber,@responseCode,@statusCode,@msg,@bookingStatusID,
			@CreatedBy,GetDate(),null,GetDate()
           )
Select SCOPE_IDENTITY() as ID

End






GO
