SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SPCPIT_CreatePrepaidAccountCouponSales]   
 -- Add the parameters for the stored procedure here  
  @accountCode varchar(50),  
  @branch varchar(50),  
  @dlbBarcode varchar(50)=null,  
  @invoiceNumber varchar(50),  
  @serialNumber varchar(50),  
  @prefix varchar(50),  
  @discount decimal(18,2),  
  --@withoutDiscountValue decimal(18,2),  
  @insurance decimal(18,2),  
  --@finalValueExGST decimal(18,2),  
  @gst decimal(18,2),  
  --@finalValueIncGST decimal(18,2),  
  --@quantity int,  
  --@total decimal(18,2),   
  @createdBy int   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 declare @InventoryID int,@PrepaidAccountID int,@tblPrepaidAccountSalesProfileID int 
 -- calculation parameters
 declare @withoutDiscountValue decimal(18,2)= 0,  @quantity int=0, @finalValueExGST decimal(18,2),@finalValueIncGST decimal(18,2), @total decimal(18,2)

 -- Temp Calculation Parameters
 declare @discountValue decimal(18,2), @insuranceValue decimal (18,2), @gstValue decimal(18,2)

 Select @prepaidAccountID = Isnull(PrepaidAccountID,0) from [dbo].[tblPrepaidAccount] where AccountCode = @accountCode and Branch = @branch and IsActive <> 0  
 Select @inventoryID = Isnull(InventoryID,0), @quantity = isnull(NoOfCoupon, 0), @withoutDiscountValue = isnull(CostPerCoupon, 0)  from [dbo].[tblInventory] where InventoryCode = @prefix and IsActive <> 0  
 Select @tblPrepaidAccountSalesProfileID = tblPrepaidAccountSalesProfileID from [dbo].[tblPrepaidAccountSalesProfile] where  PrepaidAccountID = @prepaidAccountID and  InventoryID = @inventoryID  
  
 If (Isnull(@prepaidAccountID,0) > 0 and Isnull(@tblPrepaidAccountSalesProfileID,0) > 0)  
 Begin      
  Begin Transaction  
  Begin Try  
    
	set @discountValue = @withoutDiscountValue * @discount / 100;
	set @insuranceValue = @withoutDiscountValue * @insurance / 100;
	set @finalValueExGST = @withoutDiscountValue - @discountValue + @insuranceValue;
	set @gstValue = @finalValueExGST * @gst / 100; 
	set @finalValueIncGST = @finalValueExGST + @gstValue;
	set @total = @finalValueIncGST * @quantity;
    INSERT INTO [dbo].[tblPrepaidCouponSales]  
         ([AccountCode],[Branch],[DLBBarcode],[InvoiceNumber],[Serialnumber],[Prefix],[Quantity],[Value],[Discount],[Insurance],  
          [ValueExGST],[GST],[ValueIncGST],[TotalValue],[UTCDateTime],[LocalDateTime],[CreatedDateTime],[CreatedBy] )  
                VALUES  (@accountCode,@Branch,@dlbBarcode,@invoiceNumber,@serialNumber,@prefix,@quantity,@withoutDiscountValue,@withoutDiscountValue - @discountValue,@insuranceValue,  
                @finalValueExGST,@gstValue,@finalValueIncGST,@total,SYSDATETIMEOFFSET(),GETDATE(),GETDATE(),@createdBy)  
  
  Commit Transaction  
  Select SCOPE_IDENTITY () as ID  
  End Try  
  Begin Catch  
  Rollback Transaction  
  End Catch  
  
 End  
END  
GO
