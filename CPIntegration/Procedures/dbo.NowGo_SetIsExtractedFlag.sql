SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NowGo_SetIsExtractedFlag]
(
	@Branch VARCHAR(30)
)

AS
BEGIN

    SET NOCOUNT ON;
	SET XACT_ABORT ON;
	
	BEGIN TRY
		UPDATE CPN
		SET 
			[IsExtractedToPronto] = 1,
			[UpdatedDateTime] = CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time')
		FROM tblCouponSales CPN
		INNER JOIN [tblCouponSalesSLSExportStaging] STG
		ON CPN.ID = STG.ID
		WHERE STG.ID IN (SELECT ID FROM [tblCouponSalesSLSExportStaging] WHERE [Branch] = @Branch)
	END TRY

	BEGIN CATCH

		THROW;

	END CATCH

END
GO
