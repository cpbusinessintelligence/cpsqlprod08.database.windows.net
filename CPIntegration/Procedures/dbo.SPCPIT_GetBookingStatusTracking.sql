SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SPCPIT_GetBookingStatusTracking]
@accountNumber varchar(50)=''
AS
BEGIN 
Select TOP 100 * FROM [dbo].[tblBookingStatus]  WHERE CASE WHEN isnull(@accountNumber,'') = '' THEN '' ELSE isnull(accountNumber,'')  END = isnull(@accountNumber,'') 
 		ORDER BY accountNumber DESC  
END
GO
