SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE SPCPIT_GetInsuranceCharge
@InsuranceCategoryCode VARCHAR(50)
AS
SELECT InsuranceCharge FROM tblInsuranceReference where InsuranceCategoryCode =@InsuranceCategoryCode
GO
