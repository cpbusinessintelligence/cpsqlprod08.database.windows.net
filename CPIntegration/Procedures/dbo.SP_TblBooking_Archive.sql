SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
CREATE PROCEDURE [dbo].[SP_TblBooking_Archive]

AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON

    -- Insert statements for procedure here
/****** Script for SelectTopNRows command from SSMS  ******/
BEGIN TRY
	BEGIN TRAN
	
	PRINT  '-------- COPY BOOKING TABLE --------';

		INSERT INTO [dbo].[tblBooking_Archive]	  
		SELECT *  FROM [dbo].[tblBooking] with(NOLOCK)  where CONVERT(date, CreatedDateTime) < CONVERT(date,GETDATE()-7) -- CreatedDateTime < GETDATE() - 45

		
		  PRINT  '-------- COPY BOOKING TABLE COMPLETED --------';

		  PRINT  '-------- DELETE RECORDS FROM BOOKING TABLE  --------';

		  DELETE FROM  [tblBooking] where CONVERT(date, CreatedDateTime) < CONVERT(date,GETDATE()-7) -- CreatedDateTime < GETDATE() - 45

		  
		  PRINT  '-------- DELETE RECORDS FROM BOOKING TABLE COMPLETED--------';

		  PRINT  '-------- COPY BOOKING TABLE --------';

		INSERT INTO [dbo].[tblReassignBookingArchive]	  
		SELECT *  FROM [dbo].[tblReassignBooking] with(NOLOCK)  where CONVERT(date, CreatedDateTime) < CONVERT(date,GETDATE()-7) -- CreatedDateTime < GETDATE() - 45

		
		  PRINT  '-------- COPY REASSIGN BOOKING TABLE COMPLETED --------';

		  PRINT  '-------- DELETE RECORDS FROM REASSIGN BOOKING TABLE  --------';

		  DELETE FROM  [tblReassignBooking] where CONVERT(date, CreatedDateTime) < CONVERT(date,GETDATE()-7) -- CreatedDateTime < GETDATE() - 45

		  
		  PRINT  '-------- DELETE RECORDS FROM REASSIGN BOOKING TABLE COMPLETED--------';

		  SELECT COUNT(*)  FROM [dbo].[tblBooking_Archive]
  COMMIT TRAN
  END TRY
  BEGIN CATCH
  begin
  	ROLLBACK TRAN
  end
  END CATCH
END
GO
