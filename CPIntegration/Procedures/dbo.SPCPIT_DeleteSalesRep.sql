SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_DeleteSalesRep]
(
@SalesRepID int ,
@UserID int 

)
As
Begin
BEGIN TRY
BEGIN tran 

update  [tblSalesRep] set IsActive =0 , InactiveDateTime =GETDATE() ,UpdatedDateTime=GETDATE(),UpdatedBy=@UserID   where SalesRepID=@SalesRepID
COMMIT tran 

Select 'Success' as OutPutMsg 
END TRY
BEGIN CATCH
begin
    rollback tran
	
	INSERT INTO [dbo].[tblErrorLog] ([Error],[ErrorCode],[ErrorType],[System], [Function],
				[UTCDateTime],[CreatedDateTime],[LocalTime],CreatedBy )
				VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,ERROR_SEVERITY(),ERROR_PROCEDURE(),
				'Local',
				'[SPCPIT_DeleteSalesRep]',
				SYSDATETIMEOFFSET(),getdate(), convert(time,GETDATE(),103),-1
				)

				Select 'Error' as [OutPutMsg], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
end
END CATCH

End






 
GO
