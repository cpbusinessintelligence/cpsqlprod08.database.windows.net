SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NowGo_SetCustomerProntoAccountCode_Test]

AS
BEGIN

    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRY
		
		-- GET Customer's Pronto Code
		UPDATE	STG
		SET 
				STG.[ProntoAccountCode] = CUS.[ProntoAccountCode]
		FROM	[tblCouponSalesSLSExportStaging_Test] STG
		INNER JOIN [dbo].[tblCosmosCustomers_Test] CUS
		ON		STG.[accountCode] = CUS.[Code]
		AND		TRIM(STG.[branch]) =  TRIM(CUS.[branch]);

		UPDATE	STG
		SET 
				STG.[ProntoAccountCode] = CUS.[ProntoAccountCode]
		FROM	[tblCouponSalesSLSExportStaging_Test] STG
		INNER JOIN [dbo].[tblCosmosCustomers_Test] CUS
		ON		STG.[accountCode] = CUS.[LocationNumber]
		AND		TRIM(STG.[branch]) =  TRIM(CUS.[branch]);

	END TRY

	BEGIN CATCH
	
		THROW;

	END CATCH	
END
GO
