SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_CreatePrepaidAccountDetails]
(
@SalesCode varchar(50),
@AccountType int,
@IndustryCode int,
@CreatedBy int,
@PrepaidAccountID int,
@StartDate datetime,
@LastUsed datetime,
@Added varchar(50),
@Eft bit,
@Status bit,
@RegularRun bit, 
@PickupDays varchar(50),
@RepStart datetime,
@Comments varchar(100)

)
AS
Begin
Declare @SalesRepID Int

Insert into [dbo].[tblSalesRep](SalesCode,AccountType,IndustryCode,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy)
values (@SalesCode,@AccountType,@IndustryCode,GETDATE(),GETDATE(),GETDATE(),@CreatedBy)
set @SalesRepID =SCOPE_IDENTITY()

Insert into [dbo].[tblPrepaidAccountDetail] (PrepaidAccountID,StartDate,LastUsed,Added,AccountType,IndustryCode,Eft,Status,RegularRun,PickupDays,SalesRepID,
RepStart,Comments,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy)
values (@PrepaidAccountID,@StartDate,@LastUsed,@Added,@AccountType,@IndustryCode,@Eft,@Status,@RegularRun,@PickupDays,@SalesRepID,
@RepStart,@Comments,GETDATE(),GETDATE(),GETDATE(),@CreatedBy)
Select SCOPE_IDENTITY() as ID
End
GO
