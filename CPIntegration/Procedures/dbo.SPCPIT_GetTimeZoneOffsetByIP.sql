SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [SPCPIT_GetTimeZoneOffsetByIP] 
	-- Add the parameters for the stored procedure here
	@ipAddress nvarchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT time_zone FROM IP2Location_DB where @ipAddress between ip_from and ip_to 
END

--Exec SPCPIT_GetTimeZoneOffsetByIP 1731144835
GO
