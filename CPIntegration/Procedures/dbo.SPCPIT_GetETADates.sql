SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[SPCPIT_GetETADates]  --'PINE'
@PickupDate nvarchar(50)
As
Begin     
	SELECT * FROM tblPublicHolidays where CAST(Date as datetime)>=CAST(@PickupDate as datetime)
End


GO
