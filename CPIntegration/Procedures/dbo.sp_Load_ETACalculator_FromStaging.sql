SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      <Author, , Name>
-- Create Date: <Create Date, , >
-- Description: <Description, , >
-- =============================================
CREATE PROCEDURE sp_Load_ETACalculator_FromStaging
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON
	--Backup Table--
	Declare @date varchar(200),@sql varchar(1000)
	SET @date = replace(convert(varchar(8), getdate(), 112)+convert(varchar(8), getdate(), 114), ':','')

	SET @sql='SELECT *
	INTO [dbo].[ETACalculator_' + CONVERT(varchar(30),@date,113) +']
	FROM [CPIntegration].[dbo].[ETACalculator] '	
	print @Sql
	EXEC(@Sql)

	--Truncate
	Truncate Table ETACalculator

	Insert Into ETACalculator
	(
		ETAID
		,FromZone
		,ToZone
		,PrimaryNetworkCategory	
		,SecondaryNetworkCategory	
		,ETA	
		,FromETA	
		,ToETA	
		,[1stPickupCutOffTime]
		,[1stDeliveryCutOffTime]	
		,[1stAllowedDays]
		,[2ndPickupCutOffTime]	
		,[2ndDeliveryCutOffTime]	
		,[2ndAllowedDays]
		,AIR_ETA	
		,AIR_FromETA	
		,AIR_ToETA	
		,AIR_1stDeliveryCutOffTime	
		,AIR_1stAllowedDays	
		,AIR_2ndDeliveryCutOffTime	
		,AIR_2ndAllowedDays	
		,SameDay_ETA	
		,SameDay_FromETA	
		,SameDay_ToETA	
		,SameDay_1stDeliveryCutOffTime	
		,SameDay_1stAllowedDays	
		,SameDay_2ndDeliveryCutOffTime	
		,SameDay_2ndAllowedDays	
		,AddWho	
		,AddDateTime	
		,EditWho	
		,EditDateTime	
		,Domestic_OffPeak_ETA	
		,Domestic_OffPeak_FromETA	
		,Domestic_OffPeak_ToETA	
		,Domestic_OffPeak_1stDeliveryCutOff	
		,Domestic_OffPeak_1stAllowedDays	
		,Domestic_OffPeak_2ndDeliveryCutOff	
		,Domestic_OffPeak_2ndAllowedDays
	)
	Select
		ETAID
		,FromZone	
		,ToZone	
		,PrimaryNetworkCategory	
		,SecondaryNetworkCategory	
		,ETA	
		,FromETA	
		,ToETA	
		,[1stPickupCutOffTime]	
		,[1stDeliveryCutOffTime]	
		,[1stAllowedDays]	
		,[2ndPickupCutOffTime]
		,[2ndDeliveryCutOffTime]	
		,[2ndAllowedDays]	
		,AIR_ETA	
		,AIR_FromETA	
		,AIR_ToETA	
		,AIR_1stDeliveryCutOffTime	
		,AIR_1stAllowedDays	
		,AIR_2ndDeliveryCutOffTime	
		,AIR_2ndAllowedDays	
		,SameDay_ETA	
		,SameDay_FromETA	
		,SameDay_ToETA	
		,SameDay_1stDeliveryCutOffTime	
		,SameDay_1stAllowedDays	
		,SameDay_2ndDeliveryCutOffTime	
		,SameDay_2ndAllowedDays	
		,AddWho	
		,AddWhen	
		,EditWho
		,EditWhen
		,Domestic_OffPeak_ETA	
		,Domestic_OffPeak_FromETA	
		,Domestic_OffPeak_ToETA	
		,Domestic_OffPeak_1stDeliveryCutOff	
		,Domestic_OffPeak_1stAllowedDays	
		,Domestic_OffPeak_2ndDeliveryCutOff	
		,Domestic_OffPeak_2ndAllowedDays	
	From Load_ETA_Calculator

END
GO
