SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CPIT_GETTrackingDetailByTrackingNumber]
@TrackinNumber varchar(50)=''
AS
BEGIN
	SELECT DISTINCT LABELNUMBER,WORKFLOWTYPE,OUTCOME,DriverId,
	EventDateTime,DBO.Fn_GETMessageUsingWorkFOutCome(WORKFLOWTYPE,OUTCOME)AS MESSAGE  
	FROM tbl_TrackingEvent  
	WHERE LabelNumber=@TrackinNumber order by EventDateTime asc	
END
 

GO
