SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author		:	Rajkumar Sahu
-- Create date	:	23-Sep-2019
-- Description	:	Insert video file Information to show in cp-web-IT application
-- =============================================
Create Procedure [dbo].[SpCppl_CreateVideoMaster]
@Title Nvarchar(100)=null,
@Description Nvarchar(300)=null,
@FileName Nvarchar(100)=null,
@Url Nvarchar(500)=null,
@UTCDateTime datetime=null, 
@CreatedBy Nvarchar(50)=null,
@ThumbnailName Nvarchar(100)=null,
@ThumbnailUrl  Nvarchar(500)=null

AS

BEGIN
	INSERT INTO tblvideoMaster(Title,Description,FileName,Url,UTCDateTime,CreatedDateTime,CreatedBy,ThumbnailName,ThumbnailUrl)
	VALUES(@Title,@Description,@FileName,@Url,Getdate(),@UTCDateTime,IIF(ISNULL(@CreatedBy,'')='','System',@CreatedBy),@ThumbnailName,@ThumbnailUrl)
	Select @@identity
END
GO
