SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[NowGo_SetProntoInsuranceCode]

AS
BEGIN

    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN TRY
		
		-- GET Pronto Insurance Code
		UPDATE	STG
		SET 
				STG.[ProntoInsuranceCode] = [dbo].[fn_NowGo_GetProntoInsuranceCode]([insuranceCode])
		FROM	[tblCouponSalesSLSExportStaging] STG

	END TRY

	BEGIN CATCH
	
		THROW;

	END CATCH	
END
GO
