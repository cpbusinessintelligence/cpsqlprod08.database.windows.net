SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[SPCPIT_UpdateReassignBooking]

@ReBookingID int,
@NowGoJobID varchar(50) = null,
@UpdatedBy varchar(50)= null,
@Response varchar(max) = null,
@NowGoRequest varchar(max) = null,
@NowGoResponse varchar(max) = null,
@DriverExtRef varchar(100)=null,
@UTCDateTime datetimeoffset(7)= null ,
@LocalDateTime datetime= null 
As 
Begin
SET NOCOUNT ON;
   SET XACT_ABORT ON;
     BEGIN TRANSACTION
	   	SAVE TRANSACTION SavePoint;
           BEGIN TRY	
                    Update [tblReassignBooking] set [NowGoJobID] = @NowGoJobID, [IsProcessed]  = 1 ,[UpdatedDateTime]=GetDate(), [UpdatedBy] = @UpdatedBy, [Response] = @Response, 
					[NowGoRequest] = @NowGoRequest, [NowGoResponse] = @NowGoResponse,  [DriverExtRef] = @DriverExtRef, [UTCDateTime] = @UTCDateTime, [LocalDateTime] = @LocalDateTime
                    where [ReBookingID] = @ReBookingID

                    Select SCOPE_IDENTITY() as ReBookingID
     COMMIT TRANSACTION			
           END TRY  
       BEGIN CATCH
            ROLLBACK TRANSACTION;
	   END CATCH

End
GO
