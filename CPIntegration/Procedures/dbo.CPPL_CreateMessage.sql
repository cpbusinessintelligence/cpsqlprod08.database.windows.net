SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[CPPL_CreateMessage]
(
@ThreadID int ,
@BookingNumber varchar(50)=null,
@SourceSystem   varchar(50)=null,
@CPMessageID  varchar(100)=null,
@Branch  varchar(10)=null,
@Message nvarchar(max) ,
@Comment varchar(100)=null,
@SendBy varchar(100)=null,
@SendTo varchar(100),
@EmmId varchar(50)  =null,
@IsProcessed bit =0,
@driverRef nvarchar(100)=null,
@CPRequest nvarchar(max) =null,
@NowGoRequest nvarchar(max)=null,
@UTCDateTime datetimeoffset(7)= null ,
@LocalDateTime datetime= null ,
@CreatedBy varchar(50) =null
)
As
Begin
Declare @MessageID int 
Insert into tblMessage (ThreadID,BookingNumber,SourceSystem,CPMessageID,Branch,MessageDatetime,Message,Comment,SendBy,SendTo,
EmmId,IsProcessed,driverRef,CPRequest,NowGoRequest,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy)
values (@ThreadID,@BookingNumber,@SourceSystem,@CPMessageID,@Branch,GETDATE(),@Message,@Comment,@SendBy,@SendTo,
@EmmId,@IsProcessed,@driverRef,@CPRequest,@NowGoRequest,@UTCDateTime,@LocalDateTime,GETDATE(),@CreatedBy)

select SCOPE_IDENTITY()  as  MessageID 


End
GO
