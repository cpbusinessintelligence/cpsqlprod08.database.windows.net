SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SPCPIT_CreateBooking]

@BookingType varchar(50)= null ,
@BookingRefNo varchar(50)= null ,
@Branch varchar(10)= null ,
@AccountCode varchar(50)= null ,
@PickupCompanyName varchar(50)= null ,
@PickupContactName varchar(50)= null ,
@PickupAddress1 varchar(250)= null ,
@PickupAddress2 varchar(250)= null ,
@PickupAddress3 varchar(250)= null ,
@PickupSuburb varchar(100)= null ,
@PickupState varchar(10)= null ,
@PickupPostCode int= null ,
@PickupPhone varchar(30)=null,
@PickupEmail varchar(50)= null ,
@PickupDate date= null ,
@PickupTime time(7)= null ,
@PickupFrom varchar(20)= null ,
@OrderCoupons varchar(50)= null ,
@DeliveryCompanyName varchar(50)= null ,
@DeliveryContactName varchar(50)= null ,
@DeliveryAddress1 varchar(250)= null ,
@DeliveryAddress2 varchar(250)= null ,
@DeliveryAddress3 varchar(250)= null ,
@DeliverySuburb varchar(100)= null ,
@DeliveryState varchar(10)= null ,
@DeliveryPostcode int= null ,
@DeliveryEmail varchar(50)= null ,
@DeliveryPhone varchar(30)=null,
@UTCDateTime datetimeoffset(7)= null ,
@LocalDateTime datetime= null ,
@CreatedBy varchar(50)= null,
@Request varchar(max) = null,
@NowGoRequest varchar(max) = null

As 
Begin

INSERT INTO [dbo].[tblBooking]
           ([BookingType],[BookingRefNo],[Branch],[AccountCode],[PickupCompanyName],[PickupContactName],[PickupAddress1],[PickupAddress2],[PickupAddress3],
		   [PickupSuburb],[PickupState],[PickupPostCode],[PickupPhone],[PickupEmail],[PickupDate], [PickupTime],[PickupFrom],[OrderCoupons],[DeliveryCompanyName],[DeliveryContactName],
		   [DeliveryAddress1],[DeliveryAddress2],[DeliveryAddress3],[DeliverySuburb],[DeliveryState],[DeliveryPostcode],[DeliveryEmail],[DeliveryPhone],[IsProcessed],
		   [UTCDateTime],[LocalDateTime],[CreatedDateTime],[CreatedBy],[NowGoJobID],[Request],[Response],[NowGoRequest],[NowGoResponse])
     VALUES
           (
			@BookingType,@BookingRefNo,@Branch,@AccountCode,@PickupCompanyName,@PickupContactName,@PickupAddress1,@PickupAddress2,@PickupAddress3,@PickupSuburb,
			@PickupState,@PickupPostCode,@PickupPhone,@PickupEmail,@PickupDate,@PickupTime ,@PickupFrom ,@OrderCoupons,@DeliveryCompanyName,@DeliveryContactName,@DeliveryAddress1,@DeliveryAddress2,
			@DeliveryAddress3,@DeliverySuburb,@DeliveryState,@DeliveryPostcode ,@DeliveryEmail,@DeliveryPhone,0, @UTCDateTime ,@LocalDateTime,GetDate(),@CreatedBy,null,@Request,null,@NowGoRequest,null
           )
Select SCOPE_IDENTITY() as BookingID

End





GO
