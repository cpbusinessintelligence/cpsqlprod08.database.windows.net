SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CPPL_DeleteTrackingEventMapping]
@TrackingEventMappingId Int =0,
@UpdatedBy Nvarchar(50)='-1'
AS 
BEGIN 
	UPDATE dbo.tblTrackingEventMapping SET UpdatedDateTime=GETDATE(),UpdatedBy=@UpdatedBy,IsDeleted=1 WHERE TrackingEventMappingId=@TrackingEventMappingId
	Select 'Success' As OutPutMsg 
	--EXEC CPPL_GetTrackingEventMapping
END 
GO
