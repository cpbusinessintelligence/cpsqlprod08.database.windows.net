SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SPCPIT_GetNonCustomisedRateCardDetail]  --'PINE'
@Service nvarchar(50),
@OriginZone nvarchar(50),
@DestinationZone nvarchar(50),
@EffectiveDate nvarchar(50)
As
Begin     
	 SELECT EffectiveDate,MinimumCharge,BasicCharge,FuelOverride,FuelPercentage,Rounding,Cast(ChargePerKilo as decimal(10,4)) as ChargePerKilo 
	 FROM tblNonCustomisedRateCardDetail 
	 where Service =@Service  
	 and OriginZone = @OriginZone  
	 and DestinationZone = @DestinationZone  
	 and EffectiveDate <=@EffectiveDate 
End



GO
