SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*
*/
-- =============================================
-- Author		:	Arpan Sharma
-- Create date	:	31-July-2020
-- Description	:	To get all detail of Safetr Driver for view in CPGo project
-- =============================================


CREATE Procedure [dbo].[SpCppl_GetAllSafetyModuleContentAdmin]
@ModuleContentId int=null,
@ModuleId int=null,
@Title varchar(200)=null,
@FileName nvarchar(500)=null

As
BEGIN 
	SELECT mdc.*, md.ModuleName FROM tblDriverSafetyModuleContent mdc inner join tblDriverSafetyModule md on md.ID=mdc.DriverySafetyModuleID WHERE 1=1 and	
	CASE WHEN isnull(@ModuleContentId,0) = 0 THEN 0 ELSE isnull(mdc.ID,0)  END = isnull(@ModuleContentId,0)	AND
	CASE WHEN isnull(@ModuleId,0) = 0 THEN 0 ELSE isnull(mdc.DriverySafetyModuleID,0)  END = isnull(@ModuleId,0) AND
	CASE WHEN isnull(@Title,'') = '' THEN '' ELSE isnull(mdc.Title,'')  END like '%'+ isnull(@Title,'')+'%' AND
	CASE WHEN isnull(@FileName,'') = '' THEN '' ELSE isnull(mdc.ImagePath,'')  END like '%'+ isnull(@FileName,'')+'%'
	ORDER BY mdc.ID DESC
END 

GO
