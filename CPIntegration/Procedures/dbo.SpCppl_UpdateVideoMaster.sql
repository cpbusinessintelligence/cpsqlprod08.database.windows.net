SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author		:	Rajkumar Sahu
-- Create date	:	09-Oct-2019
-- Description	:	update video information from manage video screen
-- =============================================
CREATE Procedure [dbo].[SpCppl_UpdateVideoMaster]
@VideoId int=0,
@Title nVARCHAR(200)='',
@Description nVARCHAR(600)='', 
@FileName nVARCHAR(200)='',
@Url nVARCHAR(MAX)='',
@UpdatedDateTime datetime=null,
@UpdatedBy Nvarchar(100)='',
@ThumbnailName Nvarchar(100)='',
@ThumbnailUrl nVARCHAR(200)='' 
As
BEGIN	 
	UPDATE tblVideoMaster SET
	Title=@Title,
	Description=@Description,
	FileName=iif (isnull(@FileName,'')='',FileName,@FileName),
	Url=iif (isnull(@Url,'')='',Url,@Url), 	
	ThumbnailName=iif (isnull(@ThumbnailName,'')='',ThumbnailName,@ThumbnailName),
	ThumbnailUrl=iif (isnull(@ThumbnailUrl,'')='',ThumbnailUrl,@ThumbnailUrl),
	UpdatedDateTime=@UpdatedDateTime,
	UpdatedBy=IIF(ISNULL(@UpdatedBy,'')='','System',@UpdatedBy)  
	WHERE VideoId=@VideoId

	select @VideoId
END 
GO
