SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author		:	Rajkumar Sahu
-- Create date	:	23-Sep-2019
-- Description	:	SELECT VIDEO Information to show in cp-web-IT application
-- =============================================
CREATE Procedure [dbo].[SpCppl_GetPdfMaster]
@PdfId int=0,
@Title Nvarchar(200)='',
@Description Nvarchar(200)='',
@FileName Nvarchar(200)='',
@Url Nvarchar(200)=''
As
BEGIN 
	SELECT PdfId,Title,Description,FileName,Url,ThumbnailName,ThumbnailUrl FROM tblPdfMaster WHERE IsActive=1 
	AND
	CASE WHEN isnull(@PdfId,0) = 0 THEN 0 ELSE isnull(PdfId,0)  END = isnull(@PdfId,0)
	AND
	CASE WHEN isnull(@Title,'') = '' THEN '' ELSE isnull(Title,'')  END like '%'+ isnull(@Title,'')+'%'
	AND
	CASE WHEN isnull(@Description,'') = '' THEN '' ELSE isnull(Description,'')  END like '%'+ isnull(@Description,'')+'%'
	AND
	CASE WHEN isnull(@FileName,'') = '' THEN '' ELSE isnull(FileName,'')  END like '%'+ isnull(@FileName,'')+'%'
	AND
	CASE WHEN isnull(@Url,'') = '' THEN '' ELSE isnull(Url,'')  END like '%'+  isnull(@Url,'')+'%'
	ORDER BY PdfId DESC
END 
GO
