SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[SPCPIT_GetCustomisedRateCardDetail]  --'PINE'
@Account nvarchar(50),
@Service nvarchar(50),
@OriginZone nvarchar(50),
@DestinationZone nvarchar(50),
@EffectiveDate nvarchar(50)
As
Begin     
 
	 SELECT EffectiveDate,MinimumCharge,BasicCharge,FuelOverride,FuelPercentage,Rounding,ChargePerKilo 
	 FROM tblCustomisedRateCardDetail 
	 where Account = @Account  
	 and Service = @Service  
	 and OriginZone = @OriginZone  
	 and DestinationZone = @DestinationZone  
	 and EffectiveDate <= @EffectiveDate 	 
End

GO
