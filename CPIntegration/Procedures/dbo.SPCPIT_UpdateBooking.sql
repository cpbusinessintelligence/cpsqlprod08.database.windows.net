SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[SPCPIT_UpdateBooking]

@BookingID int,
@NowGoJobID varchar(50) = null,
@UpdatedBy varchar(50)= null,
@Response varchar(max) = null,
@NowGoRequest varchar(max) = null,
@NowGoResponse varchar(max) = null,
@DriverRef varchar(100)=null,
@UTCDateTime datetimeoffset(7)= null ,
@LocalDateTime datetime= null 
As 
Begin

--Update [tblBooking] set [NowGoJobID] = @NowGoJobID, [IsProcessed]  = 1 ,[UpdatedDateTime]=GetDate(), [UpdatedBy] = @UpdatedBy, [Response] = @Response, [NowGoRequest] = @NowGoRequest, [NowGoResponse] = @NowGoResponse, [DriverRef]=@DriverRef  where [BookingID] = @BookingID
Update [tblBooking] set [NowGoJobID] = @NowGoJobID, [IsProcessed]  = 1 ,[UpdatedDateTime]=GetDate(), [UpdatedBy] = @UpdatedBy, [Response] = @Response, [NowGoRequest] = @NowGoRequest, [NowGoResponse] = @NowGoResponse,  [DriverRef] = @DriverRef, 
[UTCDateTime] = @UTCDateTime, [LocalDateTime] = @LocalDateTime
where [BookingID] = @BookingID

Select SCOPE_IDENTITY() as BookingID

End






GO
