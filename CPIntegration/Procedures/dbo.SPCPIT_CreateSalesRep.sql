SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_CreateSalesRep]
(
 @SalesCode varchar(50),
 @SalesRepName varchar(100),
 @AccountType int,
 @IndustryCode int,
 @LocalDateTime datetime,
 @CreatedBy int 
)
AS
Begin
BEGIN TRY
BEGIN tran 
Insert into [tblSalesRep] (SalesCode,SalesRepName,AccountType,IndustryCode,UTCDateTime,LocalDateTime,CreatedDateTime,CreatedBy)
values (@SalesCode,@SalesRepName,@AccountType,@IndustryCode,SYSDATETIMEOFFSET(),@LocalDateTime,GETDATE(),@CreatedBy)
Select SCOPE_IDENTITY() as ID
COMMIT tran 
END TRY
BEGIN CATCH
begin
    rollback tran
	
	INSERT INTO [dbo].[tblErrorLog] ([Error],[ErrorCode],[ErrorType],[System], [Function],
				[UTCDateTime],[CreatedDateTime],[LocalTime],CreatedBy )
				VALUES (cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() ,ERROR_SEVERITY(),ERROR_PROCEDURE(),
				'Local',
				'[SPCPIT_CreateSalesRep]',
				SYSDATETIMEOFFSET(),getdate(), convert(time,GETDATE(),103),-1
				)

				Select 'Error' as [OutPutMsg], cast(ERROR_LINE() as varchar) +' : '+ERROR_MESSAGE() as ErrorMessage
end
END CATCH

End
GO
