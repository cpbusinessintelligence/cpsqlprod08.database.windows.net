SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- exec [dbo].[SPCPIT_GetBookingByRefNumber] @BooingRefNo='sydney-20180502-321351'
CREATE PROCEDURE [dbo].[SPCPIT_GetBookingByRefNumber]
@BooingRefNo varchar(50)=''
AS
BEGIN 
		SELECT TOP 100 BookingID,BookingType,BookingRefNo,Branch,AccountCode,PickupCompanyName,PickupContactName,PickupAddress1,PickupAddress2,PickupAddress3,PickupSuburb,
		PickupState,PickupPostCode,PickupEmail,convert(varchar(30), PickupDate,109) PickupDate,cast( PickupTime as varchar(8)) PickupTime,PickupFrom,OrderCoupons,DeliveryCompanyName,DeliveryContactName,DeliveryAddress1,DeliveryAddress2,
		DeliveryAddress3,DeliverySuburb,DeliveryState,DeliveryPostcode,DeliveryEmail,MessageId,IsProcessed,UTCDateTime,LocalDateTime,NowGoJobID,Request,Response,NowGoRequest,
		NowGoResponse,DriverRef,DriverExtRef,DriverName,DriverEmail,convert(varchar(30), CreatedDateTime,109) as CreatedDateTime,CreatedBy,UpdatedDateTime,UpdatedBy ,CreatedDateTime CreatedDateTime1,Request,Response,NowGoRequest,NowGoResponse
		FROM tblBooking  WHERE CASE WHEN isnull(@BooingRefNo,'') = '' THEN '' ELSE isnull(BookingRefNo,'')  END = isnull(@BooingRefNo,'') 
 		ORDER BY CreatedDateTime1 DESC  
END
GO
