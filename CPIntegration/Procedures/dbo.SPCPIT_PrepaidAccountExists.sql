SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[SPCPIT_PrepaidAccountExists] @accountCode as varchar(50),@branch as varchar(50)  
as 
begin

	--Select PrepaidAccountID from [dbo].[tblPrepaidAccount] where AccountCode = @accountCode and Branch = @branch and IsActive <> 0
	Select PrepaidAccountID,IsActive from [dbo].[tblPrepaidAccount] where AccountCode = @accountCode and Branch = @branch

end
GO
