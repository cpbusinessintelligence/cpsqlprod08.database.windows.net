SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[SPCPIT_GetRateCardHeader]  --'PINE'
@Service nvarchar(50),
@ValidFrom nvarchar(50)
As
Begin     
	 
	 SELECT ValidFrom, ChargeMethod, VolumetricDivisor, FuelOverride, cast(Fuel as nvarchar(max)) as Fuel FROM tblRateCardHeader 
	 where Service = @Service and ValidFrom <= @ValidFrom
	 
End


GO
