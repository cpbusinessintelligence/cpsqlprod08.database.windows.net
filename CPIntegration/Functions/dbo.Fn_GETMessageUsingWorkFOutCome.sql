SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_GETMessageUsingWorkFOutCome]
(@WorkFlow Nvarchar(50),@OutCome Nvarchar(50))
returns Nvarchar(max)
As
Begin
Declare @Message Nvarchar(max)
Select @Message = Message from tblTrackingEventMapping WHERE WorkFlow=@WorkFlow AND OutCome=@OutCome and isdeleted=0
RETURN @Message
End

 
GO
