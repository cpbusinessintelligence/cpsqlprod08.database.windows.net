SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Fn_RtnPODImageIdByEventId]
(@EventId Int)
Returns varchar(50)
AS
Begin 
Declare @PODID varchar(50)=''
SELECT @PODID=PodImageId from VIEW_ImagePodList WHERE EVENTID=@EventId
return @PODID
End 

--SELECT @PODID= PodImageId from CPSQLPROD02.tracking.dbo.[tbl_TrackingEvent] Etv 
--Inner Join CPSQLPROD02.tracking.dbo.[tbl_TrackingEvent_PODImage] AS PODImage 
--On Etv.EventId=PODImage.Eventid  
--WHERE Etv.EVENTID=@EventId
 
GO
