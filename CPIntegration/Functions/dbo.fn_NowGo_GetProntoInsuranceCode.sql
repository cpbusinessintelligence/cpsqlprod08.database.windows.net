SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_NowGo_GetProntoInsuranceCode](
	@NowGoInsuranceCode VARCHAR(5)
)

RETURNS VARCHAR(3)

AS

BEGIN
	RETURN 
		CASE 
		WHEN @NowGoInsuranceCode = 'IN1' THEN NULL
		WHEN @NowGoInsuranceCode = 'INS1' THEN NULL
		WHEN @NowGoInsuranceCode = 'INS2' THEN 'IN2'
		WHEN @NowGoInsuranceCode = 'INS3' THEN 'IN3'
		WHEN @NowGoInsuranceCode = 'INS4' THEN 'IN4'
		WHEN @NowGoInsuranceCode = 'INS5' THEN 'IN5'
		WHEN @NowGoInsuranceCode = 'INS6' THEN 'IN6'
		WHEN @NowGoInsuranceCode = 'INS7' THEN 'IN7'
		WHEN @NowGoInsuranceCode = 'INS8' THEN 'IN8'
		WHEN @NowGoInsuranceCode = 'INS9' THEN 'IN9'
		ELSE NULL
	END
END
GO
