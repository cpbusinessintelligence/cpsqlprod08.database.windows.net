CREATE TYPE [dbo].[AuditTrailTable]
AS TABLE (
		[EventType]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TableName]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ColumnName]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[OldValue]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewValue]          [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LocalDateTime]     [datetime] NOT NULL,
		[CreatedBy]         [int] NULL
)
GO
