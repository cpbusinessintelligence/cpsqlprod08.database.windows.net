SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblImageMaster_JS_BUP_20200423] (
		[ImageId]              [int] IDENTITY(1, 1) NOT NULL,
		[Title]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]          [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileName]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Url]                  [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]             [bit] NULL,
		[InActiveDatetime]     [datetime] NULL,
		[InActiveBy]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]          [datetime] NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailName]        [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailUrl]         [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblImageMaster_JS_BUP_20200423] SET (LOCK_ESCALATION = TABLE)
GO
