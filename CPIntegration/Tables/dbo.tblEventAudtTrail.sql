SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblEventAudtTrail] (
		[AuditTrailID]        [int] IDENTITY(1, 1) NOT NULL,
		[EventType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[TableName]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ColumnName]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[OldValue]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewValue]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]         [datetimeoffset](7) NOT NULL,
		[LocalDateTime]       [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[UpdatedBy]           [int] NULL,
		[UpdatedDateTime]     [datetime] NULL,
		CONSTRAINT [PK_tblEventAudtTrail]
		PRIMARY KEY
		CLUSTERED
		([AuditTrailID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblEventAudtTrail] SET (LOCK_ESCALATION = TABLE)
GO
