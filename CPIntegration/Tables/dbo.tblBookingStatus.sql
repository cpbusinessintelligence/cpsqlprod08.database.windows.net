SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBookingStatus] (
		[ID]                      [int] IDENTITY(1, 1) NOT NULL,
		[uniqueID]                [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[accountNumber]           [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[branch]                  [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bookingType]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[itemNumber]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[lastStatus]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[lastUpdatedDateTime]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[driverNumber]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[responseCode]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[statusCode]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[msg]                     [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[bookingStatusID]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedBy]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]         [datetime] NULL,
		[UpdatedBy]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]         [datetime] NULL,
		CONSTRAINT [PK_tblBookingStatusTracking]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[tblBookingStatus] SET (LOCK_ESCALATION = TABLE)
GO
