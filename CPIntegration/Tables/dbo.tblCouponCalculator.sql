SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCouponCalculator] (
		[ID]               [int] IDENTITY(1, 1) NOT NULL,
		[FromZone]         [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ToZone]           [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WB1Count]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WB2Count]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WB3Count]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SatchelCount]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblCouponCalculator]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[tblCouponCalculator] SET (LOCK_ESCALATION = TABLE)
GO
