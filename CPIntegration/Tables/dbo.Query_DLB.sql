SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Query_DLB] (
		[PrepaidAccountID]           [int] NULL,
		[NationalLocationNumber]     [varchar](103) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                     [char](16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Code]                       [varchar](134) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerName]               [char](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Location]                   [char](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]                   [int] NOT NULL,
		[InActiveDateTime]           [datetime2](3) NULL,
		[UTCDateTime]                [datetime2](3) NULL,
		[LocalDateTime]              [datetime2](3) NULL,
		[CreatedDateTime]            [datetime2](3) NULL,
		[CreatedBy]                  [int] NOT NULL,
		[UpdatedDateTime]            [datetime2](3) NULL,
		[UpdatedBy]                  [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Query_DLB] SET (LOCK_ESCALATION = TABLE)
GO
