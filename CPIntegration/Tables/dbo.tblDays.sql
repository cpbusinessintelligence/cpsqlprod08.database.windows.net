SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDays] (
		[DayID]                [int] NOT NULL,
		[DayCode]              [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DayName]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsActive]             [bit] NOT NULL,
		[InActiveDateTime]     [datetime] NULL,
		[UTCDateTime]          [datetimeoffset](7) NOT NULL,
		[LocalDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblWeekDays]
		PRIMARY KEY
		CLUSTERED
		([DayID])
)
GO
ALTER TABLE [dbo].[tblDays] SET (LOCK_ESCALATION = TABLE)
GO
