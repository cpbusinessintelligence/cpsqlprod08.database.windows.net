SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTariffAcount_bkup_fromCosmos] (
		[Accountcode]           [int] NOT NULL,
		[TariffAccountCode]     [int] NOT NULL,
		[Shortname]             [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NotUsed]               [bit] NOT NULL,
		[Warehouse]             [bit] NOT NULL,
		[ClearFlag]             [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[FreightCode]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TermDisc]              [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Territory]             [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[id]                    [varchar](42) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[tblTariffAcount_bkup_fromCosmos] SET (LOCK_ESCALATION = TABLE)
GO
