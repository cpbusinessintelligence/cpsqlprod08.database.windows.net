SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTariffAccount_bkup] (
		[id]                    [bigint] IDENTITY(1, 1) NOT NULL,
		[Accountcode]           [float] NULL,
		[TariffAccountCode]     [float] NULL,
		[Shortname]             [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NotUsed]               [float] NULL,
		[Warehouse]             [float] NULL,
		[ClearFlag]             [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FreightCode]           [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TermDisc]              [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Territory]             [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblTariffAccount_bkup] SET (LOCK_ESCALATION = TABLE)
GO
