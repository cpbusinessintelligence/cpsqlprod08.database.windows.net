SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblVideoMaster] (
		[VideoId]              [int] IDENTITY(1, 1) NOT NULL,
		[Title]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]          [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileName]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Url]                  [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]             [bit] NULL,
		[InActiveDatetime]     [datetime] NULL,
		[InActiveBy]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]          [datetime] NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailName]        [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailUrl]         [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__tblVideo__BAE5126A33D77749]
		PRIMARY KEY
		CLUSTERED
		([VideoId])
)
GO
ALTER TABLE [dbo].[tblVideoMaster]
	ADD
	CONSTRAINT [DF__tblVideoM__Creat__0035E158]
	DEFAULT ('System') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblVideoMaster]
	ADD
	CONSTRAINT [DF__tblVideoM__IsAct__7F41BD1F]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblVideoMaster] SET (LOCK_ESCALATION = TABLE)
GO
