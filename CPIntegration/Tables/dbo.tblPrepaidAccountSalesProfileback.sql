SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPrepaidAccountSalesProfileback] (
		[tblPrepaidAccountSalesProfileID]     [int] IDENTITY(1, 1) NOT NULL,
		[PrepaidAccountID]                    [int] NOT NULL,
		[InventoryID]                         [int] NOT NULL,
		[WarehouseCode]                       [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsDiscountApplicable]                [bit] NULL,
		[DiscountPercentage]                  [decimal](4, 2) NULL,
		[DiscountStartDate]                   [datetime] NULL,
		[DiscountEndDate]                     [datetime] NULL,
		[SalesPrice]                          [decimal](18, 4) NOT NULL,
		[IsActive]                            [bit] NULL,
		[InActiveDateTime]                    [datetime] NULL,
		[UTCDateTime]                         [datetimeoffset](7) NOT NULL,
		[LocalDateTime]                       [datetime] NOT NULL,
		[CreatedDateTime]                     [datetime] NOT NULL,
		[CreatedBy]                           [int] NOT NULL,
		[UpdatedDateTime]                     [datetime] NULL,
		[UpdatedBy]                           [int] NULL
)
GO
ALTER TABLE [dbo].[tblPrepaidAccountSalesProfileback] SET (LOCK_ESCALATION = TABLE)
GO
