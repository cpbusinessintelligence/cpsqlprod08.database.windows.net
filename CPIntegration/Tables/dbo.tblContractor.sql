SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblContractor] (
		[ID]                  [int] IDENTITY(1, 1) NOT NULL,
		[Branch]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverNumber]        [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ContractorType]      [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverName]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address1]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address2]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Address3]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]            [nvarchar](4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ProntoId]            [nvarchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblContractor]
	ADD
	CONSTRAINT [DF_tblContractor_CreatedDateTime]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblContractor] SET (LOCK_ESCALATION = TABLE)
GO
