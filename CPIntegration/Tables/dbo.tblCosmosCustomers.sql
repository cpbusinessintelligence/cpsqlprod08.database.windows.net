SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCosmosCustomers] (
		[ID]                    [int] IDENTITY(1, 1) NOT NULL,
		[Branch]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CustomerId]            [int] NULL,
		[Code]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Name]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OpenTime]              [time](7) NULL,
		[CloseTime]             [time](7) NULL,
		[DriverNumber]          [int] NULL,
		[ProntoAccountCode]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LocationNumber]        [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblCosmosCustomers]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[tblCosmosCustomers] SET (LOCK_ESCALATION = TABLE)
GO
