SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBranch] (
		[BranchID]             [int] IDENTITY(1, 1) NOT NULL,
		[BranchName]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[BranchCode]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsActive]             [bit] NULL,
		[InActiveDateTime]     [datetime] NULL,
		[UTCDateTime]          [datetimeoffset](7) NOT NULL,
		[LocalDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblBranch]
		PRIMARY KEY
		CLUSTERED
		([BranchID])
)
GO
ALTER TABLE [dbo].[tblBranch] SET (LOCK_ESCALATION = TABLE)
GO
