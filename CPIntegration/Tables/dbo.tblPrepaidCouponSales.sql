SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPrepaidCouponSales] (
		[CouponSalesID]       [int] IDENTITY(1, 1) NOT NULL,
		[AccountCode]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Branch]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DLBBarcode]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InvoiceNumber]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Serialnumber]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Prefix]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Quantity]            [int] NOT NULL,
		[Value]               [decimal](18, 2) NOT NULL,
		[Discount]            [decimal](18, 2) NOT NULL,
		[Insurance]           [decimal](18, 2) NOT NULL,
		[ValueExGST]          [decimal](18, 2) NOT NULL,
		[GST]                 [decimal](18, 2) NOT NULL,
		[ValueIncGST]         [decimal](18, 2) NOT NULL,
		[TotalValue]          [decimal](18, 2) NULL,
		[UTCDateTime]         [datetime] NULL,
		[LocalDateTime]       [datetime] NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblPrepaidCouponSales]
		PRIMARY KEY
		CLUSTERED
		([CouponSalesID])
)
GO
ALTER TABLE [dbo].[tblPrepaidCouponSales] SET (LOCK_ESCALATION = TABLE)
GO
