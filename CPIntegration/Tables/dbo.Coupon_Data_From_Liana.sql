SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Coupon_Data_From_Liana] (
		[Item_Code]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description_Type]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[2019_20_Retail]       [float] NULL
)
GO
ALTER TABLE [dbo].[Coupon_Data_From_Liana] SET (LOCK_ESCALATION = TABLE)
GO
