SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblETACalculator_Temp] (
		[ID]                           [int] IDENTITY(1, 1) NOT NULL,
		[ServiceCode]                  [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FromZone]                     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ToZone]                       [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PrimaryNetworkCategory]       [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SecondaryNetworkCategory]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETA]                          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FromETA]                      [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ToETA]                        [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[1stPickupCutOffTime]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[1stDeliveryCutOffTime]        [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[1stAllowedDays]               [float] NULL,
		[2ndPickupCutOffTime]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[2ndDeliveryCutOffTime]        [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[2ndAllowedDays]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblETACalculator_1Temp]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[tblETACalculator_Temp] SET (LOCK_ESCALATION = TABLE)
GO
