SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER OFF
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_TrackingEvent_PODImage] (
		[PODImageID]          [int] NOT NULL,
		[EventID]             [int] NOT NULL,
		[LabelNumber]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NowGoImageID]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ImageType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HasContents]         [bit] NULL,
		[ImageDetail]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsDownloaded]        [bit] NULL,
		[DownloadMessage]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
