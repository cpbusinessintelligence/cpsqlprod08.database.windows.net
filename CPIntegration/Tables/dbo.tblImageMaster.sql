SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblImageMaster] (
		[ImageId]             [int] IDENTITY(1, 1) NOT NULL,
		[Title]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]         [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileName]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Url]                 [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]            [bit] NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailName]       [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailUrl]        [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__tblImage__7516F70C78ECF55F]
		PRIMARY KEY
		CLUSTERED
		([ImageId])
)
GO
ALTER TABLE [dbo].[tblImageMaster]
	ADD
	CONSTRAINT [DF__tblImageM__IsAct__2DFCAC08]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblImageMaster]
	ADD
	CONSTRAINT [DF__tblImageM__Creat__2EF0D041]
	DEFAULT ('System') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblImageMaster] SET (LOCK_ESCALATION = TABLE)
GO
