SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRedemption] (
		[RedemptionID]        [int] IDENTITY(1, 1) NOT NULL,
		[ContractorCode]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RunNumber]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RunName]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Activity]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ActivityDate]        [datetime] NULL,
		[Category]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CouponType]          [nchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Count]               [decimal](18, 2) NULL,
		[Amount]              [decimal](18, 2) NULL,
		[AccountCode]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountName]         [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [int] NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblRedemption]
		PRIMARY KEY
		CLUSTERED
		([RedemptionID])
)
GO
ALTER TABLE [dbo].[tblRedemption] SET (LOCK_ESCALATION = TABLE)
GO
