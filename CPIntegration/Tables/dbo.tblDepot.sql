SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDepot] (
		[DepotID]              [int] IDENTITY(1, 1) NOT NULL,
		[DepotName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DepotCode]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                [int] NOT NULL,
		[IsActive]             [bit] NULL,
		[InActiveDateTime]     [datetime] NULL,
		[UTCDateTime]          [datetimeoffset](7) NOT NULL,
		[LocalDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblDepot]
		PRIMARY KEY
		CLUSTERED
		([DepotID])
)
GO
ALTER TABLE [dbo].[tblDepot]
	WITH NOCHECK
	ADD CONSTRAINT [tblState_tblDepot]
	FOREIGN KEY ([State]) REFERENCES [dbo].[tblState] ([StateID])
ALTER TABLE [dbo].[tblDepot]
	CHECK CONSTRAINT [tblState_tblDepot]

GO
ALTER TABLE [dbo].[tblDepot] SET (LOCK_ESCALATION = TABLE)
GO
