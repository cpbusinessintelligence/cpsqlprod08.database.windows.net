SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCustomisedRateCardDetail] (
		[id]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Account]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EffectiveDate]       [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginZone]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationZone]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MinimumCharge]       [decimal](10, 4) NULL,
		[BasicCharge]         [decimal](10, 4) NULL,
		[FuelOverride]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage]      [decimal](10, 4) NULL,
		[Rounding]            [decimal](10, 4) NULL,
		[ChargePerKilo]       [decimal](10, 4) NULL,
		[_rid]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_self]               [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_etag]               [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_attachments]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_ts]                 [bigint] NULL,
		[IDkey]               [int] IDENTITY(1, 1) NOT NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__tblCusto__8C5884FD6A026531]
		PRIMARY KEY
		CLUSTERED
		([IDkey])
)
GO
ALTER TABLE [dbo].[tblCustomisedRateCardDetail] SET (LOCK_ESCALATION = TABLE)
GO
