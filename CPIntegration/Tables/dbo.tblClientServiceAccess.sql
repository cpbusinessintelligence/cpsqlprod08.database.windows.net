SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tblClientServiceAccess] (
		[AccessID]             [int] IDENTITY(1, 1) NOT NULL,
		[ClientID]             [int] NOT NULL,
		[ServiceID]            [int] NOT NULL,
		[IsActive]             [bit] NOT NULL,
		[InActiveDateTime]     [datetime] NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblClientServiceAccess]
		PRIMARY KEY
		CLUSTERED
		([AccessID])
)
GO
ALTER TABLE [dbo].[tblClientServiceAccess]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblClient_tblClientServiceAccess]
	FOREIGN KEY ([ClientID]) REFERENCES [dbo].[tblClient] ([ClientID])
ALTER TABLE [dbo].[tblClientServiceAccess]
	CHECK CONSTRAINT [FK_FK_tblClient_tblClientServiceAccess]

GO
ALTER TABLE [dbo].[tblClientServiceAccess]
	WITH CHECK
	ADD CONSTRAINT [FK_FK_tblServices_tblClientServiceAccess]
	FOREIGN KEY ([ServiceID]) REFERENCES [dbo].[tblServices] ([ServiceID])
ALTER TABLE [dbo].[tblClientServiceAccess]
	CHECK CONSTRAINT [FK_FK_tblServices_tblClientServiceAccess]

GO
ALTER TABLE [dbo].[tblClientServiceAccess] SET (LOCK_ESCALATION = TABLE)
GO
