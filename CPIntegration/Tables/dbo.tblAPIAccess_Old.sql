SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAPIAccess_Old] (
		[IDExtra]              [int] NULL,
		[ClientCode]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceName]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedBy]            [int] NULL,
		[UpdatedDateTime]      [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedBy]            [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]             [int] NULL,
		[InActiveDateTime]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[id]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_rid]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_self]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_etag]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_attachments]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_ts]                  [int] NULL,
		[isComplete]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdateDateTime]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdateBy]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Name]                 [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceStatus]        [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblAPIAccess_Old] SET (LOCK_ESCALATION = TABLE)
GO
