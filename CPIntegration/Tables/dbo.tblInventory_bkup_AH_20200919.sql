SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblInventory_bkup_AH_20200919] (
		[InventoryID]          [int] IDENTITY(1, 1) NOT NULL,
		[InventoryCode]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Description]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[NoOfCoupon]           [int] NULL,
		[CostPerCoupon]        [decimal](18, 4) NULL,
		[SalesPriceExGST]      [decimal](18, 4) NOT NULL,
		[IsActive]             [bit] NULL,
		[InActiveDateTime]     [datetime] NULL,
		[UTCDateTime]          [datetimeoffset](7) NOT NULL,
		[LocalDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblInventory_bkup_AH_20200919] SET (LOCK_ESCALATION = TABLE)
GO
