SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblTrackingEventMapping] (
		[TrackingEventMappingId]     [int] IDENTITY(1, 1) NOT NULL,
		[WorkFlow]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OutCome]                    [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Message]                    [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PublishTo]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsArchive]                  [bit] NULL,
		[IsDeleted]                  [bit] NULL,
		[CreatedDateTime]            [datetime] NOT NULL,
		[CreatedBy]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]            [datetime] NULL,
		[UpdatedBy]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_WorkFlowOutComeMsg]
		PRIMARY KEY
		CLUSTERED
		([TrackingEventMappingId])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblTrackingEventMapping]
	ADD
	CONSTRAINT [DF__tblTracki__IsDel__24485945]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tblTrackingEventMapping] SET (LOCK_ESCALATION = TABLE)
GO
