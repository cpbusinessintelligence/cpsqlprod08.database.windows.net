SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBooking] (
		[BookingID]               [int] IDENTITY(1, 1) NOT NULL,
		[BookingType]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[BookingRefNo]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                  [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountCode]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupCompanyName]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupContactName]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress1]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress2]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupAddress3]          [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupSuburb]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupState]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupPostCode]          [int] NULL,
		[PickupPhone]             [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupEmail]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupDate]              [date] NULL,
		[PickupTime]              [time](7) NULL,
		[PickupFrom]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OrderCoupons]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryCompanyName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryContactName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryAddress1]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryAddress2]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryAddress3]        [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliverySuburb]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryState]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryPostcode]        [int] NULL,
		[DeliveryEmail]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryPhone]           [nvarchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MessageId]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]             [bit] NULL,
		[UTCDateTime]             [datetimeoffset](7) NOT NULL,
		[LocalDateTime]           [datetime] NULL,
		[NowGoJobID]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Request]                 [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Response]                [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoRequest]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoResponse]           [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverRef]               [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverExtRef]            [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverName]              [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverEmail]             [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]         [datetime] NOT NULL,
		[CreatedBy]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]         [datetime] NULL,
		[UpdatedBy]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__tblBooki__73951ACD87AB1DD3]
		PRIMARY KEY
		CLUSTERED
		([BookingID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBooking] SET (LOCK_ESCALATION = TABLE)
GO
