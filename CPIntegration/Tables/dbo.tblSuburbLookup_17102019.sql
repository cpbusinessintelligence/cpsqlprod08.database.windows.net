SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSuburbLookup_17102019] (
		[PostCodeID]             [int] NULL,
		[PostCode]               [int] NULL,
		[Suburb]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PrizeZone]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PrizeZoneName]          [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETAZone]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETAZoneName]            [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedemptionZone]         [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedemptionZoneName]     [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DepotCode]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SortCode]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsPickup]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsDelivery]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCodeSuburb]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCodeOld]            [int] NULL
)
GO
ALTER TABLE [dbo].[tblSuburbLookup_17102019] SET (LOCK_ESCALATION = TABLE)
GO
