SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblErrorLogOld] (
		[ErrorCode]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorType]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Error]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Function]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Application]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Request]          [nvarchar](1100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Response]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[URL]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AffectedUser]     [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LocalTime]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedBy]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientCode]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[id]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_rid]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_self]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_etag]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_attachments]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_ts]              [int] NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblErrorLogOld] SET (LOCK_ESCALATION = TABLE)
GO
