SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCutoffTime] (
		[ID]                     [int] IDENTITY(1, 1) NOT NULL,
		[PostCode]               [float] NULL,
		[Suburb]                 [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                  [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Category]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PickupCutoffTime]       [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DeliveryCutoffTime]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblCutoffTime]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[tblCutoffTime] SET (LOCK_ESCALATION = TABLE)
GO
