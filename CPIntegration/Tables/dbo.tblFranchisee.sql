SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFranchisee] (
		[FranchiseeID]         [int] IDENTITY(1, 1) NOT NULL,
		[FranchiseeCode]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsActive]             [bit] NULL,
		[InactiveDateTime]     [datetime] NULL,
		[UTCDateTime]          [datetimeoffset](7) NOT NULL,
		[LocalDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblFranchisee]
		PRIMARY KEY
		CLUSTERED
		([FranchiseeID])
)
GO
ALTER TABLE [dbo].[tblFranchisee] SET (LOCK_ESCALATION = TABLE)
GO
