SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblServices] (
		[ServiceID]           [int] IDENTITY(1, 1) NOT NULL,
		[ServiceName]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ServiceCode]         [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ServiceURL]          [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [int] NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [int] NULL,
		CONSTRAINT [PK_tblServices]
		PRIMARY KEY
		CLUSTERED
		([ServiceID])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblServices] SET (LOCK_ESCALATION = TABLE)
GO
