SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCouponSales_bkp_06052019] (
		[ID]                        [int] IDENTITY(1, 1) NOT NULL,
		[RP]                        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SaleDateTime]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[accountCode]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[barcode]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[branch]                    [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[consignmentType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[coupon]                    [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[customerPhoneNumber]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[driverWareHouseNumber]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[insuranceCode]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[insurancevalue]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[invoiceNumber]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[sale]                      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transactionType]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SalesPriceExGST]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[GST]                       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InsuranceAmount]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SalesPriceIncGST]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NoOfCoupon]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsExtractedToPronto]       [bit] NULL,
		[CreatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]           [datetime] NULL
)
GO
ALTER TABLE [dbo].[tblCouponSales_bkp_06052019] SET (LOCK_ESCALATION = TABLE)
GO
