SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblErrorLog_bkp_03042019] (
		[ID]               [int] IDENTITY(1, 1) NOT NULL,
		[ErrorCode]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorType]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Error]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Function]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Application]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Request]          [nvarchar](1100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Response]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[URL]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AffectedUser]     [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]      [datetime] NULL,
		[LocalTime]        [datetime] NULL,
		[CreatedBy]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientCode]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblErrorLog_bkp_03042019] SET (LOCK_ESCALATION = TABLE)
GO
