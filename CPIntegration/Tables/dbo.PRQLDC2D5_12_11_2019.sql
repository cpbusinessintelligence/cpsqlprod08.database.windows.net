SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PRQLDC2D5_12_11_2019] (
		[Postcode]               [int] NULL,
		[Suburb]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SuburbProfile]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CutOff1]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CutOff2]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Cycle_1_start_time]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Cycle_1_end_time]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Cycle_2_start_time]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Cycle_2_end_time]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PRQLDC2D5_12_11_2019] SET (LOCK_ESCALATION = TABLE)
GO
