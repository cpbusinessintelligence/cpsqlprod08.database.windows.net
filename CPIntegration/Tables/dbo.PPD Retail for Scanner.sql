SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PPD Retail for Scanner] (
		[Coupon_Code]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NoOfCoupon]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CostPerCoupon]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SalesPriceExGST]     [float] NULL,
		[Comment]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PPD Retail for Scanner] SET (LOCK_ESCALATION = TABLE)
GO
