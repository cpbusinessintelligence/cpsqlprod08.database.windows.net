SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCPNotificationMaster] (
		[CPNotificationId]     [int] IDENTITY(1, 1) NOT NULL,
		[Title]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]          [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileName]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Url]                  [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]             [bit] NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailName]        [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailUrl]         [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__tblCPNot__64CF7D68C55FF486]
		PRIMARY KEY
		CLUSTERED
		([CPNotificationId])
)
GO
ALTER TABLE [dbo].[tblCPNotificationMaster]
	ADD
	CONSTRAINT [DF__tblCPNoti__IsAct__228AF95C]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblCPNotificationMaster]
	ADD
	CONSTRAINT [DF__tblCPNoti__Creat__237F1D95]
	DEFAULT ('System') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblCPNotificationMaster] SET (LOCK_ESCALATION = TABLE)
GO
