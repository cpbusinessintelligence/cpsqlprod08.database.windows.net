SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCouponSalesSLSExportStaging_Test] (
		[ID]                        [int] NOT NULL,
		[RP]                        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SaleDateTime]              [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[accountCode]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[barcode]                   [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[branch]                    [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[consignmentType]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[coupon]                    [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[customerPhoneNumber]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[driverWareHouseNumber]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[insuranceCode]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[insurancevalue]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[invoiceNumber]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[sale]                      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[transactionType]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverRunNumber]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ProntoId]                  [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ProntoAccountCode]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ProntoInsuranceCode]       [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblCouponSalesSLSExportStaging_Test] SET (LOCK_ESCALATION = TABLE)
GO
