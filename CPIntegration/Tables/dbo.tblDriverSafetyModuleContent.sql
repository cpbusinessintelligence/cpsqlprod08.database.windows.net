SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDriverSafetyModuleContent] (
		[ID]                        [int] IDENTITY(1, 1) NOT NULL,
		[DriverySafetyModuleID]     [int] NOT NULL,
		[Title]                     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ImagePath]                 [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[YouTubeLink]               [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]                  [bit] NULL,
		[CreatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]           [datetime] NULL,
		CONSTRAINT [PK_tblDriverSafetyModuleContent]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[tblDriverSafetyModuleContent]
	ADD
	CONSTRAINT [DF_tblDriverSafetyModuleContent_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblDriverSafetyModuleContent] SET (LOCK_ESCALATION = TABLE)
GO
