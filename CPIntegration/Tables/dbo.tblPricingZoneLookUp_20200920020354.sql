SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPricingZoneLookUp_20200920020354] (
		[ID]                     [bigint] IDENTITY(1, 1) NOT NULL,
		[PostCodeID]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                 [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                  [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PrizeZone]              [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PrizeZoneName]          [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETAZone]                [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETAZoneName]            [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedemptionZone]         [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedemptionZoneName]     [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DepotCode]              [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SortCode]               [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsPickup]               [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsDelivery]             [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCodeSuburb]         [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]               [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCodeOld]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]        [datetime] NULL,
		[CreatedBy]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblPricingZoneLookUp_20200920020354] SET (LOCK_ESCALATION = TABLE)
GO
