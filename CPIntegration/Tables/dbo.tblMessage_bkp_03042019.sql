SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMessage_bkp_03042019] (
		[ID]                  [int] IDENTITY(1, 1) NOT NULL,
		[ThreadID]            [int] NOT NULL,
		[BookingNumber]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SourceSystem]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPMessageID]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Branch]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[MessageDatetime]     [datetime] NULL,
		[Message]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Comment]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SendBy]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SendTo]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[EmmId]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]         [bit] NULL,
		[NowGoMessageID]      [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[driverRef]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPRequest]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPResponse]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoRequest]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoResponse]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]         [datetimeoffset](7) NOT NULL,
		[LocalDateTime]       [datetime] NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMessage_bkp_03042019] SET (LOCK_ESCALATION = TABLE)
GO
