SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDriverSafetyModule] (
		[ID]                  [int] NOT NULL,
		[ModuleName]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Path]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]            [bit] NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]     [datetime] NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDriverSafetyModule] SET (LOCK_ESCALATION = TABLE)
GO
