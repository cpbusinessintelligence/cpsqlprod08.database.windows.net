SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAddressType] (
		[AddressTypeID]          [int] IDENTITY(1, 1) NOT NULL,
		[AddressType]            [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddressDescription]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]               [bit] NULL,
		[InActiveDateTime]       [datetime] NULL,
		[UTCDateTime]            [datetimeoffset](7) NOT NULL,
		[LocalDateTime]          [datetime] NOT NULL,
		[CreatedDateTime]        [datetime] NOT NULL,
		[CreatedBy]              [int] NOT NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [int] NULL,
		CONSTRAINT [PK_tblAddressType]
		PRIMARY KEY
		CLUSTERED
		([AddressTypeID])
)
GO
ALTER TABLE [dbo].[tblAddressType] SET (LOCK_ESCALATION = TABLE)
GO
