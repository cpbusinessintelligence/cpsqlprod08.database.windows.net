SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAgentLookUp] (
		[ID]                    [int] IDENTITY(1, 1) NOT NULL,
		[AgentCode]             [float] NULL,
		[AgentName]             [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AgentShortname]        [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AgentDriverNumber]     [float] NULL,
		[Branch]                [float] NULL,
		[State]                 [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]              [float] NULL,
		[Suburb]                [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsInternal]            [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsPODRequired]         [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                 [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelPrinter]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BookingBranch]         [float] NULL,
		[BookingEmail]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReturnDirect]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EzyFreightDirect]      [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OgmTrigger]            [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OgmData]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Ogmruns]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SplitReturns]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Style]                 [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[App]                   [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AppInfo]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Sms]                   [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DirectMethod]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DirectAdditional0]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DirectAdditional1]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DirectAdditional2]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DirectAdditional3]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DirectAdditional4]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postcodesuburb]        [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCodeOld]           [float] NULL,
		CONSTRAINT [PK_tblAgentLookUp]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[tblAgentLookUp] SET (LOCK_ESCALATION = TABLE)
GO
