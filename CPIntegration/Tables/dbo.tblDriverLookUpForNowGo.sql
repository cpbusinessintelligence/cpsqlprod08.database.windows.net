SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDriverLookUpForNowGo] (
		[Id]           [int] IDENTITY(1, 1) NOT NULL,
		[DriverId]     [int] NULL,
		[Ext_ref]      [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Name]         [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]        [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblDriverLookUpForNowGo]
		PRIMARY KEY
		CLUSTERED
		([Id])
)
GO
ALTER TABLE [dbo].[tblDriverLookUpForNowGo] SET (LOCK_ESCALATION = TABLE)
GO
