SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblErrorLogNew1] (
		[Id]               [int] IDENTITY(1, 1) NOT NULL,
		[Error]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FunctionInfo]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientId]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDate]       [datetime] NULL,
		[Request]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[URL]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblErrorLogNew1]
		PRIMARY KEY
		CLUSTERED
		([Id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblErrorLogNew1]
	ADD
	CONSTRAINT [DF_tblErrorLogNew1_CreateDate]
	DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[tblErrorLogNew1] SET (LOCK_ESCALATION = TABLE)
GO
