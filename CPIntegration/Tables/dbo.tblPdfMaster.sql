SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPdfMaster] (
		[PdfId]                [int] IDENTITY(1, 1) NOT NULL,
		[Title]                [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]          [nvarchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileName]             [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Url]                  [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]             [bit] NULL,
		[InActiveDatetime]     [datetime] NULL,
		[InActiveBy]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]          [datetime] NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailName]        [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThumbnailUrl]         [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__tblPdfMa__91794C61C5CC0345]
		PRIMARY KEY
		CLUSTERED
		([PdfId])
)
GO
ALTER TABLE [dbo].[tblPdfMaster]
	ADD
	CONSTRAINT [DF__tblPdfMas__IsAct__7988E3C9]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblPdfMaster]
	ADD
	CONSTRAINT [DF__tblPdfMas__Creat__7A7D0802]
	DEFAULT ('System') FOR [CreatedBy]
GO
ALTER TABLE [dbo].[tblPdfMaster] SET (LOCK_ESCALATION = TABLE)
GO
