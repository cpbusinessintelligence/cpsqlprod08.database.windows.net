SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblNonCustomisedRateCardDetail] (
		[idKey]               [bigint] IDENTITY(1, 1) NOT NULL,
		[id]                  [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Account]             [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]             [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EffectiveDate]       [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OriginZone]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationZone]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MinimumCharge]       [decimal](10, 4) NULL,
		[BasicCharge]         [decimal](10, 4) NULL,
		[FuelOverride]        [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage]      [decimal](10, 4) NULL,
		[Rounding]            [decimal](10, 4) NULL,
		[ChargePerKilo]       [decimal](10, 4) NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__tblNonCu__3FBEE740ACE3A845]
		PRIMARY KEY
		CLUSTERED
		([idKey])
)
GO
ALTER TABLE [dbo].[tblNonCustomisedRateCardDetail] SET (LOCK_ESCALATION = TABLE)
GO
