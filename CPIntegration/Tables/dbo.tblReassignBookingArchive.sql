SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblReassignBookingArchive] (
		[ReBookingID]         [int] NULL,
		[BookingRefNo]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverExtRef]        [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]         [bit] NULL,
		[UTCDateTime]         [datetimeoffset](7) NULL,
		[LocalDateTime]       [datetime] NULL,
		[NowGoJobID]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Request]             [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Response]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoRequest]        [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoResponse]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblReassignBookingArchive] SET (LOCK_ESCALATION = TABLE)
GO
