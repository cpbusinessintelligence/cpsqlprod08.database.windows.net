SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRequestLog_bkp_03042019] (
		[RequestID]        [int] IDENTITY(1, 1) NOT NULL,
		[AccountCode]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IPaddress]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RequestURL]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Request]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Function]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Application]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]      [datetime] NULL,
		[LocalTime]        [datetime] NULL,
		[CreatedBy]        [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientCode]       [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ServiceCode]      [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[id]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_rid]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_self]            [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_etag]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_attachments]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[_ts]              [int] NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRequestLog_bkp_03042019] SET (LOCK_ESCALATION = TABLE)
GO
