SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPrepaidAccount] (
		[PrepaidAccountID]      [int] IDENTITY(1, 1) NOT NULL,
		[AccountCode]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ParentAccountID]       [int] NULL,
		[ProntoAccountCode]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Branch]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DespatchAddressID]     [int] NOT NULL,
		[MailingAddressID]      [int] NOT NULL,
		[OpenDays]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[OpenTime]              [time](7) NULL,
		[CloseTime]             [time](7) NULL,
		[Contractor]            [int] NOT NULL,
		[Depot]                 [int] NOT NULL,
		[State]                 [int] NOT NULL,
		[Latitude]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Longitude]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]              [bit] NULL,
		[InactiveDateTime]      [datetime] NULL,
		[UTCDateTime]           [datetimeoffset](7) NOT NULL,
		[LocalDateTime]         [datetime] NOT NULL,
		[CreatedDateTime]       [datetime] NOT NULL,
		[CreatedBy]             [int] NOT NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [int] NULL,
		CONSTRAINT [PK_tblPrepaidAccount]
		PRIMARY KEY
		CLUSTERED
		([PrepaidAccountID])
)
GO
ALTER TABLE [dbo].[tblPrepaidAccount]
	WITH NOCHECK
	ADD CONSTRAINT [tblAddress_tblPrepaidAccount]
	FOREIGN KEY ([DespatchAddressID]) REFERENCES [dbo].[tblAddress] ([AddressID])
ALTER TABLE [dbo].[tblPrepaidAccount]
	NOCHECK CONSTRAINT [tblAddress_tblPrepaidAccount]

GO
ALTER TABLE [dbo].[tblPrepaidAccount]
	WITH NOCHECK
	ADD CONSTRAINT [tblAddress_tblPrepaidAccount_Mailing]
	FOREIGN KEY ([MailingAddressID]) REFERENCES [dbo].[tblAddress] ([AddressID])
ALTER TABLE [dbo].[tblPrepaidAccount]
	NOCHECK CONSTRAINT [tblAddress_tblPrepaidAccount_Mailing]

GO
ALTER TABLE [dbo].[tblPrepaidAccount]
	WITH NOCHECK
	ADD CONSTRAINT [tblState_tblPrepaidAccount]
	FOREIGN KEY ([State]) REFERENCES [dbo].[tblState] ([StateID])
ALTER TABLE [dbo].[tblPrepaidAccount]
	NOCHECK CONSTRAINT [tblState_tblPrepaidAccount]

GO
ALTER TABLE [dbo].[tblPrepaidAccount] SET (LOCK_ESCALATION = TABLE)
GO
