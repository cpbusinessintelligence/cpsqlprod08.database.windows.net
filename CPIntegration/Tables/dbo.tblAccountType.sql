SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAccountType] (
		[AccountTypeID]        [int] IDENTITY(1, 1) NOT NULL,
		[AccountType]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Description]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]             [bit] NULL,
		[InactiveDateTime]     [datetime] NULL,
		[UTCDateTime]          [datetime2](7) NOT NULL,
		[LocalDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblPrepaidAccountType]
		PRIMARY KEY
		CLUSTERED
		([AccountTypeID])
)
GO
ALTER TABLE [dbo].[tblAccountType] SET (LOCK_ESCALATION = TABLE)
GO
