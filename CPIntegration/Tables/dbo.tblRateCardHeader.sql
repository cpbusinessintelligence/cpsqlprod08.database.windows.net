SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRateCardHeader] (
		[IDkey]                 [bigint] IDENTITY(1, 1) NOT NULL,
		[id]                    [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ValidFrom]             [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelOverride]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Fuel]                  [decimal](10, 4) NULL,
		[ChargeMethod]          [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VolumetricDivisor]     [decimal](10, 4) NULL,
		[TariffId]              [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logwho]                [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logdate]               [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]       [datetime] NULL,
		[CreatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]       [datetime] NULL,
		[UpdatedBy]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__tblRateC__8C5884FD3A12A174]
		PRIMARY KEY
		CLUSTERED
		([IDkey])
)
GO
ALTER TABLE [dbo].[tblRateCardHeader] SET (LOCK_ESCALATION = TABLE)
GO
