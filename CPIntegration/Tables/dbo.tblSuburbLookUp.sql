SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblSuburbLookUp] (
		[ID]                     [int] IDENTITY(1, 1) NOT NULL,
		[PostCodeID]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCode]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Suburb]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[State]                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PrizeZone]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PrizeZoneName]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETAZone]                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ETAZoneName]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedemptionZone]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedemptionZoneName]     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DepotCode]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SortCode]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsPickup]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsDelivery]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCodeSuburb]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostCodeOld]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblSuburbLookUp]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
CREATE NONCLUSTERED INDEX [IndtblSuburbLookUpPostCode]
	ON [dbo].[tblSuburbLookUp] ([PostCodeSuburb], [Suburb])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblSuburbLookUp] SET (LOCK_ESCALATION = TABLE)
GO
