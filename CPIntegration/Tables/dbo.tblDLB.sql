SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDLB] (
		[DlbID]                      [int] IDENTITY(1, 1) NOT NULL,
		[PrepaidAccountID]           [int] NOT NULL,
		[NationalLocationNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Branch]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Code]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[CustomerName]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Location]                   [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsActive]                   [bit] NULL,
		[InActiveDateTime]           [datetime] NULL,
		[UTCDateTime]                [datetime] NOT NULL,
		[LocalDateTime]              [datetime] NOT NULL,
		[CreatedDateTime]            [datetime] NOT NULL,
		[CreatedBy]                  [int] NOT NULL,
		[UpdatedDateTime]            [datetime] NULL,
		[UpdatedBy]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblDLB]
		PRIMARY KEY
		CLUSTERED
		([DlbID])
)
GO
ALTER TABLE [dbo].[tblDLB]
	WITH CHECK
	ADD CONSTRAINT [tblPrepaidAccount_tblDLB]
	FOREIGN KEY ([PrepaidAccountID]) REFERENCES [dbo].[tblPrepaidAccount] ([PrepaidAccountID])
ALTER TABLE [dbo].[tblDLB]
	CHECK CONSTRAINT [tblPrepaidAccount_tblDLB]

GO
ALTER TABLE [dbo].[tblDLB] SET (LOCK_ESCALATION = TABLE)
GO
