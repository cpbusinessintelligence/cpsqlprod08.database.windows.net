SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblMessageArchive] (
		[ID]                  [int] NULL,
		[ThreadID]            [int] NULL,
		[BookingNumber]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SourceSystem]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPMessageID]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]              [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MessageDatetime]     [datetime] NULL,
		[Message]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Comment]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SendBy]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SendTo]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EmmId]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]         [bit] NULL,
		[NowGoMessageID]      [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[driverRef]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPRequest]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPResponse]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoRequest]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoResponse]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]         [datetimeoffset](7) NULL,
		[LocalDateTime]       [datetime] NULL,
		[CreatedDateTime]     [datetime] NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblMessageArchive] SET (LOCK_ESCALATION = TABLE)
GO
