SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblErrorLogNew] (
		[Id]               [int] IDENTITY(1, 1) NOT NULL,
		[Error]            [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FunctionInfo]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ClientId]         [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDate]       [datetime] NULL,
		[Request]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[URL]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblErrorLog]
		PRIMARY KEY
		CLUSTERED
		([Id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblErrorLogNew] SET (LOCK_ESCALATION = TABLE)
GO
