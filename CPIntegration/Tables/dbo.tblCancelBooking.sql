SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCancelBooking] (
		[CancelBookingID]     [int] IDENTITY(1, 1) NOT NULL,
		[BookingRefNo]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverExtRef]        [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsClosed]            [bit] NULL,
		[UTCDateTime]         [datetimeoffset](7) NOT NULL,
		[LocalDateTime]       [datetime] NULL,
		[NowGoJobID]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Request]             [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Response]            [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoRequest]        [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoResponse]       [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCancelBooking] SET (LOCK_ESCALATION = TABLE)
GO
