SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblChannel] (
		[ChannelID]              [int] IDENTITY(1, 1) NOT NULL,
		[ChannelName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ChannelDescription]     [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]               [bit] NOT NULL,
		[CreatedDateTime]        [datetime] NOT NULL,
		[CreatedBy]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]        [datetime] NULL,
		[UpdatedBy]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblChannel]
		PRIMARY KEY
		CLUSTERED
		([ChannelID])
)
GO
ALTER TABLE [dbo].[tblChannel]
	ADD
	CONSTRAINT [DF_tblChannel_IsActive]
	DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[tblChannel]
	ADD
	CONSTRAINT [DF_tblChannel_CreatedDateTime]
	DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[tblChannel] SET (LOCK_ESCALATION = TABLE)
GO
