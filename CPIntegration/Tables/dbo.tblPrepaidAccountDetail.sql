SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPrepaidAccountDetail] (
		[AccountDetailID]      [int] IDENTITY(1, 1) NOT NULL,
		[PrepaidAccountID]     [int] NOT NULL,
		[StartDate]            [datetime] NULL,
		[LastUsed]             [datetime] NOT NULL,
		[Added]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountType]          [int] NOT NULL,
		[IndustryCode]         [int] NOT NULL,
		[Eft]                  [bit] NULL,
		[Status]               [bit] NULL,
		[RegularRun]           [bit] NULL,
		[PickupDays]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SalesRepID]           [int] NOT NULL,
		[RepStart]             [datetime] NULL,
		[Comments]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]          [datetime2](7) NOT NULL,
		[LocalDateTime]        [datetime] NOT NULL,
		[CreatedDateTime]      [datetime] NOT NULL,
		[CreatedBy]            [int] NOT NULL,
		[UpdatedDateTime]      [datetime] NULL,
		[UpdatedBy]            [int] NULL,
		CONSTRAINT [PK_tblPrepaidAccountDetail]
		PRIMARY KEY
		CLUSTERED
		([AccountDetailID])
)
GO
ALTER TABLE [dbo].[tblPrepaidAccountDetail]
	WITH NOCHECK
	ADD CONSTRAINT [tblPrepaidAccount_tblPrepaidAccountDetail]
	FOREIGN KEY ([PrepaidAccountID]) REFERENCES [dbo].[tblPrepaidAccount] ([PrepaidAccountID])
ALTER TABLE [dbo].[tblPrepaidAccountDetail]
	NOCHECK CONSTRAINT [tblPrepaidAccount_tblPrepaidAccountDetail]

GO
ALTER TABLE [dbo].[tblPrepaidAccountDetail]
	WITH NOCHECK
	ADD CONSTRAINT [tblSalesRep_tblPrepaidAccountDetail]
	FOREIGN KEY ([SalesRepID]) REFERENCES [dbo].[tblSalesRep] ([SalesRepID])
ALTER TABLE [dbo].[tblPrepaidAccountDetail]
	NOCHECK CONSTRAINT [tblSalesRep_tblPrepaidAccountDetail]

GO
ALTER TABLE [dbo].[tblPrepaidAccountDetail] SET (LOCK_ESCALATION = TABLE)
GO
