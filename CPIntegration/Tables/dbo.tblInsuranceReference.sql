SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblInsuranceReference] (
		[InsuranceID]               [int] IDENTITY(1, 1) NOT NULL,
		[InsuranceCategoryID]       [int] NULL,
		[InsuranceCategoryCode]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[InsuranceCharge]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]           [datetime] NULL,
		[CreatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UpdatedDateTime]           [datetime] NULL,
		[UpdatedBy]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__tblInsur__74231BC48DB5535D]
		PRIMARY KEY
		CLUSTERED
		([InsuranceID])
)
GO
ALTER TABLE [dbo].[tblInsuranceReference] SET (LOCK_ESCALATION = TABLE)
GO
