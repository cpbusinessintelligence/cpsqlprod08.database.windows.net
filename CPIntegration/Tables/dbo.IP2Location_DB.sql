SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IP2Location_DB] (
		[ip_from]          [bigint] NULL,
		[ip_to]            [bigint] NULL,
		[country_code]     [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[country_name]     [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[region_name]      [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[city_name]        [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[latitude]         [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[longitude]        [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[zip_code]         [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[time_zone]        [nvarchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
CREATE NONCLUSTERED INDEX [nci_wi_IP2Location_DB_2C855FEE1021F8159E999B4067A5147C]
	ON [dbo].[IP2Location_DB] ([ip_from])
	INCLUDE ([ip_to], [time_zone])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[IP2Location_DB] SET (LOCK_ESCALATION = TABLE)
GO
