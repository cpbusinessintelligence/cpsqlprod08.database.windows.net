SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ETACalculator_bkup_AH_20200922] (
		[ETAID]                                  [int] NOT NULL,
		[FromZone]                               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ToZone]                                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PrimaryNetworkCategory]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SecondaryNetworkCategory]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ETA]                                    [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[FromETA]                                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ToETA]                                  [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[1stPickupCutOffTime]                    [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[1stDeliveryCutOffTime]                  [datetime2](7) NOT NULL,
		[1stAllowedDays]                         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[2ndPickupCutOffTime]                    [datetime2](7) NOT NULL,
		[2ndDeliveryCutOffTime]                  [datetime2](7) NOT NULL,
		[2ndAllowedDays]                         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AIR_ETA]                                [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AIR_FromETA]                            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AIR_ToETA]                              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AIR_1stDeliveryCutOffTime]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AIR_1stAllowedDays]                     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AIR_2ndDeliveryCutOffTime]              [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AIR_2ndAllowedDays]                     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SameDay_ETA]                            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SameDay_FromETA]                        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SameDay_ToETA]                          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SameDay_1stDeliveryCutOffTime]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SameDay_1stAllowedDays]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SameDay_2ndDeliveryCutOffTime]          [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SameDay_2ndAllowedDays]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddWho]                                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AddDateTime]                            [datetime2](7) NOT NULL,
		[EditWho]                                [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditDateTime]                           [nvarchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Domestic_OffPeak_ETA]                   [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Domestic_OffPeak_FromETA]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Domestic_OffPeak_ToETA]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Domestic_OffPeak_1stDeliveryCutOff]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Domestic_OffPeak_1stAllowedDays]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Domestic_OffPeak_2ndDeliveryCutOff]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Domestic_OffPeak_2ndAllowedDays]        [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ETACalculator_bkup_AH_20200922] SET (LOCK_ESCALATION = TABLE)
GO
