SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblPublicHolidays] (
		[ID]              [int] IDENTITY(1, 1) NOT NULL,
		[Date]            [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Description]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Zone]            [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblPublicHolidays]
		PRIMARY KEY
		CLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[tblPublicHolidays] SET (LOCK_ESCALATION = TABLE)
GO
