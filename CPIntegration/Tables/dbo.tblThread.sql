SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblThread] (
		[ThreadID]            [int] IDENTITY(1, 1) NOT NULL,
		[NowGoThreadID]       [nvarchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BookingNumber]       [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ThreadName]          [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IdempotencyKey]      [nvarchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverID]            [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[StopExtRef]          [uniqueidentifier] NULL,
		[Branch]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SendBy]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EmmID]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]         [bit] NULL,
		[driverRef]           [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UTCDateTime]         [datetimeoffset](7) NULL,
		[LocalDateTime]       [datetime] NULL,
		[CPRequest]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CPResponse]          [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoRequest]        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NowGoResponse]       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDateTime]     [datetime] NOT NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[UpdatedDateTime]     [datetime] NULL,
		[UpdatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblThread]
		PRIMARY KEY
		CLUSTERED
		([ThreadID])
) TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_tblThread_7A294F6B88461C88342F60AB35508F51]
	ON [dbo].[tblThread] ([BookingNumber])
	INCLUDE ([Branch], [CPRequest], [CPResponse], [CreatedBy], [CreatedDateTime], [DriverID], [driverRef], [EmmID], [IdempotencyKey], [IsProcessed], [LocalDateTime], [NowGoRequest], [NowGoResponse], [NowGoThreadID], [SendBy], [StopExtRef], [ThreadName], [UpdatedBy], [UpdatedDateTime], [UTCDateTime])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblThread] SET (LOCK_ESCALATION = TABLE)
GO
