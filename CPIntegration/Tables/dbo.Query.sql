SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Query] (
		[AccountCode]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ParentAccountID]       [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ProntoAccountCode]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DespatchAddressID]     [int] NULL,
		[MailingAddressID]      [int] NULL,
		[OpenDays]              [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[OpenTime]              [sql_variant] NULL,
		[CloseTime]             [sql_variant] NULL,
		[Contractor]            [int] NULL,
		[Depot]                 [int] NULL,
		[State]                 [int] NULL,
		[Latitude]              [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Longitude]             [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsActive]              [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[InactiveDateTime]      [datetime2](3) NULL,
		[UTCDateTime]           [datetimeoffset](7) NULL,
		[LocalDateTime]         [datetime2](3) NULL,
		[CreatedDateTime]       [datetime2](3) NULL,
		[CreatedBy]             [int] NULL,
		[UpdatedDateTime]       [datetime2](3) NULL,
		[UpdatedBy]             [int] NULL
)
GO
ALTER TABLE [dbo].[Query] SET (LOCK_ESCALATION = TABLE)
GO
