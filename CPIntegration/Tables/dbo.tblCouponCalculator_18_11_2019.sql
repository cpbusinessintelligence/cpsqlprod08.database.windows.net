SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCouponCalculator_18_11_2019] (
		[FromZone]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ToZone]           [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WB1Count]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WB2Count]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[WB3Count]         [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SatchelCount]     [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[tblCouponCalculator_18_11_2019] SET (LOCK_ESCALATION = TABLE)
GO
